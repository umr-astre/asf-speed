##########################################################################
## Code for reproducing some of the figures and table in our 3rd report ##
##########################################################################
## rm(list=ls())

library(dplyr)
library(sf)
library(tmap)
library(knitr)
library(ggplot2)
library(ggrepel)
library(tidyr)
library(igraph)
library(intergraph)
library(forcats)

setwd('~/asf-challenge/submittedSimulations')
source("epidemic_threshold.R")
load("simsCiradInraeTeamRound3.Rdata")


####################
## Some functions ##
####################
plot_area <- function(x, offset_east, buffer) {
  x %>%
    st_geometry() %>%
    ## add a margin of 70 km to the east
    c(., . + c(offset_east, 0)) %>%
    st_union() %>%
    st_buffer(dist = buffer) %>%
    st_bbox()
}

prob_fomites <- function(x, dist_mat) {
  risk_fomites <- function(x, lambda = 700/log(2)) exp(-x/lambda)
  r_ij <- risk_fomites(dist_mat)
  p_fomites <- 1 - exp(
    colSums(log(1 - r_ij * x) - diag(log(1-x)))
  )
  return(p_fomites)
}


relative_importance_network <- function(x) {
  etx <- epidemic_threshold(x, beta = 1)
  epiR0 <- etx$unweighted
  sna <- attr(epiR0, "sna")
  if (isTRUE(all.equal(unname(epiR0["R0"]), 0))) {
    ## In this case, all individual contributions are equally 0
    stopifnot(all(vapply(sna$R0k, all.equal, TRUE, 0)))
    relative_contributions <- rep(1/length(sna$R0k), length(sna$R0k))
  } else {
    relative_contributions <- sna$R0k / epiR0["R0"]
  }
  node_importance <- data.frame(
    id = sna[, "node"],
    importance = 100 * relative_contributions
  )
}

rim <- relative_importance_network(shipment_nw) %>%
  rename(r_diff = importance) %>%
  arrange(desc(r_diff)) %>%
  mutate(
    cumr = cumsum(r_diff),
    rank = row_number()
  )


mapWildBoarCasesDayX <- function(obsWildBoarCasesMatrix,
                                 Hex,
                                 mapArea,
                                 Admin, Day=50,
                                 ltyFence = 1,
                                 ltyHuntZone=1,
                                 outputDir, fileName, StepsPerChunk=1, Fence, HuntZone,
                                 returnMap=FALSE, legendOutside = FALSE,
                                 log10farmSize=TRUE, allowNegative = FALSE,
                                 legendMax = 0,
                                 pig_sites, Outbreaks, caption="MeanCumCases") {
## browser()
  createImageFile  <- TRUE
  if (missing(outputDir) | missing(fileName))
    createImageFile  <- FALSE
  ###
  obsWildBoarCasesMatrix    <- t(obsWildBoarCasesMatrix)
  cumObsWildBoarCasesMatrix <- 0 * obsWildBoarCasesMatrix
  for (ii in 1:nrow(cumObsWildBoarCasesMatrix))
    cumObsWildBoarCasesMatrix[ii,] <- cumsum(obsWildBoarCasesMatrix[ii,])
  ##
  nSteps <- dim(cumObsWildBoarCasesMatrix)[2]
  if (StepsPerChunk == 1) {
    colnames(cumObsWildBoarCasesMatrix) <- paste0(caption,"_Day",(1:nSteps)-1)
  } else if (StepsPerChunk > 1) {
    ## browser() ## Need to fix for this case
    (x <- (1:nSteps)*StepsPerChunk)
    (x1 <- c(1, x+1)[1:nSteps])
    colnames(cumObsWildBoarCasesMatrix) <-
      c(paste0(caption,"_Day0)"), paste0(caption,"_Days",paste0(x1,"-",x)))[1:nSteps]
  } else {
    stop("Error in mapInfectiousDayX")
  }
  Hex <- Hex %>% select(log10_Ewb1)
  Hex <- cbind(Hex, cumObsWildBoarCasesMatrix)
  if (!allowNegative) {
    breaks    <- pretty(c(0, legendMax, max(cumObsWildBoarCasesMatrix)), n=11)
    breaks    <- sort(unique(c(0, 1, breaks)))
  } else {
    breaks <- sort(c(-0.1,0.1,pretty(max(abs(cumObsWildBoarCasesMatrix)) * c(-1,1), n=11)))
    breaks <- breaks[breaks!=0]
  }
  lBreaks   <- length(breaks)
  sclB      <- round(2.5*lBreaks)
  if (allowNegative) {
    rwb <- colorRampPalette(colors = c("red", "white", "blue"))
    mypalette <- rwb(lBreaks)
  } else {
    mypalette <- rev(rev(heat.colors(sclB))[sclB:round(sclB/3)]) # c("white", rev(brewer.pal(length(breaks),"RdYlBu")))
  }
  pigSiteColours <- c("darkblue", "darkviolet", "darkgreen")
  if (!missing(pig_sites)) {
    log10_size <- (pig_sites %>% select(size) %>% st_drop_geometry() %>% log10())
    colnames(log10_size) <- "log10_size"
    pig_sites[["log10_size"]] <- (log10_size[,1])
    pig_sites[["Type&Practice"]] <- paste0(as.character(pig_sites$FarmType), as.character(pig_sites$practice))
    pig_sites[["is_outdoor"]]  <- as.factor(pig_sites[["is_outdoor"]])
    pig_sites[["is_commercial"]]  <- as.factor(pig_sites[["is_commercial"]])
  }
  ## https://stackoverflow.com/questions/1298100/creating-a-movie-from-a-series-of-plots-in-r
  ## browser()
  deltaX <- (Admin %>% st_bbox())$xmax - (Admin %>% st_bbox())$xmin
  deltaY <- (Admin %>% st_bbox())$ymax - (Admin %>% st_bbox())$ymin
  Height = 11
  Width = Height / deltaY * deltaX
  nimble::nimPrint("createImageFile = ", createImageFile)
  if (createImageFile)
    jpeg(paste0(outputDir, "/", fileName,".jpg"), quality = 98, res = 300, width=Width, height=Height, units="in")
  for (t in Day) { # t=Day
    ## print(t)
    ### browser()
    (colNameT <- colnames(cumObsWildBoarCasesMatrix)[t+1])
    (iHex <- ((Hex%>%select(all_of(colNameT))%>%st_drop_geometry() )[,all_of(colNameT)]) != 0)
    if (missing(mapArea)) {
      mytm <- tm_shape(Admin) + tm_borders()
    } else {
      mytm <- tm_shape(mapArea) + tm_borders(col=rgb(0,0,0,0))
    }
    mytm <- mytm +
      # Hexagons
      tm_shape(Hex) +
      tm_fill(colNameT, breaks=breaks, palette=mypalette,
              legend.show = TRUE, legend.backgroun="white") +
      tm_layout(legend.position = c("left", "bottom"), legend.outside=legendOutside, legend.bg.color = "white") +
      # Hexagons - Expected WB density = f(pForest)
      tm_shape(Hex) + tm_fill("log10_Ewb1", legend.show = TRUE, palette=c("darkolivegreen1","darkolivegreen4")) +
      # Sea
      tm_layout(bg.color = "lightblue") +
      # Admin area
      tm_shape(Admin) + tm_borders(col="darkslateblue", lwd=2.5) +
      tm_grid(n.x=4, n.y=7, labels.show = FALSE)
    if (sum(iHex)>0) {
      mytm <- mytm +
        ## Hexagons with non-zeros
        tm_shape(Hex[iHex,]) +
        tm_borders(col="lightgrey") +
        tm_fill(colNameT, breaks=breaks, palette=mypalette, legend.show = FALSE)
    }
    # Fence
    if (!missing(Fence))
      mytm <- mytm + tm_shape(Fence) + tm_borders(col="darkviolet", lwd=3, lty=ltyFence)
    # Hunt zone
    if (!missing(HuntZone))
      mytm <- mytm + tm_shape(HuntZone) + tm_borders(col="darkred", lwd=3, lty=ltyHuntZone)
    # Outbreaks
    if (!missing(Outbreaks))
      mytm <- mytm +
        tm_shape(Outbreaks %>% filter(HOST == "pig herd" & DATE.SUSP <= t)) +
        tm_dots(col="lightgreen", size=1, title="Farm Cases", legend.show = TRUE)
    # Pig farms
    if (!missing(pig_sites)) {
      if (log10farmSize) {
        mytm <- mytm + tm_shape(pig_sites) +
          tm_dots(col = "FarmType", size="log10_size", shape="practice", palette=pigSiteColours)
      } else {
        mytm <- mytm + tm_shape(pig_sites) +
          tm_dots(col = "FarmType", size=0.1, shape="practice", palette=pigSiteColours)
      }
    }
  }
  if (createImageFile) {
    print(mytm)
    dev.off()
  }
  if(returnMap) {
    return(mytm)
  } else {
    print(mytm)
  }
}



mapInfectiousDayX <- function(infectiousWildBoarMatrix,
                              Hex,
                              mapArea,
                              Admin, Day=50,
                              ltyFence = 1, ltyHuntZone=1,
                              outputDir, fileName,
                              StepsPerChunk=1,
                              Fence, HuntZone,
                              returnMap=FALSE, legendOutside = FALSE,
                              log10farmSize=TRUE,
                              pig_sites, Outbreaks, caption="MeanI") {
  createImageFile  <- TRUE
  if (missing(outputDir) | missing(fileName))
    createImageFile  <- FALSE
  ###
  infectiousWildBoarMatrix <- t(infectiousWildBoarMatrix)
  nSteps <- dim(infectiousWildBoarMatrix)[2]
  if (StepsPerChunk == 1) {
    colnames(infectiousWildBoarMatrix) <- paste0(caption,"_Day_",(1:nSteps)-1)
  } else if (StepsPerChunk > 1) {
    ## browser() ## Need to fix for this case
    (x <- (1:nSteps)*StepsPerChunk)
    (x1 <- c(1, x+1)[1:nSteps])
    colnames(infectiousWildBoarMatrix) <-
      c(paste0(caption,"_Day_0)"), paste0(caption,"_Days_",paste0(x1,"-",x)))[1:nSteps]
  } else {
    stop("Error in mapInfectiousDayX")
  }
  Hex       <- Hex %>% select(log10_Ewb1)
  Hex       <- cbind(Hex, infectiousWildBoarMatrix)
  breaks    <- pretty(c(0, max(infectiousWildBoarMatrix)), n=11)
  breaks    <- sort(unique(c(0, 1, breaks)))
  lBreaks   <- length(breaks)
  sclB      <- round(2.5*lBreaks)
  mypalette <- rev(rev(heat.colors(sclB))[sclB:round(sclB/3)]) # c("white", rev(brewer.pal(length(breaks),"RdYlBu")))
  pigSiteColours <- c("darkblue", "darkviolet", "darkgreen")
  if (!missing(pig_sites)) {
    log10_size <- (pig_sites %>% select(size) %>% st_drop_geometry() %>% log10())
    colnames(log10_size) <- "log10_size"
    pig_sites[["log10_size"]] <- (log10_size[,1])
    pig_sites[["Type&Practice"]] <- paste0(as.character(pig_sites$FarmType), as.character(pig_sites$practice))
    pig_sites[["is_outdoor"]]  <- as.factor(pig_sites[["is_outdoor"]])
    pig_sites[["is_commercial"]]  <- as.factor(pig_sites[["is_commercial"]])
  }
  ## https://stackoverflow.com/questions/1298100/creating-a-movie-from-a-series-of-plots-in-r
  ## browser()
  deltaX <- (Admin %>% st_bbox())$xmax - (Admin %>% st_bbox())$xmin
  deltaY <- (Admin %>% st_bbox())$ymax - (Admin %>% st_bbox())$ymin
  Height = 11
  Width = Height / deltaY * deltaX
  if (createImageFile)
    jpeg(paste0(outputDir, "/", fileName,".jpg"), quality = 98, res = 300, width=Width, height=Height, units="in")
  for (t in Day) { # t=Day
    ## print(t)
    ### browser()
    (colNameT <- colnames(infectiousWildBoarMatrix)[t+1])
    (iHex <- ((Hex%>%select(all_of(colNameT))%>%st_drop_geometry() )[,all_of(colNameT)]) > 0)
    if (missing(mapArea)) {
      mytm <- tm_shape(Admin) + tm_borders()
    } else {
      mytm <- tm_shape(mapArea) + tm_borders(col=rgb(0,0,0,0))
    }
    mytm <- mytm +
      # Hexagons
      tm_shape(Hex) +
      tm_fill(colNameT, breaks=breaks, palette=mypalette,
              legend.show = TRUE, legend.backgroun="white") +
      tm_layout(legend.position = c("left", "bottom"), legend.outside=legendOutside, legend.bg.color = "white") +
      # Hexagons - forest
      tm_shape(Hex) + tm_fill("log10_Ewb1", legend.show = FALSE, palette=c("darkolivegreen1","darkolivegreen4")) +
      # Sea
      tm_layout(bg.color = "lightblue") +
      # Admin area
      tm_shape(Admin) + tm_borders(col="darkslateblue", lwd=2.5) +
      tm_grid(n.x=4, n.y=7)
    if (sum(iHex)>0) {
      mytm <- mytm +
        ## Hexagons with non-zeros
        tm_shape(Hex[iHex,]) +
        tm_borders(col="lightgrey") +
        tm_fill(colNameT, breaks=breaks, palette=mypalette, legend.show = FALSE)
    }
    # Fence
    if (!missing(Fence))
      mytm <- mytm + tm_shape(Fence) + tm_borders(col="darkviolet", lwd=3, lty=ltyFence)
    # Hunt zone
    if (!missing(HuntZone))
      mytm <- mytm + tm_shape(HuntZone) + tm_borders(col="darkred", lwd=3, lty=ltyHuntZone)
    # Outbreaks
    if (!missing(Outbreaks))
      mytm <- mytm +
        tm_shape(Outbreaks %>% filter(HOST == "pig herd" & DATE.SUSP <= t)) +
        tm_dots(col="lightgreen", size=1, title="Farm Cases", legend.show = TRUE)
    # Pig farms
    if (!missing(pig_sites)) {
      if (log10farmSize) {
        mytm <- mytm + tm_shape(pig_sites) +
          tm_dots(col = "FarmType", size="log10_size", shape="practice", palette=pigSiteColours)
      } else {
        mytm <- mytm + tm_shape(pig_sites) +
          tm_dots(col = "FarmType", size=0.1, shape="practice", palette=pigSiteColours)
      }
    }
    print(mytm)
  }
  if (createImageFile)
    dev.off()
  if(returnMap)
    return(mytm)
}


############################################################################################
## Fig 1: Predicted risks made at day 80 (using updated methods) and                      ##
## observed infections at pig sites up to day 110. Points outlined in red                 ##
## were predicted with probability lower than 5%. The only one in this case is farm id 60 ##
############################################################################################
outbreaks_predD80_vs_realised <-
  full_join(
    outbreaks_at_D110 %>%
      filter(HOST == "pig herd") %>%
      st_drop_geometry() %>%
      mutate(Infection = ifelse(DATE.SUSP <= 80, "Old", "New")) %>%
      filter(Infection != "Old")
    ,
    tab_farms_pred_CSIF_D80 %>%
      select(-size) %>%
      mutate(predicted = p_pred > 0.05) %>%
      filter(predicted),
    by = c(ID = "id")
  ) %>%
  replace_na(
    list(
      Infection = "Not infected",
      predicted = FALSE
    )
  ) %>%
  mutate(Infection = fct_rev(fct_inorder(Infection))) %>%
  left_join(
    pig_sites,
    by = c(ID = "population_id")
  ) %>%
  st_sf()


fig1 <- tm_shape(
  admin,
  bbox = st_bbox(
    outbreaks_predD80_vs_realised %>%
      st_buffer(dist = 2e4) %>%
      st_union() %>%
      c(., . + c(5e4, 0))  # Add some margin to the east
  )
) +
  tm_layout(bg.color = "lightblue") +
  # tm_grid(col = "grey", labels.size = 1) +
  tm_scale_bar(
    breaks = c(0, 10, 30, 60),
    text.size = 0.9,
    position = c("right", "bottom")
  ) +
  tm_polygons() +
  ## Pig sites
  tm_shape(
    outbreaks_predD80_vs_realised %>% filter(predicted)
  ) +
  tm_bubbles(
    size = "p_pred",
    col = "Infection",
    scale = 2,
    popup.vars = c("DET", "p_pred", "size", "practice", "wb_expo", "trade", "fomites"),
    legend.size.show = FALSE
  ) +
  tm_shape(
    outbreaks_predD80_vs_realised %>% filter(!predicted)
  ) +
  tm_bubbles(
    border.col = "darkred", col = "Infection", size = .1,
    border.lwd = 2, legend.col.show = FALSE
  ) +
  ## Fences
  tm_shape(c(st_geometry(fence), st_geometry(huntZone))) +
  tm_borders()

print(fig1)

######################################################################################################
## Fig 2:  Region at risk for outdoor farm contamination due to proximity to infected wild boar.    ##
## The background colour represents a kernel density smoothing of observed wild boar cases.         ##
## Red dots are the locations of WB cases. The larger white                                         ##
## points are outdoor farms. Those already infected and detected by day                             ##
## 110 are marked with a black dot. 'kde' (legend) indicates 'kernel density estimate,              ##
## and is used to quantify the exposition level of outdoor farms.                                       ##
######################################################################################################
infected_sites_region <-
  outbreaks_at_D110 %>%
  filter(HOST == "pig herd") %>%
  select(ID) %>%
  st_set_agr("identity") %>%
  st_intersection(st_union(wb_case_density_D110))

fig2 <- ggplot() +
  ## WB density surface
  geom_sf(
    data = wb_case_density_D110,
    aes(colour = kde, fill = kde),
    lwd = 0
  ) +
  ## Fence and hunting zone
  geom_sf(data = fence, fill = NA, colour = "gray80") +
  geom_sf(data = huntZone, fill = NA, colour = "gray80") +
  ## WB cases
  geom_sf(
    data = outbreaks_at_D110 %>% filter(HOST == "wild boar"),
    col = "red"
  ) +
  ## Farm sites (outdoor) predicted at risk
  geom_sf(
    data = pig_sites_pred_CS00_D110 %>% filter(susceptible_wb),
    col = "gray90",
    size = 3
  ) +
  ## Farm sites predicted at risk via fomites
  ## Infected farm sites
  geom_sf(
    data = infected_sites_region,
    ) +
  geom_text_repel(
    # Awesome trick to use ggrepel with geom_sf.
    # https://github.com/slowkow/ggrepel/issues/111#issuecomment-416853013
    data = infected_sites_region,
    aes(label = ID, geometry = geometry),
    stat = "sf_coordinates"
  ) +
  ## Scales and aesthetics
  scale_fill_viridis_c() +
  scale_colour_viridis_c() +
  ggsn::scalebar(
    x.min = min(st_coordinates(wb_case_density_D110)[, "X"]),
    x.max = max(st_coordinates(wb_case_density_D110)[, "X"]),
    y.min = min(st_coordinates(wb_case_density_D110)[, "Y"]),
    y.max = max(st_coordinates(wb_case_density_D110)[, "Y"]),
    dist = 10,
    dist_unit = "km",
    transform = FALSE,
    st.size = 3
  ) +
  labs(x = NULL, y = NULL)

print(fig2)



#####################################################################
## Fig 3: Probability of farm infection and detection (curve),     ##
## estimated from infection status for outdoor farms (points)      ##
## in the region at risk, as a function of their exposition level. ##
#####################################################################
fig3 <- pig_sites_risks_D110 %>%
  filter(susceptible_wb) %>%
  mutate(
    detected = as.numeric(detected)
  ) %>%
  left_join(
    outbreaks_at_D110 %>%
      filter(HOST == "pig herd") %>%
      st_drop_geometry() %>%
      transmute(
        population_id = ID,
        Infection = ifelse(
          DATE.CONF <= 80,
          "Prior D80", "New"
        )
      ),
    by = "population_id"
  ) %>%
  replace_na(list(Infection = "Not infected")) %>%
  ggplot(aes(expo_inf_wb, detected)) +
  geom_point(aes(group = population_id, colour = Infection)) +
  geom_line(
    data = data.frame(
      expo_inf_wb = seq(
        min(pig_sites_pred_CS00_D110$expo_inf_wb),
        max(pig_sites_pred_CS00_D110$expo_inf_wb),
        length = 101
      )
    ) %>%
      mutate(
        pred = predict(fm_farm_inf_D110, type = "response", newdata = .)
      ),
    aes(expo_inf_wb, pred)
  ) +
  labs(x = "Exposition level", y = "Detection probability") +
  theme(
    panel.grid.minor.x = element_blank(),
    panel.grid.minor.y = element_blank(),
    panel.grid.major.x = element_blank()
  )

print(fig3)


####################################################################################
## Fig 4: Probability of detection due to exposition to infected wild boar during ##
## the prediction period (D110 - D230), for outdoor farms in the area at risk.    ##
####################################################################################
fig4 <- tm_shape(
  admin,
  bbox = plot_area(
    pig_sites_pred_CS00_D110 %>% filter(susceptible_wb),
    5e4, 1e4
  )
) +
  tm_layout(bg.color = "lightblue") +
  # tm_grid(col = "grey", labels.size = 1) +
  tm_scale_bar(
    breaks = c(0, 10, 30, 60),
    text.size = 0.9,
    position = c("left", "top")) +
  tm_polygons() +
  tm_shape(pig_sites_pred_CS00_D110 %>% filter(susceptible_wb)) +
  tm_bubbles(
    title.size = "Prob. of detection",
    size = "p_pred",
    col = "detected",
    scale = 2
  ) +
  tm_shape(fence) +
  tm_borders() +
  tm_shape(huntZone) +
  tm_borders() +
  tm_legend(
    bg.color = "white",
    bg.alpha = .7,
    frame = TRUE
  )

print(fig4)


###################################################################################
## Fig 5: Cartographic representation of observed animal shipments (at any time) ##
## with risk of ASFV transmission (from farm sites at risk).                     ##
###################################################################################
fig5 <- tmAdmin +
  tm_shape(pig_sites_pred_CS00_D110 %>% filter(susceptible_wb)) +
  tm_bubbles(
    title.col = "Detected",
    col = "detected",
    size = 0.2,
    palette = "viridis"
  ) +
  tm_shape(
    pig_sites %>%
      right_join(
        farm_pred_inf_ship_CS00_D110,
        by = c(population_id = "dest")
      )
  ) +
  tm_bubbles(
    title.size = "Exp.# of infectious shipments.",
    size = "e_inf_shipments"
  ) +
  tm_shape(
    animal_movements %>%
      filter(
        source %in%
          filter(pig_sites_pred_CS00_D110, susceptible_wb)$population_id,
        dest %in% farm_pred_inf_ship_CS00_D110$dest)
  ) +
  tm_lines() +
  tm_shape(fence) +
  tm_borders() +
  tm_shape(huntZone) +
  tm_borders() +
  tm_legend(
    bg.color = "white",
    bg.alpha = .7,
    frame = TRUE
  )

print(fig5)



###############################################################################################
## Fig 6: Probability of detection due to exposition to fomites during the prediction period ##
## (D110 - D230). The rectangles represent the fenced (inner) and the buffer zones (outer).  ##
###############################################################################################
sites_fomites <-
  pig_sites_pred_CS00_D110 %>%
  mutate(
    p_fomites = prob_fomites(p_inf, unclass(st_distance(.)))
  ) %>%
  filter(p_fomites > 0.01 | detected)

fig6 <- tm_shape(admin, bbox = plot_area(sites_fomites, 7e4, 1e4)) +
  tm_layout(bg.color = "lightblue") +
  tm_scale_bar(
    breaks = c(0, 10, 30, 60),
    text.size = 0.9,
    position = c("left", "top")) +
  tm_polygons() +
  tm_shape(sites_fomites) +
  tm_bubbles(
    title.size = "Prob. of detection",
    size = "p_fomites",
    col = "detected",
    scale = 2
  ) +
  tm_shape(fence) +
  tm_borders() +
  tm_shape(huntZone) +
  tm_borders() +
  tm_legend(
    bg.color = "white",
    bg.alpha = .7,
    frame = TRUE
  ) +
  tm_layout(
    legend.position = c("right", "top")
  )


print(fig6)


#########################################################################################
## Table 2: Probability of infection in undetected farm sites with non-negligible      ##
## probability (P > 0.05) in the next four months. The last column (r\\_diff)          ##
## is the relative potential of dissemination of the farm over the commercial network. ##
#########################################################################################
tab_predictions <-
  tab_farms_pred_CS00_D110 %>%
  filter(p_pred > 0.05, !detected) %>%
  select(-detected) %>%
  ## Risk of diffusion in the trade network if infected
  left_join(rim %>% select(id, r_diff), by = "id") %>%
  replace_na(list(r_diff = 0))

kable(
  tab_predictions,
  caption = "Probability of infection in undetected farm sites with non-negligible probability (P > 0.05) in the next four months. The last column (r\\_diff) is the relative potential of dissemination of the farm over the commercial network.",
  format = "latex",
  format.args = list(zero.print = ""),
  digits = 3,
  booktabs = TRUE
)


###############################################################################
## Fig 7: Probability of infection and detection by any possible route       ##
## during the prediction period (D110 - D230).                               ##
## The rectangles represent the fenced (inner) and the buffer zones (outer). ##
###############################################################################
sites_all <-
  pig_sites_pred_CS00_D110 %>%
  select(-detected, -size, -p_pred) %>%
  inner_join(
    bind_rows(
      tab_farms_pred_CS00_D110 %>%
        filter(p_pred > 0.01, !detected),
      tab_farms_pred_CS00_D110 %>%  # Missing the detection in the north
        filter(detected)
    ),
    by = c(population_id = "id")
  )

fig7 <- tm_shape(admin, bbox = plot_area(sites_all, 7e4, 1e4)) +
  tm_layout(bg.color = "lightblue") +
  # tm_grid(col = "grey", labels.size = 1) +
  tm_scale_bar(
    breaks = c(0, 10, 30, 60),
    text.size = 0.9,
    position = c("right", "bottom")) +
  tm_polygons() +
  tm_shape(sites_all) +
  tm_bubbles(
    title.size = "Prob. of detection",
    size = "p_pred",
    col = "detected",
    scale = 2
  ) +
  tm_shape(fence) +
  tm_borders() +
  tm_shape(huntZone) +
  tm_borders() +
  tm_legend(
    bg.color = "white",
    bg.alpha = .7,
    frame = TRUE
  ) +
  tm_layout(
    legend.position = c("right", "top")
  )

print(fig7)

###########################################################
## Fig 8: Cumulative fraction of transmission potential. ##
###########################################################
fig8 <- rim %>%
  filter(r_diff > 0) %>%
  ggplot(aes(rank, cumr)) +
  geom_path(
    data = function(x)
      filter(x, cumr >= 80) %>%
        slice(1) %>%
        with(., data.frame(
          rank = c(0, rank, rank),
          cumr = c(cumr, cumr, 0)
        )),
    colour = "gray80"
  ) +
  geom_line() +
  labs(
    x = "Ranking of farms",
    y = expression(Cumulative~fraction~of~R[0])
  )

print(fig8)



##########################################################################################
## Fig 9: Farm hubs, with high potential of virus transmission through the trade netwok ##
##########################################################################################
hubs_at_risk <-
  pig_sites %>%
  select(id = population_id, practice) %>%
  right_join(tab_predictions, by = "id") %>%
  filter(r_diff > 0)

fig9 <- tm_shape(admin, bbox = plot_area(hubs_at_risk, 5e4, 1e4)) +
  tm_layout(bg.color = "lightblue") +
  tm_scale_bar(
    breaks = c(0, 10, 30),
    text.size = 0.9,
    position = c("right", "bottom")) +
  tm_polygons() +
  tm_shape(hubs_at_risk) +
  tm_bubbles(
    title.size = "Transmission\npotential",
    title.col = "Prob. of\ninfection",
    size = "r_diff",
    scale = 2,
    col = "p_pred",
    palette = "viridis"
  ) +
  tm_shape(fence) +
  tm_borders() +
  tm_shape(huntZone) +
  tm_borders() +
  tm_legend(
    bg.color = "white",
    bg.alpha = .7,
    frame = TRUE
  )

print(fig9)


#################
## Fig 10 left ##
#################
fig10L <- mapWildBoarCasesDayX(aggWildBoarArrayAll,
                               mapArea = mapAreaR3, Hex=hexAll, Day=110, StepsPerChunk=1,
                               Admin = admin, legendOutside = TRUE, returnMap = TRUE,
                               caption= "Obs_SumCases", Fence=fence, HuntZone=huntZone)
print(fig10L)

##################
## Fig 10 right ##
##################
fig10R <-mapWildBoarCasesDayX(simIWB_meanWBCasesObs_sc1_report2, legendMax = 45,
                              mapArea = mapAreaR3, Hex=hexAll, Day=110, StepsPerChunk=1,
                              Admin = admin, legendOutside = TRUE, returnMap = TRUE,
                              caption= "E_SumCases", Fence=fence, HuntZone=huntZone)

print(fig10R)


#####################################################################################
## Fig 11: Difference between true and simulated cummulative incidence of observed ##
## infectious wild boar (up to day 110), obtained from fitting the model to        ##
## 80 days of data (as described in report 2                                       ##
#####################################################################################
diff110_report2 = aggWildBoarArrayAll[1:111,] - simIWB_meanWBCasesObs_sc1_report2[1:111,]

fig11 = mapWildBoarCasesDayX(diff110_report2, allowNegative = TRUE,
                             mapArea = mapAreaR3, Hex=hexAll, Day=110, StepsPerChunk=1,
                             Admin = admin, legendOutside = TRUE, returnMap = TRUE,
                             caption= "Diff", Fence=fence, HuntZone=huntZone)
print(fig11)


#################
## Fig 12 left ##
#################
fig12L = mapWildBoarCasesDayX(aggWildBoarArrayAll,
                              mapArea = mapAreaR3, Hex=hexAll, Day=110, StepsPerChunk=1,
                              Admin = admin, legendOutside = TRUE, returnMap = TRUE,
                              caption= "Obs_SumCases", Fence=fence, HuntZone=huntZone)

print(fig12L)

##################
## Fig 12 right ##
##################
fig12R = mapWildBoarCasesDayX(simIWB_meanWBCasesObs_sc1_report3, legendMax = 45,
                              mapArea = mapAreaR3, Hex=hexAll, Day=110, StepsPerChunk=1,
                              Admin = admin, legendOutside = TRUE, returnMap = TRUE,
                              caption= "E_SumCases", Fence=fence, HuntZone=huntZone)

print(fig12R)


###############################################################################################################################################################
## Fig 13: Difference between true and simulated cummulative incidence of observed infectious wild boar, obtained from fitting the model to 110 days of data ##
###############################################################################################################################################################
diff110_report3 = aggWildBoarArrayAll[1:111,] - simIWB_meanWBCasesObs_sc1_report3[1:111,]

fig13 = mapWildBoarCasesDayX(diff110_report3, # 111 x 6652
                                            allowNegative = TRUE,
                                            mapArea = mapAreaR3, Hex=hexAll, Day=110, StepsPerChunk=1,
                                            Admin = admin, legendOutside = TRUE, returnMap = TRUE,
                                            caption= "Diff", Fence=fence, HuntZone=huntZone)
print(fig13)


###################################################################################################
## Fig 14: Trajectories of the expected number of infectious wild boar (IWB) by scenario.        ##
## Scenarios that included increased hunting pressure (1 and 2) proved the most effective.       ##
## The fence without extra hunting (3) provides some benefit.                                    ##
## The scenarios with no control measures (4) would be the least effective.                      ##
## Note, we have not presented variation about the expected value, but can do so if requested to ##
###################################################################################################
nScenarios <- nrow(scenariosWBR3)
days <- as.integer(sub("D","",row.names(IWB_scenarios_report3)))
plot(days, IWB_scenarios_report3[,1], typ="n", xlab="Day", ylab="Total IWB", ylim=c(0,1.2*max(IWB_scenarios_report3)))
myCols = rainbow(nScenarios)
for (ii in 1:nScenarios)
  lines(days, IWB_scenarios_report3[,ii], col=myCols[ii])
abline(v=c(60, 120, 204),col="lightgrey")
abline(h=0)
legend("topright", col=myCols, lty=1,
       cex = 0.8, box.col = "white", bg = "white",
       legend=c("Scenario 1: Fence + Hunting","Scenario 2: Hunting Only","Scenario 3: Fence Only","Scenario 4: No Control"))
box()


####################################################################################################################################################################################################################
## Fig 15: Posterior distribution for the number of infectious wild boar at day 230 under the current control scenario. The proportion of simulations with zero infectious wild boar at day 230 was ", pZero, "." ##
####################################################################################################################################################################################################################
pZero = signif(1 - globalProbHaveIWB_sc1_report3, 2)

filteredRowSums <- rowSums(simIWBspatial_sc1_report3)
#filteredRowSums <- filteredRowSums[filteredRowSums>0]
hist_simIWBspatial_sc1_report3 =
  hist(filteredRowSums, freq=FALSE,
       # breaks=0:max(pretty(rowSums(simIWBspatial_sc1_report3))),
       breaks=seq(-9, max(pretty(rowSums(simIWBspatial_sc1_report3))), by=10),
       xlab="Number of IWB on day 230", main="")


#########################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################
## Fig 16: paste0("Temporal trajectories of the total number of infectious wild boar (IWB) from ", nrow(simMat), " simulations. Vertical grey lines correspond to: fence completion and start of army intervention (day 60); the end of army intervention (day 120); and the end of the hunting season (day 204). The red line indicates the proportion of ", nSims4PZero, " simulations with zero infectious wild boar on any given day -- this proides a maxium possible value (upper bound) for the probability of the outbreak coming to an end.") ##
#########################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################
nSims4Plot <- 1000
simMat <- simIWBtemporal_sc1_report3
pZero <- colMeans(simMat == 0)
nSims4PZero <- nrow(simMat)
iSubSet <- sample(nrow(simMat), nSims4Plot)
simMat <- simMat[iSubSet,]
yLims = range(pretty(as.matrix(simMat)))
yMax = max(yLims)
cexLab = 1.2
par(mar = c(4.8, 4.3, 3, 4.3), cex.axis=1.2, cex.lab=cexLab)
lineCol = rgb(0,0,0,0.2)
plot((1:ncol(simMat))-1, simMat[1,], typ="l", ylim=yLims,
     col=lineCol,
     xlab="Time (day)", ylab="Nb. Infectious Wild Boar (IWB)")
abline(v=c(60, 120, 204),col="lightgrey")
for (iSim in 2:nrow(simMat))
  lines((1:ncol(simMat))-1, simMat[iSim,], col=lineCol, typ="l")
par(new = TRUE)
plot((1:ncol(simMat))-1, pZero, typ="l", col="red", xlab = "", ylab = "", axes=FALSE)
axis(side = 4, at = pretty(range(pZero)))
mtext("Proportion of simulations with zero IWB", side = 4, line = 3, cex=cexLab)


############################################################################################
## Fig 17: Standard deviation in the simulated number of infectious wild boar on days 110 ##
## (top left), 150 (top right), 190 (bottom left) and 230 (bottom right). These figures   ##
## indicate that the principal zone at risk of a second wave is located to the south and  ##
## south-west of the current fence and zone of increased hunting pressure (rectangles)    ##
############################################################################################
mapSimStDevI_sc1_report3_day110 = mapInfectiousDayX(simIWB_stDevI_sc1_report3, Hex=hexAll, mapArea = mapAreaR3, Admin = admin,
                                                    Day=110, StepsPerChunk=1, legendOutside = TRUE, returnMap = TRUE,
                                                    caption= "StDev_I", Fence=fence, HuntZone=huntZone)

mapSimStDevI_sc1_report3_day150 = mapInfectiousDayX(simIWB_stDevI_sc1_report3, Hex=hexAll, mapArea = mapAreaR3, Admin = admin,
                                                    Day=150, StepsPerChunk=1, legendOutside = TRUE, returnMap = TRUE,
                                                    caption= "StDev_I", Fence=fence, HuntZone=huntZone)

mapSimStDevI_sc1_report3_day190 = mapInfectiousDayX(simIWB_stDevI_sc1_report3, Hex=hexAll, mapArea = mapAreaR3, Admin = admin,
                                                    Day=190, StepsPerChunk=1, legendOutside = TRUE, returnMap = TRUE,
                                                    caption= "StDev_I", Fence=fence, HuntZone=huntZone)

mapSimStDevI_sc1_report3_day230 = mapInfectiousDayX(simIWB_stDevI_sc1_report3, Hex=hexAll, mapArea = mapAreaR3, Admin = admin,
                                                    Day=230, StepsPerChunk=1, legendOutside = TRUE, returnMap = TRUE,
                                                    caption= "StDev_I", Fence=fence, HuntZone=huntZone)

print(mapSimStDevI_sc1_report3_day110)
print(mapSimStDevI_sc1_report3_day150)
print(mapSimStDevI_sc1_report3_day190)
print(mapSimStDevI_sc1_report3_day230)


#################################################################################
## Fig 18: Expected wild boar density across Merry Island, repressented on the ##
## 'log plus one' scale. The fenced area and its 15km buffer are shown         ##
#################################################################################
fig18 = tm_shape(admin) + tm_layout(bg.color = "lightblue") +
    tm_polygons() +
    tm_shape(hexAll) +
    tm_polygons("log10_Ewb1", title="log10(E[WB] + 1)", n = 20, lwd=0.01) +
    tm_shape(fence) + tm_borders(col="darkviolet", lwd=1.6) +
    tm_grid(col = "grey", labels.show = FALSE, n.x=4, n.y=2) +
    tm_shape(huntZone) + tm_borders(col="red", lwd=1.6) +
    tm_layout(legend.position = c("left", "bottom"),
              legend.outside=TRUE, legend.bg.color = "white") +
    tm_scale_bar(breaks = c(0, 50, 100, 150, 200),
                 text.size = 0.9, position = c("right", "BOTTOM"))

print(fig18)




###################
## Drake objects ##
###################
## drake::loadd(
##   admin,
##   aggWildBoarArrayAll,
##   animal_movements,
##   corPHomeBeta,
##   farm_pred_inf_ship_CS00_D110,
##   fence,
##   fm_farm_inf_D80,
##   fm_farm_inf_D110,
##   globalProbHaveIWB_sc1_report3,
##   hexAll,
##   huntZone,
##   huntedWBzoom,
##   IWB_scenarios_report3,
##   mapAreaR3,
##   mapCaseNorth,
##   mapDiff110_report2,
##   mapDiff110_report3,
##   map_expectedI_scenario1_140,
##   map_expectedI_scenario2_140,
##   map_expectedI_scenario3_140,
##   map_expectedI_scenario4_140,
##   nMCMCmod11,
##   nMCMCmod12,
##   outbreaks_at_D110,
##   pig_sites,
##   pig_sites_pred_CS00_D50,
##   pig_sites_pred_CS00_D80,
##   pig_sites_pred_CS00_D110,
##   pig_sites_risks_D80,
##   pig_sites_risks_D110,
##   pigSitesExposure,
##   shipment_nw,
##   tab_farms_pred_CS00_D110,
##   tab_farms_pred_CSIF_D80,
##   scenariosWBR3,
##   simIWBspatial_sc1_report3,
##   simIWBtemporal_sc1_report3,
##   simIWB_meanWBCasesObs_sc1_report2,
##   simIWB_meanWBCasesObs_sc1_report3,
##   simIWB_stDevI_sc1_report3,
##   tab_probAS,
##   tmAdmin,
##   wb_case_density_D80,
##   wb_case_density_D110
## )

## save(admin,
##   aggWildBoarArrayAll,
##   animal_movements,
##   corPHomeBeta,
##   farm_pred_inf_ship_CS00_D110,
##   fence,
##   fm_farm_inf_D80,
##   fm_farm_inf_D110,
##   globalProbHaveIWB_sc1_report3,
##   hexAll,
##   huntZone,
##   huntedWBzoom,
##   IWB_scenarios_report3,
##   mapAreaR3,
##   mapCaseNorth,
##   mapDiff110_report2,
##   mapDiff110_report3,
##   map_expectedI_scenario1_140,
##   map_expectedI_scenario2_140,
##   map_expectedI_scenario3_140,
##   map_expectedI_scenario4_140,
##   nMCMCmod11,
##   nMCMCmod12,
##   outbreaks_at_D110,
##   pig_sites,
##   pig_sites_pred_CS00_D50,
##   pig_sites_pred_CS00_D80,
##   pig_sites_pred_CS00_D110,
##   pig_sites_risks_D80,
##   pig_sites_risks_D110,
##   pigSitesExposure,
##   shipment_nw,
##   tab_farms_pred_CS00_D110,
##   tab_farms_pred_CSIF_D80,
##   scenariosWBR3,
##   simIWBspatial_sc1_report3,
##   simIWBtemporal_sc1_report3,
##   simIWB_meanWBCasesObs_sc1_report2,
##   simIWB_meanWBCasesObs_sc1_report3,
##   simIWB_stDevI_sc1_report3,
##   tab_probAS,
##   tmAdmin,
##   wb_case_density_D80,
##   wb_case_density_D110,
##   file="simsCiradInraeTeamRound3.Rdata")
