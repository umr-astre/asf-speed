## TO-DO
## - visualise relation between passive and active search
## - adapt to hexagonal grid

bugsBoarCode = nimbleCode({
  ## Known Parameters
  omega  <- 0.8                               # Habitat preference
  tauI   <- 1/7                               # Incubation rate
  tauC   <- 1/7                               # Disease induced mortality rate
  tauR   <- tauC * pR/(1 - pR)                # Recovery rate so that pR (recovfery probability ) is 0.05
  tauRot <- 1/90                              # Carcass decomposition rate
  tauH   <- -log((1 - pHuntY)^(12/(8 * 365))) # Hunting rate. Hunters think pHuntY=0.5 (prob. shot in one year). Hunting season is 8 month long.
  pTest  <- 20/100                            # Probability to test a hunted wild boar
  tDelayActiveSearch <- 10                    # 3 days for the test and 7 days to organise search. 100% constant in their simulaed data.
  decayTauA <- (1 - 1/7)                        # Geometric decay of tauA
  ## Unknown Parameters
  logit(scTIntro)   ~ dLogitBeta(7, 7)         # Introduction date (on 0:1)
  logit(pIntroX)    ~ dLogitBeta(1, 1)         # Introduction location relative to bounding box of whole island
  logit(pIntroY)    ~ dLogitBeta(1, 1)         # Introduction location relative to bounding box of whole island
  logit(pHome)      ~ dLogitBeta(2, 2)         # Connectivity: pHome=p(wild boar in home cell). (1-pHome)=p(wild boar in neighbouring cell)
  logit(attractI)   ~ dLogitBeta(1, 1)         # Relative contact rate, i.e. how attractive is an infectious individual relative to a carcass.
  logit(omegaFence) ~ dLogitBeta(2, 2)         # Fence efficacy
  log(tauP)         ~ dLogExp(rate = 1/1e+07)  # Carcass detection rate - passive detection
  log(tauA)         ~ dLogExp(rate = 1/1e+07)  # Carcass detection rate - active detection
  log(tauHArmy)     ~ dLogExp(rate = 1/1e+07)  # Increase in hunting rate around fenced area after day 60
  transPHome2beta   ~ dLogExp(rate = 1/1e+07)  # The scale of transmission - multiply with the realtive attractivity of Infectious and Carcasses.
  beta   <- exp(logit_pHome + transPHome2beta) # Reparameterisation to hopefully account for a correlation of ~0.99
  betaI  <- beta * attractI
  betaC  <- beta * (1 - attractI)
  tIntro <- tMin - scTIntro * tMin
  xIntro <- xMin + pIntroX * (xMax - xMin)
  yIntro <- yMin + pIntroY * (yMax - yMin)
  ## Somewhat known, can be fixed for simplicity
  logit(pR)     ~ dLogitBeta(5/2.18, 95/2.18)
  logit(pHuntY) ~ dLogitBeta(330, 330)
  ## Other constants
  tFence <- 60  # Last day without fence and extra hunting
  ## Observed &/or Simulated Data
  obsY[1:(nObsStates + returnI), 1:nTimeChunksPlus1, 1:nPops] <- simulateObservations(
    tIntro = tIntro, xIntro = xIntro, yIntro = yIntro,
    nObsStates = nObsStates, returnI = returnI, nPops = nPops, nTimeChunksPlus1 = nTimeChunksPlus1,
    stepsPerChunk = stepsPerChunk, xCentroid = xCentroid[1:nPops],
    yCentroid = yCentroid[1:nPops], weightF = weightF[1:nPops],
    weightA = weightA[1:nPops], areaHuntBag = areaHuntBag[1:nPops],
    omega = omega, betaI = betaI, betaC = betaC, pHome = pHome,
    pR = pR, tauA = tauA, tauP = tauP, tauI = tauI, tauC = tauC,
    tauR = tauR, tauRot = tauRot, tauH = tauH, tauHArmy = tauHArmy, pHuntY = pHuntY,
    pTest = pTest, nNeigh = nNeigh[1:nPops], neighVec = neighVec[1:lNeighVec],
    neighStart = neighStart[1:nPops], neighEnd = neighEnd[1:nPops],
    seperatedByFence = seperatedByFence[1:lNeighVec],
    omegaFence = omegaFence, tFence = tFence, lNeighVec = lNeighVec,
    inHuntZone = inHuntZone[1:nPops], probNeighAS = probNeighAS,
    tDelayActiveSearch = tDelayActiveSearch, decayTauA = decayTauA)
  # For monitor2
  loglik <- 0
})


setWildBoarModelConstants <- function (wbArray4sim, Hex, HexCentroids, Outbreaks, stepsPerChunk, bboxAll, tStop, returnI=FALSE, probNeighAS) {
  ## wbArray4sim - an element of {wildBoarArrayAll, wildBoarArraySub}
  xlims = bboxAll[c("xmin","xmax")]
  ylims = bboxAll[c("ymin","ymax")]
  nObsStates    = dim(wbArray4sim)[1]
  if (missing(tStop)) {
    nTimeChunksPlus1 = dim(wbArray4sim)[2]
  } else {
    nTimeChunks = ceiling(tStop / stepsPerChunk)
    nTimeChunksPlus1 = nTimeChunks + 1
  }
  nPops         = dim(wbArray4sim)[3]
  if (nPops != nrow(Hex)) stop("The following must be true: dim(wbArray4sim)[3] == nrow(Hex)")
  #
  const = list(
    nPops            = nPops,                    # Number of hexagonal patches
    nObsStates       = nObsStates,               # Number of observation types for wild boar, "NT" "PT" "AS" "PS" (we drop NAS)
    returnI          = returnI,                  # Indicator: FALSE - simulations don't return I, TRUE - simulations do return I
    nTimeChunksPlus1 = nTimeChunksPlus1,         # Number of bins for aggregating time-step data
    stepsPerChunk    = stepsPerChunk,            # Number of timesteps for each aggregated bin
    weightF          = Hex$weightF,              # Weight for estimating wild boar density (from admin area's hunting bag) based on forest cover in hexagon / total forest cover in admin area.
    weightA          = Hex$weightA,              # Weight for estimating wild boar density (from admin area's hunting bag) based on agro   cover in hexagon / total agro   cover in admin area.
    areaHuntBag      = Hex$adminAreaHuntBag,     # Hunting bag data - to be divided by all hexagons from a given admin area
    nNeigh           = Hex$nNeigh,               # Number of neighbours for each hexagon
    neighStart       = Hex$neighStart,           # Indicates start of sub-vector of neighVec for each hexagonal patch
    neighEnd         = Hex$neighEnd,             # Indicates end   of sub-vector of neighVec for each hexagonal patch
    neighVec         = attributes(Hex)$neighVec, # A vector containing indices for neighbours
    lNeighVec        = length(attributes(Hex)$neighVec),
    xCentroid        = (HexCentroids %>% st_coordinates())[,"X"], # X component of each hexagon's centroid
    yCentroid        = (HexCentroids %>% st_coordinates())[,"Y"], # Y component of each hexagon's centroid
    tMin             = -100,                                      # Lower bound on carcass-fall-from-space day
    xMin             = xlims[1],
    xMax             = xlims[2],
    yMin             = ylims[1],
    yMax             = ylims[2],
    seperatedByFence = attributes(Hex)$seperatedByFence,
    inHuntZone       = 1.0 * Hex$inHuntZone,
    probNeighAS      = probNeighAS
    ## x0            = as.double((Outbreaks %>% filter(DATE.SUSP==0) %>% st_coordinates())[1,"X"]), # X coordinate of first observed infected farm
    ## y0            = as.double((Outbreaks %>% filter(DATE.SUSP==0) %>% st_coordinates())[1,"Y"])  # Y coordinate of first observed infected farm
  )
  const
}

setWildBoarModelInitialValues <- function (wbArray4sim, wildBoarObs, stepsPerChunk, tStop, bboxAll, returnI=FALSE) {
  ## browser()
  ## wbArray4sim - an element of {wildBoarArrayAll, wildBoarArraySub}
  nObsStates       = dim(wbArray4sim)[1]
  nTimeChunksPlus1 = dim(wbArray4sim)[2]
  nPops            = dim(wbArray4sim)[3] # = nrow(Hex) # Same value
  nTimeChunks      = nTimeChunksPlus1 - 1
  nReturnStates    = nObsStates + returnI
  #
  if (missing(tStop)) {
    nimPrint("USING ORIGINAL OBSERVATIONS IN setWildBoarModelInitialValues")
    nimPrint("dim(obsY)[2] = ", nTimeChunksPlus1)
    tStop  = nTimeChunks * stepsPerChunk
    myObsY = nimArray(value=0, dim=c(nReturnStates, nTimeChunksPlus1, nPops))
    myObsY[1:nObsStates, 1:nTimeChunksPlus1, 1:nPops] = wbArray4sim[1:nObsStates, 1:nTimeChunksPlus1, 1:nPops] # These nodes are first initialised with observed data, but the data will be replaced each time this node is simulated.
  } else if (nTimeChunks <= ceiling(tStop / stepsPerChunk)) {
    nTimeChunks      = ceiling(tStop / stepsPerChunk)
    nTimeChunksPlus1 = nTimeChunks + 1
    nimPrint("NOT USING ORIGINAL OBSERVATIONS IN setWildBoarModelInitialValues. IF THIS IS NOT WHAT YOU WANT THEN DELETE tStop ARGUMENT.")
    nimPrint("setting dim(obsY)[2] to ", nTimeChunksPlus1)
    myObsY = nimArray(value=0, dim=c(nReturnStates, nTimeChunksPlus1, nPops))
  } else {
    stop("Please ensure ceiling(tStop / stepsPerChunk) >= dim(wbArray4sim)[2] in order to simulate at least as far as original data.")
  }
  # Ball-park Initial value for location of first infectious carcass
  xIntro = (wildBoarObs %>% filter(is.element(DET, c("AS","PS","PT"))) %>% st_coordinates() %>% colMeans())["X"]
  yIntro = (wildBoarObs %>% filter(is.element(DET, c("AS","PS","PT"))) %>% st_coordinates() %>% colMeans())["Y"]
  xMin = bboxAll["xmin"]
  xMax = bboxAll["xmax"]
  yMin = bboxAll["ymin"]
  yMax = bboxAll["ymax"]
  #
  inits = list(
    obsY             = myObsY[1:nReturnStates, 1:nTimeChunksPlus1, 1:nPops],
    logit_scTIntro   = logit(0.75),  # Time first infectious carcass fell from space
    logit_pIntroX    = logit((xIntro-xMin)/(xMax-xMin)),
    logit_pIntroY    = logit((yIntro-yMin)/(yMax-yMin)),
    logit_pHome      = logit(1/7),   # Connectivity
    logit_attractI   = logit(0.5),   # Attraction of Infectious indivuidual (relative to a Carcass)
    logit_omegaFence = logit(0.999),
    logit_pHuntY     = logit(0.5),   # Probability a wild boar is shot within one year's 8 month hunting season
    logit_pR         = logit(0.05),  # Recovery probability. FIXED
    log_tauP         = log(0.0001),  # Detection rate for passive detection
    log_tauA         = log(0.001),   # Detection rate for active detection
    log_tauHArmy     = -7,
    transPHome2beta  = -8.1          # beta <- exp(logit_pHome + transPHome2beta)
  )
  inits
}


######################################################
## Stochastic simulation functions - wild boar only ##
######################################################
simulateObservations = nimbleFunction (
  run = function (tIntro = double(0), xIntro = double(0), yIntro = double(0),
                  nObsStates = integer(0),
                  returnI = logical(0),       ## If TRUE, an additional output for I is returned.
                  nPops = integer(0), nTimeChunksPlus1 = double(0), stepsPerChunk = double(0),
                  xCentroid = double(1), yCentroid = double(1),
                  weightF = double(1), weightA = double(1),
                  areaHuntBag = double(1), omega = double(0), betaI = double(0), betaC = double(0),
                  pHome = double(0), pR = double(0), tauA = double(0), tauP = double(0),
                  tauI = double(0), tauC = double(0), tauR = double(0),
                  tauRot = double(0), tauH = double(0),
                  tauHArmy = double(0), pHuntY = double(0), pTest = double(0),
                  nNeigh = double(1), neighVec = double(1),
                  neighStart = double(1), neighEnd = double(1),
                  seperatedByFence = double(1), # Must be same length as neighVec
                  omegaFence = double(0),
                  tFence = double(0, default=1E6),
                  lNeighVec = integer(0),
                  inHuntZone = double(1),
                  probNeighAS = double(0),
                  tDelayActiveSearch = integer(0),
                  decayTauA = double(0) ) {
    # Indices for states
    S             = 1
    E             = 2
    I             = 3
    R             = 4
    C             = 5
    Rot           = 6
    Hu            = 7
    Hneg          = 8  # NT in docs
    Hpos          = 9  # PT in docs
    Sa            = 10 # AS in docs
    Sp            = 11 # PS in docs
    nStates       = 11
    obState1      = nStates - nObsStates + 1
    obState4      = nStates
    zero4Obs      = nimNumeric(value=0, length=nObsStates)
    nReturnStates = nObsStates + returnI
    # Indices for rates - individual
    S_Infection   = 1
    S_HuntTestNeg = 2
    S_HuntUntest  = 3
    E_Incubation  = 4
    E_HuntTestPos = 5
    E_HuntUntest  = 6
    I_Recover     = 7
    I_HuntTestPos = 8
    I_HuntUntest  = 9
    I_Death       = 10
    C_Rot         = 11
    C_DetActive   = 12
    C_DetPassive  = 13
    nRates        = 13
    # Indices for rates - vector
    iSrates = 1:3
    iErates = 4:6
    iIrates = 7:10
    iCrates = 11:13
    iSmin   = 1
    iSmax   = 3
    iEmin   = 4
    iEmax   = 6
    iImin   = 7
    iImax   = 10
    iCmin   = 11
    iCmax   = 13
    ## Establish
    dtDynamics1 = 1.0                      # Resolution of discretisation of dynamic process
    dtDynamics  = abs(ceiling(tIntro) - tIntro) # Duration of process in first time step only
    if (dtDynamics==0)
      dtDynamics = dtDynamics1
    nTimeChunks        = nTimeChunksPlus1 - 1
    tStop              = nTimeChunks * stepsPerChunk * dtDynamics1 # stepsPerChunk is nb. time-steps per chunk
    tFence1            = round(tFence + 1) ## Fence takes effect on day after completion date.
    tStopMinusDelay    = tStop - tDelayActiveSearch
    ## breaks = nimInteger(length=nTimeChunks)
    ## for (br in 1:nTimeChunks)
    ##   breaks[br] = br * stepsPerChunk
    ## breaks = c(0, breaks)
    breaks = nimInteger(length=nTimeChunksPlus1)
    for (br in 1:nTimeChunksPlus1)
      breaks[br] = (br-1) * stepsPerChunk
    ## Identify location (hexagon) of initial carcass
    distIntro = nimNumeric(length=nPops)
    for (pop in 1:nPops) {
      distIntro[pop] = sqrt((xIntro-xCentroid[pop])^2 + (yIntro-yCentroid[pop])^2)
    }
    hexIntro = which(distIntro[1:nPops]==min(distIntro[1:nPops])) ### nimPrint("hexIntro=",hexIntro)
    ## Initialise arrays for dynamics
    obs                 = nimArray(value=0, dim=c(nReturnStates, nTimeChunksPlus1, nPops))
    states              = nimMatrix(value=0, nrow=nStates, ncol=nPops)
    pStates             = nimMatrix(value=0, nrow=nStates, ncol=nPops)
    tauA_matrix         = nimMatrix(value=0, nrow=nPops, ncol=tStop) ### Index columns with max(t, 1) to acount for t<1
    dStates             = nimNumeric(value=0, length=nStates)
    rates               = nimNumeric(value=0, length=nRates)
    vecLeave            = nimNumeric(value=0, length=nRates)
    tauA_local          = 0
    states[C, hexIntro] = 1
    fenceInducedConnectivityReduction = nimNumeric(value=0, length=lNeighVec)
    ## Estimate expected boar density & sample
    ## browser()
    Eboar = nimNumeric(value=0, length=nPops)
    Eboar[1:nPops] = (omega*weightF[1:nPops] + (1-omega)*weightA[1:nPops]) * areaHuntBag[1:nPops] ## / pHuntY # E[boar] * pHuntY = annual_hunting_bag
    for (pop in 1:nPops) {
      states[S, pop] = rpois(n=1, lambda=Eboar[pop])
    }
    ## Weights for being at home or at a neighbour's
    pAway             = 1-pHome
    pHome_x_pHome     =   pHome*pHome
    pAway_x_pAway     =   pAway*pAway
    pHome_x_pAway_x_2 = 2*pHome*pAway
    pAway_x_pAway_x_2 = 2*pAway*pAway
    nNeigh_x_nNeigh   = nimNumeric(length=nPops, value=nNeigh[1:nPops]*nNeigh[1:nPops])
    ## Dynamics
    for (t in floor(tIntro):tStop) {
      # Indicator to turn off/on observation processes
      indicatorAfterDay0   = (t > 0)
      indicatorDay0OrAfter = (t >= 0)
      indicatorAfterDay60  = (t > tFence)
      indicatorUpdateAS    = indicatorAfterDay60 & (t <= tStopMinusDelay) # Can do an active search before tStop+1
      tPlusDelay           = t + tDelayActiveSearch
      tPlus1               = t + 1
      # Vector to scale (reduce) connectivity at fence
      if (t == tFence1)
        fenceInducedConnectivityReduction[1:lNeighVec] = (1 - seperatedByFence[1:lNeighVec] * omegaFence)
      # Save state of system as in previous time step
      pStates[1:nStates, 1:nPops] = states[1:nStates, 1:nPops] # p = previous
      # nimPrint(sum(pStates[S, 1:nPops]), " ", sum(pStates[Hneg, 1:nPops]))
      #
      for (pop in 1:nPops) {
        ## Share-cell (i.e. "contact") probabilities (determined by epsillon) between i (pop) and j (neighbour)
        pContactIi = pHome_x_pHome + pAway_x_pAway/nNeigh[pop]
        pContactIj = pHome_x_pAway_x_2/nNeigh[pop] + 2*pAway_x_pAway_x_2 / (nNeigh_x_nNeigh[pop]) ## Assumes all pop's neighbours have same number of neighbours as pop. This is fine if hexagons overlap the sea, and thus have zero wild boar.
        pContactCi = pHome
        pContactCj = pAway/nNeigh[pop]
        ## Number of contagious neighbours
        if ( !indicatorAfterDay60 ) { ## (t <= tFence) {
          neighSumI = sum(pStates[I, neighVec[neighStart[pop]:neighEnd[pop]]])
          neighSumC = sum(pStates[C, neighVec[neighStart[pop]:neighEnd[pop]]])
        } else {
          neighSumI = sum(pStates[I, neighVec[neighStart[pop]:neighEnd[pop]]] * fenceInducedConnectivityReduction[neighStart[pop]:neighEnd[pop]])
          neighSumC = sum(pStates[C, neighVec[neighStart[pop]:neighEnd[pop]]] * fenceInducedConnectivityReduction[neighStart[pop]:neighEnd[pop]])
        }
        ## Set "local" versions of rates. These should account for space-time differences in things like hunting pressure, active search prob etc etc
        flagExtraHuntingAndTesting = indicatorAfterDay60 * inHuntZone[pop]
        additionalHuntingPressure  = flagExtraHuntingAndTesting * tauHArmy ## Will be zero unless in extra hunt zone and after day 60
        tauH_local                 = tauH + additionalHuntingPressure
        pTest_local                = pTest * indicatorAfterDay0
        if (flagExtraHuntingAndTesting == 1)
          pTest_local = 1.0
        if (indicatorAfterDay0)
          tauA_local = tauA_matrix[pop, t] * (!flagExtraHuntingAndTesting)  # tauA
        tauP_local = tauP * indicatorAfterDay0  ## THIS NEEDS MODIFICATION - i.e. account for indicatorAfterDay60 * inHuntZone[pop]
        ## Reaction Rates
        rates[S_Infection]        = pStates[S,pop] * ( betaI*(pContactIi*pStates[I,pop] + pContactIj*neighSumI) + betaC*(pContactCi*pStates[C,pop] + pContactCj*neighSumC) )
        rates[E_Incubation]       = pStates[E,pop] * tauI
        rates[I_Recover]          = pStates[I,pop] * tauR
        rates[I_Death]            = pStates[I,pop] * tauC
        rates[C_Rot]              = indicatorDay0OrAfter * pStates[C,pop] * tauRot ## The flag should prevent the initial carcass vanishing before infecting any other animal
        rates[S_HuntUntest]       = pStates[S,pop] * tauH_local * (1-pTest_local)
        rates[E_HuntUntest]       = pStates[E,pop] * tauH_local * (1-pTest_local)
        rates[I_HuntUntest]       = pStates[I,pop] * tauH_local * (1-pTest_local)
        rates[S_HuntTestNeg]      = pStates[S,pop] * tauH_local * pTest_local
        rates[E_HuntTestPos]      = pStates[E,pop] * tauH_local * pTest_local
        rates[I_HuntTestPos]      = pStates[I,pop] * tauH_local * pTest_local
        rates[C_DetActive]        = pStates[C,pop] * tauA_local
        rates[C_DetPassive]       = pStates[C,pop] * tauP_local
        # Reactions - Part 1
        nLeaveS = rpois(n=1, lambda = dtDynamics * (rates[S_Infection]  + rates[S_HuntTestNeg] + rates[S_HuntUntest]))
        nLeaveE = rpois(n=1, lambda = dtDynamics * (rates[E_Incubation] + rates[E_HuntTestPos] + rates[E_HuntUntest]))
        nLeaveI = rpois(n=1, lambda = dtDynamics * (rates[I_Recover]    + rates[I_HuntTestPos] + rates[I_HuntUntest]) + rates[I_Death])
        nLeaveC = rpois(n=1, lambda = dtDynamics * (rates[C_Rot]        + rates[C_DetActive]   + rates[C_DetPassive]))
        nLeaveS = min(nLeaveS, pStates[S,pop])
        nLeaveE = min(nLeaveE, pStates[E,pop])
        nLeaveI = min(nLeaveI, pStates[I,pop])
        nLeaveC = min(nLeaveC, pStates[C,pop])
        ## Reactions - Part 2
        if (nLeaveS > 0) {
          vecLeave[iSmin:iSmax] = rmulti(1, size=nLeaveS, prob=rates[iSmin:iSmax])
        } else {
          vecLeave[iSmin:iSmax] = 0 * vecLeave[iSmin:iSmax]
        }
        if (nLeaveE > 0) {
          vecLeave[iEmin:iEmax] = rmulti(n=1, size=nLeaveE, prob=rates[iEmin:iEmax])
        } else {
          vecLeave[iEmin:iEmax] = (0 * vecLeave[iEmin:iEmax])
        }
        if (nLeaveI > 0) {
          vecLeave[iImin:iImax] = rmulti(n=1, size=nLeaveI, prob=rates[iImin:iImax])
        } else {
          vecLeave[iImin:iImax] = (0 * vecLeave[iImin:iImax])
        }
        if (nLeaveC > 0) {
          vecLeave[iCmin:iCmax] = rmulti(n=1, size=nLeaveC, prob=rates[iCmin:iCmax])
        } else {
          vecLeave[iCmin:iCmax] = (0 * vecLeave[iCmin:iCmax])
        }
        ## Reactions - Part 3
        dStates[S]    = (                       - sum(vecLeave[iSmin:iSmax]) )
        dStates[E]    = (vecLeave[S_Infection]  - sum(vecLeave[iEmin:iEmax]) )
        dStates[I]    = (vecLeave[E_Incubation] - sum(vecLeave[iImin:iImax]) )
        dStates[R]    = (vecLeave[I_Recover] )
        dStates[C]    = (vecLeave[I_Death]      - sum(vecLeave[iCmin:iCmax]))
        dStates[Rot]  = (vecLeave[C_Rot] )
        dStates[Hneg] = (vecLeave[S_HuntTestNeg] )
        dStates[Hpos] = (vecLeave[I_HuntTestPos] + vecLeave[E_HuntTestPos])
        dStates[Hu]   = (vecLeave[S_HuntUntest]  + vecLeave[E_HuntUntest] + vecLeave[I_HuntUntest])
        dStates[Sa]   = (vecLeave[C_DetActive] )
        dStates[Sp]   = (vecLeave[C_DetPassive] )
        ## Update states
        pStates[(nStates-nObsStates)+(1:nObsStates),pop] = zero4Obs[1:nObsStates] ## Set observation states to zero so that they do not accumulate in next step
        states[1:nStates,pop] = pStates[1:nStates,pop] + dStates[1:nStates]
        ## Update tauA_matrix
        if (indicatorUpdateAS & (dStates[Hpos] + dStates[Sa] + dStates[Sp] > 0)) {
          # Then an active search will start in 3 days (delay for test) + 7 days (fixed delay for organising search in simulated data)
          tauA_matrix[pop, tPlus1]     = decayTauA * tauA_matrix[pop, t]
          tauA_matrix[pop, tPlusDelay] = tauA
          for (ii in neighStart[pop]:neighEnd[pop]) {
            iNeigh = neighVec[ii]
            tauA_matrix[iNeigh, tPlusDelay] = min(tauA, (probNeighAS*tauA)+tauA_matrix[iNeigh, tPlusDelay])
          }
        }
      } # End loop on pop
      if (indicatorDay0OrAfter) {
        iChunk = min(which(t<=breaks))
        obs[1:nObsStates, iChunk, 1:nPops] = obs[1:nObsStates, iChunk, 1:nPops] + states[(nStates-nObsStates)+(1:nObsStates), 1:nPops]
        if (returnI) {
          obs[nReturnStates, iChunk, 1:nPops] = obs[nReturnStates, iChunk, 1:nPops] + states[I, 1:nPops]
        }
      }
      dtDynamics = dtDynamics1 # Sets dt for all subsequent time steps
    } # End loop on t
    if (returnI) {
      obs[nReturnStates, 1:nTimeChunksPlus1, 1:nPops] = obs[nReturnStates, 1:nTimeChunksPlus1, 1:nPops] / stepsPerChunk ## Return expected value of I over each chunk
    }
    returnType(double(3))
    return(obs[1:nReturnStates, 1:nTimeChunksPlus1, 1:nPops])
  }
)
