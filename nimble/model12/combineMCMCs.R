## help.start()
options(width=2000)
library(here)
library(coda)
library(nimble)
library(sf)
baseDir <- here("nimble/model12")
setwd(baseDir)
mcmcAllDir <- paste0(baseDir, "/MCMCall1000")

## Read samples
mc1 <- read.table(file="MCMC1/samples.csv", header=TRUE)
mc2 <- read.table(file="MCMC2/samples.csv", header=TRUE)
mc3 <- read.table(file="MCMC3/samples.csv", header=TRUE)
mc4 <- read.table(file="MCMC4/samples.csv", header=TRUE)
mc5 <- read.table(file="MCMC5/samples.csv", header=TRUE)

(lmcmc = c(dim(mc1)[1], dim(mc2)[1], dim(mc3)[1], dim(mc4)[1], dim(mc5)[1]))
(lTail = min(lmcmc))
(lTail = lTail - 9000)
# (lTail = lTail / 2)
# (lTail = lTail * 6/11)

## Trim to standard length
mc1 <- tail(mc1, lTail)
mc2 <- tail(mc2, lTail)
mc3 <- tail(mc3, lTail)
mc4 <- tail(mc4, lTail)
mc5 <- tail(mc5, lTail)

## Write to combined file
mcAll <- rbind(mc1, mc2, mc3, mc4, mc5)
write.table(mcAll, file=paste0(mcmcAllDir, "/samples.csv"))
# plot(as.mcmc(mcAll))

## Plot trajectories
mcl <- as.mcmc.list(list(as.mcmc(mc1),as.mcmc(mc2),as.mcmc(mc3),as.mcmc(mc4),as.mcmc(mc5)))
pdf(file=paste0(mcmcAllDir,"/trajectories-combined.pdf"))
plot(mcl)
dev.off()

## Read likelihoods
s1 <- read.table(file="MCMC1/samples2.csv", header=TRUE)
s2 <- read.table(file="MCMC2/samples2.csv", header=TRUE)
s3 <- read.table(file="MCMC3/samples2.csv", header=TRUE)
s4 <- read.table(file="MCMC4/samples2.csv", header=TRUE)
s5 <- read.table(file="MCMC5/samples2.csv", header=TRUE)

## Trim to standard length
s1 <- tail(s1, lTail)
s2 <- tail(s2, lTail)
s3 <- tail(s3, lTail)
s4 <- tail(s4, lTail)
s5 <- tail(s5, lTail)

## Write to combined file
sAll <- rbind(s1, s2, s3, s4, s5)
write.table(sAll, file=paste0(mcmcAllDir, "/samples2.csv"))
# plot(as.mcmc(sAll))

## Plot likelihood trajectories
sl <- as.mcmc.list(list(as.mcmc(s1),as.mcmc(s2),as.mcmc(s3),as.mcmc(s4),as.mcmc(s5)))
pdf(file=paste0(mcmcAllDir,"/trajectories-loglik-combined.pdf"))
plot(sl)
dev.off()

# Omega fence
of <- nimble::ilogit(unlist(mcl[,"logit_omegaFence"]))
summary(of)
quantile(of, probs=(0:10)/10)
hist(of,breaks=101,freq=FALSE, xlab="Omega_Fence", main="Posterior distribution, D110")
abline(v=quantile(of, probs=c(10,50)/100),col="grey")

# Attract I
of <- nimble::ilogit(unlist(mcl[,"logit_attractI"]))
summary(of)
quantile(of, probs=(0:10)/10)
hist(of,breaks=51,freq=FALSE, xlab="Attract I", main="Posterior distribution, D110")
abline(v=quantile(of, probs=c(10,50)/100),col="grey")

## ess
for ( ii in 1:5 ) {
  print(sort(effectiveSize(mcl[[ii]])))
  print("")
}
print(sort(effectiveSize(mcl)))

## Autocorrelation plots
autocorr.plot(mcl, lag.max=100)


crosscorr(mcl)
crosscorr.plot(mcl)

gelman.plot(mcl)

geweke.plot(mcl)


##########################################
## Back transform each variable         ##
## Creates tmc1, tmc2, tmc3, tmc4, tmc5 ##
##########################################
head(mc1)
ilog = exp # alias for exp
tmcAll = mcAll
for (ch in 1:5) {
  (expression1=(parse(text=paste0("tmc",ch,"=mc",ch))))
  eval(expression1)
  for (ii in 1:ncol(mcAll)) {# ii=1
    cnii = colnames(mcAll)[ii]
    # (parse(text=paste0("tmc",ch)))
    # x = mcAll[,cnii]
    (expression2=parse(text=paste0("x=mc",ch,"[,cnii]")))
    eval(expression2)
    (tr = strsplit(cnii, "_")[[1]][1])
    (vr = strsplit(cnii, "_")[[1]][2])
    (expression3=parse(text=paste0("i",tr,"(x)")))
    y=eval(expression3)
    # colnames(tmcAll)[ii] <- vr
    (expression4=parse(text=paste0("colnames(tmc",ch,")[ii]=vr")))
    eval(expression4)
    # tmcAll[,ii] <- y
    (expression5=parse(text=paste0("tmc",ch,"[,ii]=y")))
    eval(expression5)
    # plot(y, typ="l"); abline(v=1:5 * lTail, col="grey")
  }
  if (FALSE) { # TRUE
    ## Transform intro coordinates
    drake::loadd(bboxAll, hexAll, hexCentroidsAll)
    xlims = bboxAll[c("xmin","xmax")]
    ylims = bboxAll[c("ymin","ymax")]
    xMin=xlims["xmin"]
    xMax=xlims["xmax"]
    yMin=ylims["ymin"]
    yMax=ylims["ymax"]
    tMin=-100
    eval(parse(text=paste0("scTIntro=tmc",ch,"$scTIntro")))
    eval(parse(text=paste0("pIntroX=tmc",ch,"$pIntroX")))
    eval(parse(text=paste0("pIntroY=tmc",ch,"$pIntroY")))
    introT <- tMin - scTIntro * tMin
    introX <- xMin + pIntroX * (xMax - xMin)
    introY <- yMin + pIntroY * (yMax - yMin)
    eval(parse(text=paste0("tmc",ch,"[,\"introX\"]=introX")))
    eval(parse(text=paste0("tmc",ch,"[,\"introY\"]=introY")))
    eval(parse(text=paste0("tmc",ch,"[,\"introT\"]=introT")))
    eval(parse(text=paste0("tmc",ch,"=tmc",ch,"[,!is.element(colnames(tmc",ch,"), c(\"pIntroX\",\"pIntroY\",\"scTIntro\"))]")))
    ## Identify location (hexagon) of initial carcass
    nPops = nrow(hexAll)
    xCentroid = (hexCentroidsAll %>% st_coordinates())[,"X"] # X component of each hexagon's centroid
    yCentroid = (hexCentroidsAll %>% st_coordinates())[,"Y"] # Y component of each hexagon's centroid
    distIntro = nimNumeric(length=nPops)
    for (pop in 1:nPops) {
      distIntro[pop] = sqrt((introX-xCentroid[pop])^2 + (introY-yCentroid[pop])^2)
    }
    hexIntro = which(distIntro[1:nPops]==min(distIntro[1:nPops])) ### nimPrint("hexIntro=",hexIntro)
    eval(parse(text=paste0("tmc",ch,"[,\"hexIntro\"]=hexIntro")))
  }
}

tmcAll <- rbind(tmc1, tmc2, tmc3, tmc4, tmc5)
tmcl <- as.mcmc.list(list(as.mcmc(tmc1),as.mcmc(tmc2),as.mcmc(tmc3),as.mcmc(tmc4),as.mcmc(tmc5)))


pdf(file=paste0(mcmcAllDir,"/trajectories-combined-transformed.pdf"))
plot(tmcl[,1:9])
dev.off()


## ess
for ( ii in 1:5 ) {
  print(sort(effectiveSize(tmcl[[ii]])))
  print("")
}
print(sort(effectiveSize(tmcl)))

XYmax <- 1200
plot(rev(effectiveSize(mcl)), rev(effectiveSize(tmcl)), xlim=c(0,XYmax), ylim=c(0,XYmax))
abline(0,1)



tcc <- crosscorr(tmcl)
tcc[upper.tri(tcc, diag=TRUE)] <- 0
tcc
rankmat <- matrix(rank(-abs(tcc)), 9, 9)
(rankmat==1) * tcc
(rankmat==2) * tcc
(rankmat==3) * tcc
(rankmat==4) * tcc
(rankmat==5) * tcc
