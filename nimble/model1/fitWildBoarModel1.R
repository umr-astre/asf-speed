## This was probably the script used (or very close to it).
## Paths would need to be adapted to reflect changes in directory structure.

# source(here::here("src/wildBoarFitEpidemic.R"))
# rm(list=ls())

setwd(here::here())
source("src/packages.R")
source("src/functions.R")
source("src/nimbleFunctions.R")
source("src/nimbleInitialisationFunctions.R")       # THIS ONLY LOADS FUNCTIONS (WRITEN FOR DRAKE), NOT THE ACTUAL VALUES
# drake::r_make()            # TO UPDATE INIT AND CONST

mcmcDir <- "MCMC1" # "MCMC"  "MCMC2"

##########################################################
## Choose which model to use - all hexagons or a subset ##
##########################################################
loadd(admin,
      bboxAll,
      constWildboarAll,
      constWildboarSub,
      hexAll,
      hexSub,
      hexCentroidsAll,
      hexCentroidsSub,
      initWildboarAll,
      initWildboarSub,
      outbreaks_at_D50,
      stepsPerChunkAll,
      stepsPerChunkSub,
      wildBoarArrayAll,  # observations aggregated over one day intervals, hexagons cover whole island
      wildBoarArraySub,  # observations aggregated over ten day intervals, hexagons restricted to 20km buffer
      wildBoarObsAll,
      wildBoarObsSub)

## UPDATE INITIATION - not needed if drake targets are up to date
tStopAll <- 90
tStopSub <- 50
#debug(setWildBoarModelConstants)
initWildboarSub  <- setWildBoarModelInitialValues (wildBoarArraySub, wildBoarObsSub, stepsPerChunk=stepsPerChunkSub, bboxAll=bboxAll)                 # For fitting     - so must omit tStop argument
initWildboarAll  <- setWildBoarModelInitialValues (wildBoarArrayAll, wildBoarObsAll, stepsPerChunk=stepsPerChunkAll, bboxAll=bboxAll, tStop=tStopAll) # For projections - so tStop argument required
constWildboarSub <- setWildBoarModelConstants (wildBoarArraySub, Hex=hexSub, HexCentroids=hexCentroidsSub, Outbreaks=outbreaks_at_D50, stepsPerChunk=stepsPerChunkSub, bboxAll=bboxAll, tStop=tStopSub)
constWildboarAll <- setWildBoarModelConstants (wildBoarArrayAll, Hex=hexAll, HexCentroids=hexCentroidsAll, Outbreaks=outbreaks_at_D50, stepsPerChunk=stepsPerChunkAll, bboxAll=bboxAll, tStop=tStopAll)

## Build model
rWildBoarModelAll = nimbleModel(bugsBoarCode,
                             constants=constWildboarAll,
                             inits=initWildboarAll,
                             calculate = FALSE, debug = FALSE)
rWildBoarModelSub = nimbleModel(bugsBoarCode,
                              constants=constWildboarSub,
                              inits=initWildboarSub,
                              calculate = FALSE, debug = FALSE)

nimPrint("All: tIntro = ", rWildBoarModelAll$tIntro)
nimPrint("Sub: tIntro = ", rWildBoarModelSub$tIntro)

######################
## Useful node sets ##
######################
detNodes      = rWildBoarModelAll$getNodeNames(determOnly=TRUE)
obsNodes      = sub("\\[.*","",detNodes[grep("obsY", detNodes)])
detNodesNoObs =                detNodes[grep("obsY", detNodes, invert=TRUE)]
stochNodes    = rWildBoarModelAll$getNodeNames(stochOnly=TRUE)

#################################
## Alternative initialisastion ##
#################################
if (TRUE) { # FALSE
  (previousSamples                  <- read.table(file=here(paste0(mcmcDir, "/samples.csv")), header = TRUE))
  (rWildBoarModelAll$tIntro         <- tail(previousSamples[,"tIntro"], 1))
  (rWildBoarModelAll$logit_pIntroX  <- tail(previousSamples[,"logit_pIntroX"], 1))
  (rWildBoarModelAll$logit_pIntroY  <- tail(previousSamples[,"logit_pIntroY"], 1))
  (rWildBoarModelAll$logit_pHome    <- tail(previousSamples[,"logit_pHome"], 1))
  (rWildBoarModelAll$logit_attractI <- tail(previousSamples[,"logit_attractI"], 1))
  (rWildBoarModelAll$logit_pActive  <- tail(previousSamples[,"logit_pActive"], 1))
  (rWildBoarModelAll$log_beta       <- tail(previousSamples[,"log_beta"], 1))
  (rWildBoarModelAll$log_tauCarDet  <- tail(previousSamples[,"log_tauCarDet"], 1))
  (rWildBoarModelAll$logit_pActive       <- tail(previousSamples[,"logit_pActive"], 1))
  (rWildBoarModelAll$logit_pHuntY   <- logit(0.5)) ## tail(previousSamples[,"logit_pHuntY"], 1))
  (rWildBoarModelAll$logit_pR       <- logit(0.05))
  nimCopy(from=rWildBoarModelAll, to=rWildBoarModelSub, nodes=stochNodes)
}
(simulate(rWildBoarModelAll, detNodesNoObs))
(simulate(rWildBoarModelSub, detNodesNoObs))


####################
## Configure MCMC ##
####################
Target   <- c("logit_pHuntY", "tIntro", "logit_pIntroX", "logit_pIntroY", "logit_pHome", "logit_attractI", "log_beta", "log_tauCarDet", "logit_pActive")
Fixed    <- c("logit_pR")
Monitors <- Target

for (tt in Target) {
  val_tt <- eval(parse(text=paste0("rWildBoarModelSub$", tt)))
  nimPrint(tt, " = ", val_tt)
}


####################
## Compile models ##
####################
cWildBoarModelAll = compileNimble(rWildBoarModelAll, showCompilerOutput = TRUE) # FALSE
cWildBoarModelSub = compileNimble(rWildBoarModelSub, showCompilerOutput = TRUE) # FALSE


## Could add nSim to file names...
## Could unflatten tau priors a tad - attempt to reduce drifting in tau...
## Could tighten even more the prior on pHuntY

###################
## MCMC & movies ##
###################
nSim <- c(20, 100, 200, 300, 400, 500)
niter <- 1000
for (iIter in 1:100) {
  if (iIter <= length(nSim)) {
    nimCopy(from=cWildBoarModelSub, to=rWildBoarModelSub, nodes=stochNodes)
    mcPoissonLoglik <- setupMcPoissonLoglik(model=rWildBoarModelSub, obsYNode='obsY', obsY = wildBoarArraySub,
                                            constants = list(nSim=nSim[iIter], Log=TRUE), targetNodes = Target, fixedNodes = Fixed)
    mcmcConf <- configureMCMC(rWildBoarModelSub, monitors=Monitors, monitors2="loglik", nodes = NULL)
    mcmcConf$printSamplers()
    mcmcConf$getMonitors()
    mcmcConf$getMonitors2()
    mcmcConf$removeSamplers()
    mcmcConf$addSampler(target = Target, type = 'sampler_RW_llFunction_block_custom', ## sampler_RW_llFunction_block_custom - this gives "could not find function "extractControlElement"" which is nuts
                        control = list(llFunction = mcPoissonLoglik,
                                       includesTarget = FALSE,    ## FALSE tells sampler to add prior weights for target
                                       ## propCov = CovIni,       ## From a short run. Default is identity
                                       scale = 0.05,               ## Default = 1
                                       adaptFactorExponent = 0.7,  ## Default=0.8
                                       adaptive = TRUE,
                                       adaptInterval=200))        ## Default = 200
    print(mcmcConf)
    Rmcmc <- buildMCMC(mcmcConf)
    ###################
    ## Compile stuff ##
    ###################
    Cmcmc = compileNimble(Rmcmc)
  }
  ##############
  ## Run MCMC ##
  ##############
  STime <- run.time(Cmcmc$run(niter, reset=FALSE))  ## 23h22
  ##########################################################################################################
  ## Import into coda - filtering a NA line that can be generated when recompiling the modelValues object ##
  ##########################################################################################################
  samples <- as.matrix(Cmcmc$mvSamples)
  if(is.na(samples[1,]))
    samples <- samples[-1,]
  samples <- as.mcmc(samples)
  ##
  samples2 <- as.matrix(Cmcmc$mvSamples2)
  if(is.na(samples2[1,]))
    samples2 <- samples2[-1,]
  samples2 <- as.mcmc(samples2)
  ####################
  ## Plot with CODA ##
  ####################
  pdf(file=here(paste0(mcmcDir,"/twoOneForAlllMCMC_",nSim[iIter],".pdf")))
  plot(samples)
  dev.off()
  ##
  pdf(file=here(paste0(mcmcDir,"/twoOneForAllMCMC-loglik_",nSim[iIter],".pdf")))
  plot(samples2)
  dev.off()
  ##
  write.table(samples,  file=here(paste0(mcmcDir,"/samples.csv")), row.names = FALSE)
  write.table(samples2, file=here(paste0(mcmcDir,"/samples2.csv")),row.names = FALSE)
  ##############
  ## Simulate ##
  ##############
  nimCopy(from=cWildBoarModelSub, to=cWildBoarModelAll, nodes=stochNodes)
  testPast <- FALSE
  while(!testPast) {
    simulate(cWildBoarModelAll, detNodes)
    nimPrint(sum(cWildBoarModelAll$obsY[1,1:51,]), " vs. ", sum(wildBoarArrayAll[1,,]))
    nimPrint(sum(cWildBoarModelAll$obsY[2,1:51,]), " vs. ", sum(wildBoarArrayAll[2,,]))
    nimPrint(sum(cWildBoarModelAll$obsY[3,1:51,]), " vs. ", sum(wildBoarArrayAll[3,,]))
    nimPrint(sum(cWildBoarModelAll$obsY[4,1:51,]), " vs. ", sum(wildBoarArrayAll[4,,]))
    testPast <- (0 < sum(cWildBoarModelAll$obsY[2:4,,]))
  }
  if (iIter%%3==0) {
    ## Movie of 1 simulation
    makeCummulIncidenceMovie(cWildBoarModelAll$obsY, hexAll, admin, stepsPerChunkAll,
                             frameRate=3, repeatEndNtimes = 10, breaksMax=35, addDay0=FALSE,
                             movieName=here(paste0(mcmcDir,"/simFromLastSampleAll_",nSim[iIter],".mp4")))
    ## Fuse with movie of obsY
    if (file.exists(here(paste0(mcmcDir,"/fusedObsSim_",nSim[iIter],".mp4"))))
      system(paste("mv",  here(paste0(mcmcDir,"/fusedObsSim_",nSim[iIter],".mp4")),  here(paste0(mcmcDir,"/fusedObsSim_",nSim[iIter],"_OLD.mp4"))))
    system(paste("ffmpeg -i", here("animations/obsAll.mp4") ,"-i", here(paste0(mcmcDir,"/simFromLastSampleAll_",nSim[iIter],".mp4")), "-filter_complex hstack ", paste0(mcmcDir,"/fusedObsSim_",nSim[iIter],".mp4")))
  }
}





#####################################
## Synthetic Likelihood - Workflow ##
#####################################
# ConstantsSL <- list(nSim = 10, Log = TRUE) ## thresh <- constantsSL[['thresh']] ## lag    <- constantsSL[['lag']]
# debug(setupSynthLoglik)
# debug(simulateObservations)
# synthLoglik <- setupSynthLoglik(model=rWildBoarModelAll, simObsNodes='obsY', obsY = wildBoarArrayAll, constantsSL = ConstantsSL, targetNodes = Target, fixedNodes = Fixed)
# synthLoglik$run()
