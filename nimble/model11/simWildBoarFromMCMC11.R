# source(here::here("nimble/model11/simWildBoarFromMCMC11.R"))
# rm(list=ls())

SCENARIO <- 1

# This script jumps through MCMC output and generates weighted means & stdevs of the simulations

##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
## Set directories and MCMC output file ####
##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
library(here)
## help.start()
(modelDir = here::here("nimble/model11")) # For selecting model version
(mcmcDir  = paste0(modelDir,"/MCMCall"))    # For selecting samples.csv. Can be a symbolic link to a different directory if simualtion code is more recent that estimation code (e.g. fence added since MCMC).
# (simDir   = paste0(mcmcDir,"/sims"))      # For selecting model version
(simDir   = paste0(mcmcDir,"/simsCases"))      # For selecting model version
setwd(modelDir)
# setwd(mcmcDir)
mcmcFile   = paste0(mcmcDir,"/samples.csv")  ## Assumed to be parameters
mcmcFile2  = paste0(mcmcDir,"/samples2.csv") ## Assumed to be posterior log-likelihoods
## modelInitialisationFile = paste0("../", dir("../")[grep("initialisation_model", dir("../"))])

if (!file.exists(simDir))
  system(paste("mkdir", simDir))

radiusActiveSearch = 1 # 1

##%%%%%%%%%%%%%%%%%%%%%%
## Build the models ####
##%%%%%%%%%%%%%%%%%%%%%%
reBuildModels = TRUE # FALSE
if (reBuildModels) {
  setwd(modelDir)
  source(here::here("src/packages.R"))
  source(here::here("src/functions.R"))
  source(here::here("nimble/nimbleFunctions.R"))
  source(paste0(modelDir, "/model_definition.R"))     # THIS ONLY LOADS FUNCTIONS (WRITEN FOR DRAKE), NOT THE ACTUAL VALUES
  ##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  ## Choose which model to use - all hexagons or a subset ####
  ##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  loadd(admin,
        bboxAll,
        fence,
        huntZone,
        hexAll,
        hexCentroidsAll,
        weightTauAHome1km,
        weightTauAAway1km,
        weightTauAHome2km,
        weightTauAAway2km,
        # probNeighAS1km,
        # probNeighAS2km,
        outbreaks_at_D110,
        pig_sites,
        stepsPerChunkAll,
        wildBoarArrayAll,  # observations aggregated over one day intervals, hexagons cover whole island
        wildBoarObsAll)
  ##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  ## Initial values and constants ####
  ##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  tStopAll          = 230 ## For 30 day prediction beyond day 80
  initWildboarAll   = setWildBoarModelInitialValues(wbArray4sim   = wildBoarArrayAll,
                                                    wildBoarObs   = wildBoarObsAll,
                                                    stepsPerChunk = stepsPerChunkAll,
                                                    bboxAll       = bboxAll,
                                                    tStop         = tStopAll,
                                                    returnI       = TRUE)
  constWildboarAll  = setWildBoarModelConstants(wbArray4sim        = wildBoarArrayAll,
                                                Hex                = hexAll,
                                                HexCentroids       = hexCentroidsAll,
                                                Outbreaks          = outbreaks_at_D110,
                                                stepsPerChunk      = stepsPerChunkAll,
                                                bboxAll            = bboxAll,
                                                tStop              = tStopAll,
                                                weightTauAHome1km = weightTauAHome1km,
                                                weightTauAAway1km = weightTauAAway1km,
                                                weightTauAHome2km = weightTauAHome2km,
                                                weightTauAAway2km = weightTauAAway2km,
                                                ## probNeighAS1km  = probNeighAS1km,
                                                ## probNeighAS2km  = probNeighAS2km,
                                                returnI            = TRUE)
  ##%%%%%%%%%%%%%%%%%%%
  ## Build R model ####
  ##%%%%%%%%%%%%%%%%%%%
  rWildBoarModelAll = nimbleModel(bugsBoarCode,
                                  constants=constWildboarAll,
                                  inits=initWildboarAll,
                                  calculate = FALSE, debug = FALSE)
  rModel4MV = nimbleModel(bugsCode4MV) ## So we can make a modelValues without the huge obsY array
  ##%%%%%%%%%%%%%%%%%%%%%%
  ## Useful node sets ####
  ##%%%%%%%%%%%%%%%%%%%%%%
  detNodes      = rWildBoarModelAll$getNodeNames(determOnly=TRUE)
  obsNodes      = sub("\\[.*","",detNodes[grep("obsY", detNodes)])
  detNodesNoObs =                detNodes[grep("obsY", detNodes, invert=TRUE)]
  stochNodes    = rWildBoarModelAll$getNodeNames(stochOnly=TRUE)
  ##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  ## Ensure determined Nodes are initialised ####
  ##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  simulate(rWildBoarModelAll, detNodesNoObs)
  for (dn in 1:length(detNodesNoObs))
    nimPrint(detNodesNoObs[dn], " ", rWildBoarModelAll[[detNodesNoObs[dn]]])
  for (sn in 1:length(stochNodes))
    nimPrint(stochNodes[sn], " ", rWildBoarModelAll[[stochNodes[sn]]])
  ##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  ## Compile model & initialise deterministic nodes ####
  ##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  cWildBoarModelAll = compileNimble(rWildBoarModelAll, showCompilerOutput = TRUE) # FALSE
  cWildBoarModelAll$radiusActiveSearch <- radiusActiveSearch
  simulate(cWildBoarModelAll, detNodesNoObs)
  ##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  ## Set Dimension Constants ####
  ##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  nReturnTypes = dim(cWildBoarModelAll$obsY)[1]
  nObsDays     = dim(wildBoarArrayAll)[2]
  nPops        = dim(wildBoarArrayAll)[3]
  nSimDays     = dim(cWildBoarModelAll$obsY)[2]
  nObsStates   = nReturnTypes - constWildboarAll$returnI
  iInfectious  = nReturnTypes  # Index for infectious wild boar in the output array
}


##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
## Load MCMC mcmcSamples and (optionally) remove burnin ####
##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
mcmcSamples  = read.table(file=mcmcFile,  header = TRUE)
mcmcSamples2 = read.table(file=mcmcFile2, header = TRUE) # Assumed to be posterior likelihood
if (FALSE) { # TRUE
  (burnin      <- firstSampleAboveMean(as.numeric(mcmcSamples2[,1])))
  mcmcSamples  <- mcmcSamples[-(1:burnin),]
  mcmcSamples2 <- mcmcSamples2[-(1:burnin),]
}
nimPrint("Effective sample size: ", effectiveSize(mcmcSamples))
if (FALSE) { # TRUE
  trajectoriesPDF = TRUE # FALSE
  # mcmcSamples     = mcmcSamples[!mcmcSamples$log_beta==0,] ## Filters out non-samples where I stopped the MCMC
  if (trajectoriesPDF)
    pdf(file=paste0(mcmcDir, "/trajectories.pdf"))
  plot(as.mcmc(mcmcSamples))
  if (trajectoriesPDF)
    dev.off()
  dim(mcmcSamples)
}

##%%%%%%%%%%%%%%%%%%%%%
## Setup simulator ####
##%%%%%%%%%%%%%%%%%%%%%
rSimFromMCMC <- setupSimFromMCMC(model       = rWildBoarModelAll,
                                 model4MV    = rModel4MV,
                                 obsYNode    = "obsY",
                                 obsY        = wildBoarArrayAll,
                                 mcmcSamples = mcmcSamples)
cSimFromMCMC <- compileNimble(rSimFromMCMC)

## To recover original data in obsY
## nimCopy(from=rWildBoarModelAll, to=cWildBoarModelAll, nodes="obsY")

##%%%%%%%%%%%%%%%%%%%%%
## Run simulations ####
##%%%%%%%%%%%%%%%%%%%%%
nSim                = 10000 # 5000
tFence              = 60
flagFence           = c(1, 0) ## if 0 -> set omegaFence to 0
flagHuntZone        = c(1, 0) ## if 0 -> set log_tauHArmy to -Inf (if it can handle it)
radiusAS            = c(1,2)
(scenarios          = expand.grid(flagFence=flagFence, flagHuntZone=flagHuntZone, radiusAS=radiusAS))
nScenarios          = nrow(scenarios)
nOut                = 6
scenarioList        = vector("list", nScenarios)
### for (scenario in 1:nScenarios) { # scenario=1
for (scenario in SCENARIO) { # scenario=1
  nimPrint("################ Scenario=", scenario, " ################")
  nimPrint(mcmcDir)
  #####################
  ## Simulate epidemics
  #####################
  nimPrint("Simulating epidemics")
  mySims <- cSimFromMCMC$run(nSim, flagFence=scenarios[scenario,"flagFence"], flagHuntZone=scenarios[scenario,"flagHuntZone"], radiusAS=scenarios[scenario,"radiusAS"])
  ##### Plot Maps #####
  scenarioList[[scenario]] <- mySims
  fileNames = c("Mean", "stDev", "MeanW", "stDevW", "Best", "CasesMean")
  nimPrint("Plotting epidemics")
  for(day in rev(c(90, 110, 130, 180, 230))) { # day=80
    nimPrint(day)
    for (iOut in 1:(nOut-1)) {
      fileName = fileNames[iOut]
      mapInfectiousDayX(mySims[iOut,,], Hex=hexAll,
                        Admin = admin %>%filter(is.element(rowAdmin, c(6,9,11,22))),
                        Day=day, outputDir=simDir,
                        legendOutside = TRUE,
                        fileName=paste0(fileName, "_sims",nSim, "_scenario", scenario,"_d",day),
                        StepsPerChunk=1, Fence=fence, HuntZone=huntZone,
                        pig_sites=pig_sites,
                        Outbreaks=outbreaks_at_D110, caption=fileNames[iOut])
    }
    for (iOut in nOut) { # iOut=6
      (fileName = fileNames[iOut])
      mapWildBoarCasesDayX(mySims[iOut,,], Hex=hexAll,
                           Admin = admin %>%filter(is.element(rowAdmin, c(6,9,11,22))),
                           Day=day, outputDir=simDir,
                           legendOutside = TRUE,
                           fileName=paste0(fileName, "_sims",nSim, "_scenario", scenario,"_d",day),
                           StepsPerChunk=1, Fence=fence, HuntZone=huntZone,
                           pig_sites=pig_sites,
                           Outbreaks=outbreaks_at_D110)
    }
  }
  ##### Write output to files #####
  (scenariosString = paste0(c("Fence", "Army","Radius"), scenarios[scenario,], collapse="_"))
  for (iOut in 1:nOut) {
    write.table(mySims[iOut,,],
                col.names=FALSE,
                row.names = paste0("D",(1:nrow(mySims[iOut,,]))-1),
                file = paste0(simDir,"/",fileNames[iOut], "_", scenariosString, "_nSim", nSim, ".csv"))
  }
}



##
## (simFile = paste0(simDir, "/sims_nSim",nSim,"_scenario",SCENARIO,".Rdata"))
## save(SCENARIO,
##      scenarios,
##      scenarioList,
##      admin,
##      bboxAll,
##      fence,
##      hexAll,
##      hexCentroidsAll,
##      outbreaks_at_D110,
##      pig_sites,
##      stepsPerChunkAll,
##      wildBoarArrayAll,
##      wildBoarObsAll,
##      mapInfectiousDayX,
##      radiusActiveSearch,
##      file=simFile)


## Write scenariosList as a series of files
## load(simFile)


## 02h58
## Sys.time()
