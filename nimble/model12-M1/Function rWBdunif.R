source(here::here("nimble/model12-M1/simulation_model12-M1.R"))

#faire varier min et max
#moyenne mauvaise donn�e explicative

##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
## DISCRETE UNIFORM DISTRIBUTION
##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
cWildBoarModelM1$sampleEboar <- 0
nb_simulation                <- 10
Emergence_date               <- matrix(NA,nrow=nrow(hexM1),ncol=1) #nb_simulation)
WB_data                      <- matrix(NA,nrow=nrow(hexM1),ncol=nb_simulation)
InfectedArea_U               <- matrix(NA,nrow=1, ncol=nb_simulation)
Simulate_rWBdunif            <- vector("list",nb_simulation)
for (j in 1:nb_simulation) {
  #browser()
  nimPrint(j, " of ", nb_simulation)
  hexM1sim                          <- rWBdunif(min=10,max=200,hex=hexM1,output="some",threshold=0.05,niter=3,hexCentroidsM1)
  WB_data[,j]                       <- hexM1sim$E_wildboar
  cWildBoarModelM1$Eboar            <- hexM1sim$E_wildboar
  simulate(cWildBoarModelM1, detNodes)
  simE                              <- t(cWildBoarModelM1$epidemic[iE, 1:nSteps1, 1:nPops])
  Emergence_date[,1]                <- apply(simE,1, function(x) min(which(x>0)))-1
  InfectedPixels_U                  <- apply(simE>0, 1, "any")
  InfectedArea_U[j]                 <- sum(st_area(hexM1sim[InfectedPixels_U,]))
  Emergence_date[sapply(Emergence_date, is.infinite)] <- NA
  Simulate_rWBdunif[[j]]  <- hexM1sim %>% cbind(Emergence_date)
  # if (nrow(Simulate_rWBdunif[[j]])>1184){
  #   browser()
  #}
}
neighVec = attributes(hexM1)$neighVec

##%%%%%%%%%%%%%%%%%%%
## MAPS WITH rWBdunif
##%%%%%%%%%%%%%%%%%%%

# Density
breaks=sort(unique(c(0,1,pretty(c(1,max(WB_data))))))
map1_rWBdunif<-list()
breaks=sort(unique(c(0,1,pretty(c(1,max(WB_data))))))
for(j in 1:nb_simulation){
  map1_rWBdunif[[j]] <- tm_shape(Simulate_rWBdunif[[j]]) + tm_polygons("E_wildboar", breaks=breaks, legend.show = FALSE)
}
map1_rWBdunif[[1+j]] <- tm_shape(Simulate_rWBdunif[[j]]) + tm_polygons("E_wildboar",title="Nombre de sangliers", breaks=breaks) +
  tm_shape(Simulate_rWBdunif[[j]]) + tm_polygons(col="white", border.col="white") + tm_layout(legend.position = c ("LEFT", "TOP"), legend.width=0.63)
tmap_arrange(map1_rWBdunif)
#dev.print(device = svg, file = "RWBunif_density.svg")

# R0
R0_data<-matrix(NA,nrow=nrow(hexM1),ncol=1) #nb_simulation
output='all'
for (j in 1:nb_simulation){
  densityCol<-"E_wildboar"
  R0_data[,1]<-get_R0(Simulate_rWBdunif[[j]], mu = 1/(5*365), model=cWildBoarModelM1, neighVec=neighVec, densityCol=densityCol, output=output)
  Simulate_rWBdunif[[j]]  <- Simulate_rWBdunif[[j]] %>% cbind (R0_data)
  }
mapR0_rWBdunif<-list()
breaks= sort(unique(c(0,1,pretty(c(1,max(WB_data))))))/10
for(j in 1:nb_simulation){
  mapR0_rWBdunif[[j]]<-tm_shape(Simulate_rWBdunif[[j]]) + tm_polygons("R0_data",breaks=breaks, legend.show = FALSE)
}
mapR0_rWBdunif[[1+j]]<-tm_shape(Simulate_rWBdunif[[j]]) + tm_polygons("R0_data",breaks=breaks, title = paste("R0_data",output)) +
  tm_shape(Simulate_rWBdunif[[j]]) + tm_polygons(col="white", border.col="white") + tm_layout(legend.position = c ("LEFT", "TOP"),legend.width=0.80)
tmap_arrange(mapR0_rWBdunif)

# Emergence map
map2_rWBdunif<-list()
breaks=sort(unique(c(0,1,pretty(c(1,tStop)))))
for(j in 1:nb_simulation){
  eval(parse(text=paste0("Simulate_rWBdunif[[j]]$Emergence_date","=as.integer(Simulate_rWBdunif[[j]]$Emergence_date", ")")))
  map2_rWBdunif[[j]] <- tm_shape(Simulate_rWBdunif[[j]]) + tm_polygons("Emergence_date",breaks=breaks, legend.show = FALSE)
}
map2_rWBdunif[[1+j]]<-tm_shape(Simulate_rWBdunif[[j]]) + tm_polygons("Emergence_date", breaks=breaks, title = "Jour d'emergence") +
  tm_shape(Simulate_rWBdunif[[j]]) + tm_polygons(col="white", border.col="white") + tm_layout(legend.position = c ("LEFT", "TOP"),legend.width=0.63)
tmap_arrange(map2_rWBdunif)
#dev.print(device = svg, file = "RWBunif_emergence.svg")

##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
## PROPAGATION TIME BETWEEN LOCAL AND NEIGHBOURS
##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
E_time_all<-matrix(NA,nrow=nrow(Simulate_rWBdunif[[j]]),ncol=nb_simulation)
for (j in 1:nb_simulation) {
  emergenceDelay<-function(ii,emTimeVec,neighVec,Hex){
    neighStart = Hex$neighStart           # Indicates start of sub-vector of neighVec for each hexagonal patch
    neighEnd   = Hex$neighEnd             # Indicates end   of sub-vector of neighVec for each hexagonal patch
    #neighVec   = attributes(Hex)$neighVec
    tlocal <- emTimeVec[ii]
    tneigh <- emTimeVec[neighVec[neighStart[ii]:neighEnd[ii]]]
    tdiff  <- tlocal - tneigh
    imax   <- which(tdiff==max(tdiff))
    moyenne<-mean(tdiff[imax],na.rm = TRUE)
    moyenne[moyenne<0]=NA
    #print(neighVec)
    return(moyenne)
  }
  E_time<-matrix(NA,nrow=nrow(Simulate_rWBdunif[[j]]),ncol=1)
  for (i in 1:ncol(E_time) ) {
    E_time[,i]<-sapply(1:nPops,emergenceDelay, emTimeVec = (Simulate_rWBdunif[[j]][,grep("Emergence_date", colnames(Simulate_rWBdunif[[j]]))[i]]%>%st_drop_geometry())[,1],neighVec=neighVec ,Hex=hexM1)
  }
  E_time_all[,j]         <- E_time
  Simulate_rWBdunif[[j]] <- Simulate_rWBdunif[[j]] %>% cbind(E_time)
}

map_delay<-list()
for(j in 1:nb_simulation){
  map_delay[[j]]<-tm_shape(Simulate_rWBdunif[[j]]) + tm_polygons("E_time", legend.show = FALSE)
  print(j)
}
map_delay[[1+j]]<-tm_shape(Simulate_rWBdunif[[j]]) + tm_polygons("E_time", title = "Time for ASF spread") +
  tm_shape(Simulate_rWBdunif[[j]]) + tm_polygons(col="white", border.col="white") + tm_layout(legend.position = c ("LEFT", "TOP"),legend.width=0.63)

tmap_arrange(map_delay)
  
plot(WB_data,E_time_all, xlab="Wild boar density", ylab="Time for the neigh. to be E.", main="Spread of ASF according to WB density")
plot(WB_data,1/E_time_all, xlab="Wild boar density", ylab="1/Time for the neigh. to be E.", main="Spread of ASF according to WB density")

##%%%%%%%
##INDICES
##%%%%%%%
##
### AVERAGE DENSITY
##
Mean_WB <- matrix(NA,nrow=1,ncol=nb_simulation)
for (j in 1:nb_simulation) {
  Mean_WB[,j] <- mean(WB_data[,j]) 
}
##
### DENSITY in the STARTING AREA 
##
##
### EMPTY PIXEL and %
##
Empty_pixel <- matrix(NA,nrow=1,ncol=nb_simulation)
for (j in 1:nb_simulation) {
  Empty_pixel[,j] <- sum(WB_data[,j]==0)
}
Percentage <- matrix(NA,nrow=1,ncol=nb_simulation)
for (j in 1:nb_simulation) {
  Percentage[,j]<- (Empty_pixel[,j] / 1184)*100
}
##
### INFECTED AREA
##
units(InfectedArea_U)="m^2"
InfectedArea_km2_U=set_units(InfectedArea_U,"km^2")
InfectedArea_km2_U
plot(Mean_WB,InfectedArea_km2_U, xlab="Densite moyenne", ylab="Surface infecte en km2")
plot(Empty_pixel,InfectedArea_km2_U, xlab="Nombre pixel vides", ylab="Surface infecte en km2")
##
### INFECTED PIXEl
##
areaOnePixel     = (st_area(hexM1sim[1,]) %>% set_units("km^2"))
InfectedPixels_U = InfectedArea_km2_U / areaOnePixel
##
### CONNECTIVITE : nb moyen de pixel voisins occupes
##
Sim_filtred0<- vector("list",nb_simulation)
for (j in 1:nb_simulation){
Sim_filtred0[[j]]<-Simulate_rWBdunif[[j]] %>% filter(Emergence_date>0) 
#filter(Emergence_date>0) ou filter E_wildboar >0 pas de grandes diff�rences sur moyenne voisins
# faire � la fois emergence et pixel habit�
}
Neigh_WB<-matrix(NA,nrow=1,ncol=nb_simulation)
for (j in 1:nb_simulation){
  Neigh_WB[,j]<-mean(Sim_filtred0[[j]][["nNeigh"]]) 
}
Neigh_WB
##%%%%%%%%%%%%%%%%%%%%%
## REGRESSION LINEAIRES
##%%%%%%%%%%%%%%%%%%%%%

simData = data.frame(Mean=as.vector(Mean_WB), InfectedArea=as.vector(InfectedArea_km2_U), Empty_pixel=as.vector(Empty_pixel),InfectedPixels=as.vector(InfectedPixels_U))

lm1 <- lm(InfectedArea ~ Mean, data = simData)
lm1
summary(lm1)
(aic_lm1 = AIC(lm1))
plot(lm1) 
with(simData, plot(InfectedArea ~ Mean, ylab="Surface infectee en km2", xlab="Densite moyenne"))
abline(lm1)
library(car)
durbinWatsonTest(lm1) #pas d'autocorelation
acf(residuals(lm1), main="prest.lm1")
shapiro.test(residuals(lm1)) #suivent une loi normale #pas avec grand �chantillon

lm2 <- lm(InfectedArea ~ Empty_pixel, data = simData)
lm2
summary(lm2)
(aic_lm2 = AIC(lm2))
plot(lm2) 
with(simData, plot(InfectedArea ~ Empty_pixel, xlab="Surface infectee en km2", ylab="Nombre de pixels vides"))
abline(lm2)
durbinWatsonTest(lm2) 
acf(residuals(lm2), main="prest.lm1")
shapiro.test(residuals(lm2)) #ne suivent pas une loi normale

##%%%%%%%%%%%%%%%%%%%%%%%%
## GLM BINOMIAL REGRESSION
##%%%%%%%%%%%%%%%%%%%%%%%%
totalPixels = nrow(hexM1)
simData$HealthyPixels = totalPixels - simData$InfectedPixels
#
glm1 = glm(cbind(InfectedPixels, HealthyPixels)  ~ Mean, data=simData, family="binomial")
plot(glm1)
with(simData, plot(InfectedPixels/(InfectedPixels+HealthyPixels) ~ Mean, ylab="Proportion de surface infect�e", xlab="Densit� moyenne"))
lines(0:200, predict.glm(glm1, type = "response", newdata=data.frame(Mean=0:200)), col="red")
#
simData_no0<-simData%>%filter(InfectedArea>0)
glm2= glm(cbind(InfectedPixels, HealthyPixels)  ~ Mean, data=simData_no0, family="binomial")
plot(glm2)
with(simData_no0, plot(InfectedPixels/(InfectedPixels+HealthyPixels) ~ Mean, ylab="Proportion de surface infect�e", xlab="Densit� moyenne"))
lines(0:200, predict.glm(glm2, type = "response", newdata=data.frame(Mean=0:200)), col="red")


