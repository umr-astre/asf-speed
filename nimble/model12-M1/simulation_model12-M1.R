library(pscl) # install.packages("pscl")
library(mgcv) # install.packages("mgcv")

# source(here::here("nimble/model12-M1/simWildBoarFromMCMC12.R"))
# rm(list=ls())

(modelDir = here::here("nimble/model12-M1")) # For selecting model version
(mcmcDir  = paste0(modelDir,"/MCMCall1000"))  # For selecting samples.csv. Can be a symbolic link to a different directory if simualtion code is more recent that estimation code (e.g.  added since MCMC).
# (simDir   = paste0(mcmcDir,"/sims"))        # For selecting model version
(simDir   = paste0(mcmcDir,"/simsCases"))     # For selecting model version
setwd(modelDir)
# setwd(mcmcDir)
mcmcFile   = paste0(mcmcDir,"/samples.csv")  ## Assumed to be parameters
mcmcFile2  = paste0(mcmcDir,"/samples2.csv") ## Assumed to be posterior log-likelihoods
## modelInitialisationFile = paste0("../", dir("../")[grep("initialisation_model", dir("../"))])

if (!file.exists(simDir))
  system(paste("mkdir", simDir))

##%%%%%%%%%%%%%%%%%%%%%%
## Build the models ####
##%%%%%%%%%%%%%%%%%%%%%%
## reBuildModels = TRUE # FALSE
## if (reBuildModels) {
setwd(modelDir)
source(here::here("src/packages.R"))
source(here::here("src/functions.R"))
source(here::here("nimble/nimbleFunctions.R"))
source(paste0(modelDir, "/definition_model12-M1.R"))     # THIS ONLY LOADS FUNCTIONS (WRITEN FOR DRAKE), NOT THE ACTUAL VALUES
##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
## Choose which model to use - all hexagons or a subset ####
##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
loadd(admin,
      bboxM1,
      hexM1,
      hexCentroidsM1,
      pHomeSim4km)

##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
## Initial values and constants ####
##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
tStop          = 365 ## For 30 day prediction beyond day 80
initWildboarM1 = setWildBoarModelInitialValues(Hex     = hexM1,
                                               bboxHex = bboxM1,
                                               tStop   = tStop,
                                               pHome   = pHomeSim4km
                                               )
constWildboarM1 = setWildBoarModelConstants(Hex          = hexM1,
                                            HexCentroids = hexCentroidsM1,
                                            bboxHex      = bboxM1,
                                            tStop        = tStop)

##%%%%%%%%%%%%%%%%%%%
## Build R model ####
##%%%%%%%%%%%%%%%%%%%
rWildBoarModelM1 = nimbleModel(bugsBoarCode,
                               constants=constWildboarM1,
                               inits=initWildboarM1,
                               calculate = FALSE, debug = FALSE)

#### rModel4MV = nimbleModel(bugsCode4MV) ## So we can make a modelValues without the huge epidemic array

##%%%%%%%%%%%%%%%%%%%%%%
## Useful node sets ####
##%%%%%%%%%%%%%%%%%%%%%%
(detNodes      = rWildBoarModelM1$getNodeNames(determOnly=TRUE))
(epiNodes      = sub("\\[.*","",detNodes[grep("epidemic", detNodes)]))
(detNodesNoEpi =                detNodes[grep("epidemic", detNodes, invert=TRUE)])
(stochNodes    = rWildBoarModelM1$getNodeNames(stochOnly=TRUE))

##%%%%%%%%%%%%%%%%%%%%%%%##
## Load MCMC mcmcSamples ##
##%%%%%%%%%%%%%%%%%%%%%%%##
mcmcSamples  = read.table(file=mcmcFile,  header = TRUE)
mcmcSamples2 = read.table(file=mcmcFile2, header = TRUE) # Assumed to be posterior likelihood


################################################################################################
## Identify maximum a posteriori (MAP) estiamte & use it to initialise the model's parameters ##
################################################################################################
iMAP                      <- which(mcmcSamples2$var1==max(mcmcSamples2$var1)) ## returns index of most likely MCMC sample
parasMAP                  <- mcmcSamples[iMAP,]
rWildBoarModelM1$beta     <- exp(parasMAP$log_beta)
rWildBoarModelM1$attractI <- ilogit(parasMAP$logit_attractI)
rWildBoarModelM1$pHuntY   <- 0 #ilogit(parasMAP$logit_pHuntY)
rWildBoarModelM1$pIntroX  <- 1/2
rWildBoarModelM1$pIntroY  <- 1/2


##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
## Update all determined nodes ####
##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
simulate(rWildBoarModelM1, detNodesNoEpi)
for (dn in 1:length(detNodesNoEpi))
  nimPrint(detNodesNoEpi[dn], " ", rWildBoarModelM1[[detNodesNoEpi[dn]]])
for (sn in 1:length(stochNodes))
  nimPrint(stochNodes[sn], " ", rWildBoarModelM1[[stochNodes[sn]]])

## Test R simulation
#simulate(rWildBoarModelM1, detNodes)
dim(rWildBoarModelM1$epidemic)

##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
## Compile model & initialise deterministic nodes ####
##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
cWildBoarModelM1 = compileNimble(rWildBoarModelM1, showCompilerOutput = TRUE) # FALSE


##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
## Indices for outputs of simulation model
##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
iS = 1
iE = 2
iI = 3
iR = 4
iC = 5

##%%%%%%%%%%%%%%%%%%%%
## Dimension Constants
##%%%%%%%%%%%%%%%%%%%%
nStates = dim(cWildBoarModelM1$epidemic)[1]
nSteps  = dim(cWildBoarModelM1$epidemic)[2] - 1
nPops   = dim(cWildBoarModelM1$epidemic)[3]
nSteps1 = nSteps + 1


##%%%%%%%%%%
## FIND BETA
##%%%%%%%%%%

# the right beta for a R0max=20 and density max /pixel = 200 WB

hexM1$Density_max<-200
neighVec=attributes(hexM1)$neighVec

# find R0 = 1
betafix<-0.001
cWildBoarModelM1$beta<-betafix
simulate(cWildBoarModelM1,detNodesNoEpi)
R0_fix<-getmode(get_R0(hexM1, mu = 1/(5*365),neighVec=neighVec, model=cWildBoarModelM1, densityCol="Density_max", output="all"))
beta_R0_one<-betafix / R0_fix
cWildBoarModelM1$beta<-beta_R0_one
simulate(cWildBoarModelM1,detNodesNoEpi)
R0_test<-getmode(get_R0(hexM1, mu = 1/(5*365),neighVec=neighVec, model=cWildBoarModelM1, densityCol="Density_max", output="all"))

# create a list of beta
betaseq<-exp(seq(-9,-5.5,l=20))#beta_R0_one*1/2*(2^(0:5))
R0_matrix<-matrix(NA,ncol=length(betaseq),nrow=nrow(hexM1))
colnames(R0_matrix)<-paste0("Beta",betaseq)
for(i in 1:length(betaseq)){
  cWildBoarModelM1$beta=betaseq[i]
  simulate(cWildBoarModelM1,detNodesNoEpi)
  R0_matrix[,i] <- get_R0(hexM1, mu = 1/(5*365),neighVec=neighVec, model=cWildBoarModelM1, densityCol="Density_max", output="all")
}
R0_mode<-apply(R0_matrix, 2, "getmode")
plot(betaseq,R0_mode)

cWildBoarModelM1$beta<-0.0033992158



# #####################
# ## Simulate epidemics
# #####################
# nimPrint("Simulating epidemics")
# #### mySims <- cSimFromMCMC$run(nSim) ##
# #simulate(cWildBoarModelM1, detNodes)
#
#
# simS <- t(cWildBoarModelM1$epidemic[iS, 1:nSteps1, 1:nPops])
# simE <- t(cWildBoarModelM1$epidemic[iE, 1:nSteps1, 1:nPops])
# simI <- t(cWildBoarModelM1$epidemic[iI, 1:nSteps1, 1:nPops])
# simR <- t(cWildBoarModelM1$epidemic[iR, 1:nSteps1, 1:nPops])
# simC <- t(cWildBoarModelM1$epidemic[iC, 1:nSteps1, 1:nPops])
#
# colnames(simS) <- paste0("S",0:nSteps)
# colnames(simE) <- paste0("E",0:nSteps)
# colnames(simI) <- paste0("I",0:nSteps)
# colnames(simR) <- paste0("R",0:nSteps)
# colnames(simC) <- paste0("C",0:nSteps)
#
# hexSim <- hexM1 %>% select("E_wildboar") %>% cbind(simS) %>% cbind(simE) %>% cbind(simI) %>% cbind(simR) %>% cbind(simC)
# names(hexSim)
# for (ii in 1:365)
#   print(tm_shape(hexSim) + tm_polygons(paste0("C",ii)))
#
#
#
# ####################################################
# ## Simulate wild boar densities with new function ##
# ####################################################
#
#
# ##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%###
# ## SETUP SIMULATOR                                                     ###
# ##                                                                     ###
# ## NOTE: This step uses a function that                                ###
# ## 1) generates 1 simulation per MCMC line, and                        ###
# ## 2) returns summary statistics of all the simualtions                ###
# ##                                                                     ###
# ## But for Mahe, this is not really what we want.                      ###
# ## Perhaps better to break into smaller steps                          ###
# ## 1) a function to run simulations & create output files (saves RAM)  ###
# ## 2) various functions to analyse the output files                    ###
# ##                                                                     ###
# ##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%###
# ## rSimFromMCMC <- setupSimFromMCMC(model       = rWildBoarModelM1,
# ##                                  model4MV    = rModel4MV,
# ##                                  obsYNode    = "epidemic",
# ##                                  epidemic        = wildBoarArrayM1,
# ##                                  mcmcSamples = mcmcSamples)
# ## cSimFromMCMC <- compileNimble(rSimFromMCMC)
#
#
# ## To recover original data in epidemic
# ## nimCopy(from=rWildBoarModelM1, to=cWildBoarModelM1, nodes="epidemic")
#
# ##%%%%%%%%%%%%%%%%%%%%%
# ## Run simulations ####
# ##%%%%%%%%%%%%%%%%%%%%%
# nSim                = 1
# nOut                = 6
#
# nimPrint(mcmcDir)
# #####################
# ## Simulate epidemics
# #####################
# nimPrint("Simulating epidemics")
# #### mySims <- cSimFromMCMC$run(nSim) ##
# simulate(cWildBoarModelM1, detNodesNoEpi)
#
# dim(cWildBoarModelM1$epidemic)
#
#
#
#
# #################
# ## Plot Maps ####
# #################
# fileNames = c("Mean", "stDev", "MeanW", "stDevW", "Best", "CasesMean")
# nimPrint("Plotting epidemics")
# for(day in rev(c(90, 110, 130, 180, 230))) { # day=80
#   nimPrint(day)
#   for (iOut in 1:(nOut-1)) {
#     fileName = fileNames[iOut]
#     mapInfectiousDayX(mySims[iOut,,], Hex=hexM1,
#                       Admin = admin %>% filter(is.element(rowAdmin, c(6,9,11,22))),
#                       Day=day, outputDir=simDir,
#                       legendOutside = TRUE,
#                       fileName=paste0(fileName, "_sims",nSim,"_d",day),
#                       StepsPerChunk=1,
#                       caption=fileNames[iOut])
#   }
#   for (iOut in nOut) { # iOut=6
#     (fileName = fileNames[iOut])
#     mapWildBoarCasesDayX(mySims[iOut,,], Hex=hexM1,
#                          Admin = admin %>% filter(is.element(rowAdmin, c(6,9,11,22))),
#                          Day=day, outputDir=simDir,
#                          legendOutside = TRUE,
#                          fileName=paste0(fileName, "_sims",nSim,"_d",day),
#                          StepsPerChunk=1)
#   }
# }
#
# ##### Write output to files #####
# for (iOut in 1:nOut) {
#   write.table(mySims[iOut,,],
#               col.names=FALSE,
#               row.names = paste0("D",(1:nrow(mySims[iOut,,]))-1),
#               file = paste0(simDir,"/",fileNames[iOut], "_nSim", nSim, ".csv"))
# }
