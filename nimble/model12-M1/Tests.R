##%%%%%
## TEST
##%%%%%
cWildBoarModelM1$sampleEboar<-0
Mean<-seq(10,200,length=11)
WB_data<-matrix(NA,nrow=nrow(hexM1),ncol=length(Mean))
colnames(WB_data)<-paste0("Density",Mean)

Emergence_date<-matrix(NA,nrow=nrow(hexM1),ncol=length(Mean))
iCol=colnames(Emergence_date)[ncol(Emergence_date)]
E_time<-matrix(NA,nrow=nrow(hexSim),ncol=length(Mean))
one_E_time<-matrix(NA,nrow=nrow(E_time),ncol=ncol(E_time))
colnames(E_time)<-paste0("E_time",Mean)
colnames(one_E_time)<-paste0("1/E_time",Mean)

nrepeat <- 10

Mean_test<-matrix(NA, ncol=ncol(E_time),nrow=nrepeat)
Var_test<-matrix(NA, ncol=ncol(E_time),nrow=nrepeat)

for(j in 1:nrepeat){
  
  for (i in 1:length(Mean)){
    hexM1sim<-rWB(Mean[i], SD=2,hex=hexM1,niter=3, hexCentroidsM1)
    WB_data[,i]=hexM1sim$E_wildboar
    cWildBoarModelM1$Eboar<-hexM1sim$E_wildboar
    simulate(cWildBoarModelM1, detNodes)
    simE <- t(cWildBoarModelM1$epidemic[iE, 1:nSteps1, 1:nPops]) #sim E = expos�s
    Emergence_date[,i]=apply(simE,1, function(x) min(which(x>0)))-1
  }
  Emergence_date[sapply(Emergence_date, is.infinite)]<- NA
  colnames(Emergence_date)=paste0("Emergence_mean", Mean)
  hexSim <- hexM1sim %>% select(!"E_wildboar") %>% cbind(Emergence_date) %>% cbind(WB_data)
  
  # emergenceDelay<-function(ii,emTimeVec,neighVec,Hex){
  #   neighStart = Hex$neighStart           # Indicates start of sub-vector of neighVec for each hexagonal patch
  #   neighEnd   = Hex$neighEnd             # Indicates end   of sub-vector of neighVec for each hexagonal patch
  #   #neighVec   = attributes(Hex)$neighVec
  #   tlocal <- emTimeVec[ii]
  #   tneigh <- emTimeVec[neighVec[neighStart[ii]:neighEnd[ii]]]
  #   tdiff  <- tlocal - tneigh
  #   imax   <- which(tdiff==max(tdiff))
  #   moyenne<-mean(tdiff[imax])
  #   moyenne[moyenne<0]=NA
  #   #print(neighVec)
  #   return(moyenne)
  # }
  
  for (i in 1:ncol(E_time) ) {
    E_time[,i]<-sapply(1:nPops,emergenceDelay, emTimeVec = (hexSim[,grep("Emergence_mean", colnames(hexSim))[i]]%>%st_drop_geometry())[,1],neighVec=neighVec ,Hex=hexSim)
    E_time[sapply(E_time, is.na)]<- 0
    one_E_time[,i]<-1/E_time[,i]
    one_E_time[sapply(1/E_time, is.infinite)]<- 0
    Mean_test[j,i] <- mean(one_E_time[,i])
    Var_test[j,i]<- var(one_E_time[,i])
  }
}
plot(Mean_test, Var_test) #lineaire


##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
## REGRESSION LINEAIRE & NON-LINEAR
##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
simData_1 = data.frame(Mean=as.vector(Mean_new), InfectedArea=as.vector(InfectedArea_km2_1), InfectedPixels=as.vector(InfectedPixels_1))

simData_2 = data.frame(Mean=as.vector(Mean_new), InfectedArea=as.vector(InfectedArea_km2_2), InfectedPixels=as.vector(InfectedPixels_2))

lm1 <- lm(InfectedArea ~ Mean, data = simData)
lm1
summary(lm1)
(aic_lm1 = AIC(lm1))

lm2 <- lm(InfectedArea ~ Mean + I(Mean^2), data = simData)
lm2
summary(lm2)
(aic_lm2 = AIC(lm2))

plot(lm1) ## Includes various diagnostics, including QQplots

with(simData, plot(InfectedArea ~ Mean, ylab="Surface infecté en km2", xlab="Densité moyenne"))
abline(lm1)

library(car)
durbinWatsonTest(lm1)
# les residus sont independants pas d'autocorrelation proche de 2 #homo
# auto correlation + (proche 0) #hetero

acf(residuals(lm1), main="prest.lm1")
shapiro.test(residuals(lm1))
# les residus ne suivent pas une loi normale homo et hetero

bp.test(lm1$residuals)

glm1 <- glm(InfectedPixels ~ Mean, data = simData, family="poisson")
glm1
summary(glm1)
plot(glm1)
(aic_glm1 = AIC(glm1)) # CAREFUL, not same data... can't compare!!!!

glm2 <- glm(I(1+InfectedPixels) ~ Mean, data = simData, family="Gamma")
glm2
summary(glm2)
plot(glm2)
(aic_glm2 = AIC(glm2)) # CAREFUL, not same data... can't compare!!!!

gam1 <- gam(InfectedPixels ~ s(Mean), data = simData, family="poisson") # s() is a smoothing spline
AIC(gam1)

gam2 <- gam(InfectedPixels ~ Mean, data = simData, family="ziP") # zero-inflated Poisson
AIC(gam2)

gam3 <- gam(InfectedPixels ~ s(Mean), data = simData, family="ziP") # zero-inflated Poisson
AIC(gam3)

with(simData, plot(InfectedArea ~ Mean, ylab="Surface infectée en km2", xlab="Densité moyenne"))
abline(lm1)
lines(0:200, areaOnePixel * predict(glm1, type = "response", newdata=data.frame(Mean=0:200)), col="red")
lines(0:200, areaOnePixel * predict(glm2, type = "response", newdata=data.frame(Mean=0:200)), col="orange")
lines(0:200, areaOnePixel * predict(gam1, type = "response", newdata=data.frame(Mean=0:200)), col="green")
lines(0:200, areaOnePixel * predict(gam2, type = "response", newdata=data.frame(Mean=0:200)), col="purple")
lines(0:200, areaOnePixel * predict(gam3, type = "response", newdata=data.frame(Mean=0:200)), col="brown")
lines(0:200, predict(lm2, type = "response", newdata=data.frame(Mean=0:200)), col="blue")

#####################################################
## Another approach with glm - binomial regression ##
#####################################################
simData_1$HealthyPixels = totalPixels - simData_1$InfectedPixels
glm3 = glm(cbind(InfectedPixels, HealthyPixels)  ~ Mean, data=simData_1, family="binomial")
plot(glm3)

with(simData_1, plot(InfectedPixels/(InfectedPixels+HealthyPixels) ~ Mean, ylab="Proportion de surface infect�e", xlab="Densit� moyenne"))
lines(0:200, predict.glm(glm3, type = "response", newdata=data.frame(Mean=0:200)), col="red")
## This is much better!!!

simData_2$HealthyPixels = totalPixels - simData_2$InfectedPixels
glm4 = glm(cbind(InfectedPixels, HealthyPixels)  ~ Mean, data=simData_2, family="binomial")
plot(glm4)
with(simData_2, plot(InfectedPixels/(InfectedPixels+HealthyPixels) ~ Mean, ylab="Proportion de surface infect�e", xlab="Densit� moyenne"))
lines(0:200, predict.glm(glm4, type = "response", newdata=data.frame(Mean=0:200)), col="red")

glm5=glm(cbind(InfectedPixels, HealthyPixels)  ~ 1, data=simData_1, family="binomial")
glm6=glm(cbind(InfectedPixels, HealthyPixels)  ~ 1, data=simData_2, family="binomial")

AIC(glm3)
AIC(glm5)

AIC(glm4)
AIC(glm6)
# la moyenne influence le mod�le

##%%%%%%%%%
##Filtred 0
##%%%%%%%%%

simData_1no0<-simData_1%>%filter(InfectedArea>0)
glm7= glm(cbind(InfectedPixels, HealthyPixels)  ~ Mean, data=simData_1no0, family="binomial")
plot(glm7)

with(simData_1no0, plot(InfectedPixels/(InfectedPixels+HealthyPixels) ~ Mean, ylab="Proportion de surface infect�e", xlab="Densit� moyenne"))
lines(0:200, predict.glm(glm7, type = "response", newdata=data.frame(Mean=0:200)), col="red")


simData_2no0<-simData_2%>%filter(InfectedArea>0)
glm8= glm(cbind(InfectedPixels, HealthyPixels)  ~ Mean, data=simData_2no0, family="binomial")
plot(glm8)

with(simData_2no0, plot(InfectedPixels/(InfectedPixels+HealthyPixels) ~ Mean, ylab="Proportion de surface infect�e", xlab="Densit� moyenne"))
lines(0:200, predict.glm(glm8, type = "response", newdata=data.frame(Mean=0:200)), col="red")


####
#GLM
####

agram<-glm(yobs2 ~ I(1/xseq),family=Gamma(link="inverse"),
           + control = glm.control(maxit=100), start = c(2,4))
