source(here::here("nimble/model12-M1/simulation_model12-M1.R"))


##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
## VARIATION MEAN WITH THE FUNCTION rWB
##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
nb_simulation                <- 1
cWildBoarModelM1$sampleEboar <- 0
Mean                         <- seq(10,200,length=11)
WB_data                      <- matrix(NA,nrow=nrow(hexM1),ncol=length(Mean))
colnames(WB_data)            <- paste0("Density",Mean)
Emergence_date               <- matrix(NA,nrow=nrow(hexM1),ncol=length(Mean))
InfectedArea_rWB             <- matrix(NA,nrow=length(Mean), ncol=nb_simulation)
Mean_new                     <- matrix(NA,nrow=length(Mean), ncol=nb_simulation)
#
# rWB (Mean[i], SD=2,hex=hexM1,niter=5) function
Simulate_rWB <- vector("list",nb_simulation)
for (j in 1:nb_simulation) {
  nimPrint(j, " of ", nb_simulation)
  for (i in 1:length(Mean)){
    hexM1sim               <- rWB(Mean[i], SD=2,hex=hexM1,niter=3, hexCentroidsM1)
    WB_data[,i]            = hexM1sim$E_wildboar
    cWildBoarModelM1$Eboar <- hexM1sim$E_wildboar
    simulate(cWildBoarModelM1,detNodes)
    simE                   <- t(cWildBoarModelM1$epidemic[iE, 1:nSteps1, 1:nPops]) #sim E = expos�s
    Emergence_date[,i]     = apply(simE,1, function(x) min(which(x>0)))-1
    ## InfectedArea[i,j]   <- sum(st_area(hexM1sim[simE[,ncol(simE)]>0,])) ## This line is bugged. See above.
    InfectedPixels_rWB     <- apply(simE>0, 1, "any")
    InfectedArea_rWB[i,j]  <- sum(st_area(hexM1sim[InfectedPixels_rWB,]))
    Mean_new[i,j]          <- Mean[i]
  }
  Emergence_date[sapply(Emergence_date, is.infinite)] <- NA
  colnames(Emergence_date) = paste0("Emergence_mean", Mean)
  Simulate_rWB[[j]] <- hexM1sim %>% select(!"E_wildboar") %>% cbind(Emergence_date) %>% cbind(WB_data)
}
#
units(InfectedArea_rWB)="m^2"
InfectedArea_km2_2=set_units(InfectedArea_rWB,"km^2")
areaOnePixel = (st_area(hexM1sim[1,]) %>% set_units("km^2"))
InfectedPixels_rWB = InfectedArea_km2_2 / areaOnePixel # Counts of Pixels
#
plot(Mean_new,InfectedArea_km2_2, ylab="Surface infecte en km2", xlab="Densite moyenne")
neighVec = attributes(hexM1)$neighVec
#dev.print(device = svg, file = "20simRWB.svg")
#rm(hexM1sim)

##%%%%%%%%%%%%%%
## MAPS WITH rWB
##%%%%%%%%%%%%%%

# Density
generateDensityMap(Simulate_rWB[[1]])
#dev.print(device = svg, file = "RWB.svg")

# R0
generateR0Map(Simulate_rWB[[1]], output ='carcass')
#dev.print(device = svg, file = "RWB_R0.svg")

# Emergence map
generateEmergenceMap(Simulate_rWB[[1]])
#dev.print(device = svg, file = "RWB_emergence.svg")

##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
## LINKS BETWEEN DENSITY AND EMERGENCE DAYS
##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

for (j in 1:nb_simulation) {
  emergenceDelay<-function(ii,emTimeVec,neighVec,Hex){
    neighStart = Hex$neighStart           # Indicates start of sub-vector of neighVec for each hexagonal patch
    neighEnd   = Hex$neighEnd             # Indicates end   of sub-vector of neighVec for each hexagonal patch
    #neighVec   = attributes(Hex)$neighVec
    tlocal <- emTimeVec[ii]
    tneigh <- emTimeVec[neighVec[neighStart[ii]:neighEnd[ii]]]
    tdiff  <- tlocal - tneigh
    imax   <- which(tdiff==max(tdiff))
    moyenne<-mean(tdiff[imax])
    moyenne[moyenne<0]=NA
    #print(neighVec)
    return(moyenne)
  }
  #iCol=colnames(Emergence_date)[ncol(Emergence_date)]
  E_time<-matrix(NA,nrow=nrow(Simulate_rWB[[j]]),ncol=length(Mean))
  colnames(E_time)<-paste0("E_time",Mean)
  for (i in 1:ncol(E_time) ) {
    E_time[,i]<-sapply(1:nPops,emergenceDelay, emTimeVec = (Simulate_rWB[[j]][,grep("Emergence_mean", colnames(Simulate_rWB[[j]]))[i]]%>%st_drop_geometry())[,1],neighVec=neighVec ,Hex=hexM1)
  }
  Simulate_rWB[[j]] <-  Simulate_rWB[[j]] %>% cbind(E_time)
}

##%%%%%%%%%%%%%%%%%%%%
## TIME FOR ASF SPREAD
##%%%%%%%%%%%%%%%%%%%%

generateDelayPlot(Simulate_rWB[[1]])

map_delay<-list()
for(m in Mean){
  map_delay[[which(m==Mean)]]<-tm_shape(Simulate_rWB[[1]]) + tm_polygons(paste0("E_time",m), legend.show = FALSE) 
  #+tm_layout(title=paste0("Mean=",m))
}
map_delay[[1+which(m==Mean)]]<-tm_shape(Simulate_rWB[[1]]) + tm_polygons(paste0("E_time",m), title = "Time for ASF spread") +
  tm_shape(Simulate_rWB[[1]]) + tm_polygons(col="white", border.col="white") + tm_layout(legend.position = c ("LEFT", "TOP"))

tmap_arrange(map_delay)
#
plot(WB_data,E_time, xlab="Wild boar density", ylab="Time for the neigh. to be E.", main="Spread of ASF according to WB density")
# plot(WB_data,1/E_time, xlab="Wild boar density", ylab="1/Time for the neigh. to be E.", main="Spread of ASF according to WB density")
#
# hist(1/E_time[,"E_time162"], n=50)

##%%%%%%%%%%%%%%%%%%
## VARIATION SD ONLY
##%%%%%%%%%%%%%%%%%%

nb_simulation                <- 1
cWildBoarModelM1$sampleEboar <- 0
SD                           <- seq(0,10,by=2)
SD_data                      <- matrix(NA,nrow=nrow(hexM1),ncol=length(SD))
colnames(SD_data)            <- paste0("Density",SD)
Emergence_date               <- matrix(NA,nrow=nrow(hexM1),ncol=length(SD))
InfectedArea_SD              <- matrix(NA,nrow=length(SD), ncol=nb_simulation)
SD_new                       <- matrix(NA,nrow=length(SD), ncol=nb_simulation)
#
Simulate_rWB_SD <- vector("list",nb_simulation)
for (j in 1:nb_simulation) {
  nimPrint(j, " of ", nb_simulation)
  for (i in 1:length(SD)){
    hexM1sim                  <- rWB(Mean=50, SD[i],hex=hexM1,niter=3, hexCentroidsM1)
    SD_data[,i]               <- hexM1sim$E_wildboar
    cWildBoarModelM1$Eboar    <- hexM1sim$E_wildboar
    simulate(cWildBoarModelM1, detNodes)
    simE                      <- t(cWildBoarModelM1$epidemic[iE, 1:nSteps1, 1:nPops])
    Emergence_date[,i]        = apply(simE,1, function(x) min(which(x>0)))-1
    InfectedPixels_SD         <- apply(simE>0, 1, "any")
    InfectedArea_SD[i,j]      <- sum(st_area(hexM1sim[InfectedPixels_SD,]))
    SD_new[i,j]               <- SD[i]
  }
  Emergence_date[sapply(Emergence_date, is.infinite)]<- NA
  colnames(Emergence_date) <- paste0("Emergence_SD", SD)
  Simulate_rWB_SD[[j]] <- hexM1sim %>% select(!"E_wildboar") %>% cbind(Emergence_date) %>% cbind(SD_data)
  print(j)
}
units(InfectedArea_SD)="m^2"
InfectedArea_km2_SD=set_units(InfectedArea_SD,"km^2")
# areaOnePixel = (st_area(hexM1sim[1,]) %>% set_units("km^2"))
# InfectedPixels_SD = InfectedArea_km2_SD / areaOnePixel # Counts of Pixels
# 
plot(SD_new,InfectedArea_km2_SD, ylab="Surface infecte en km2", xlab="Ecart-type")
neighVec = attributes(hexM1)$neighVec


##%%%%%%%%%%%%
# DENSITY MAPS
##%%%%%%%%%%%%
breaks=sort(unique(c(0,1,pretty(c(1,max(SD_data))))))
map1_SD<-list()
for(sd in SD){
  map1_SD[[which(sd==SD)]] <- tm_shape(hexSim) + tm_polygons(paste0("Density",sd), breaks=breaks, legend.show = FALSE)
}
map1_SD[[1+which(sd==SD)]] <- tm_shape(hexSim) + tm_polygons(paste0("Density",sd),title="Density", breaks=breaks) +
  tm_shape(hexSim) + tm_polygons(col="white", border.col="white") + tm_layout(legend.position = c ("LEFT", "TOP"))

tmap_arrange(map1_SD)

##%%%%%%%%
## R0 MAPS
##%%%%%%%%
breaks= sort(unique(c(0,1,pretty(c(1,max(SD_data))))))/10
for (i in 1:length(SD)){
  densityCol<-paste0("Density",SD[i])
  hexSim[paste0("R0",SD[i])]<-get_R0(hexSim, mu = 1/(5*365), model=cWildBoarModelM1, neighVec=neighVec, densityCol=densityCol, output="all")
}

mapR0_SD<-list()
for(sd in SD){
  mapR0_SD[[which(sd==SD)]]<-tm_shape(hexSim) + tm_polygons(paste0("R0",sd),breaks=breaks, legend.show = FALSE)
}
mapR0_SD[[1+which(sd==SD)]]<-tm_shape(hexSim) + tm_polygons(paste0("R0",sd), title = "R0",breaks=breaks) +
  tm_shape(hexSim) + tm_polygons(col="white", border.col="white") + tm_layout(legend.position = c ("LEFT", "TOP"))

tmap_arrange(mapR0_SD)

##%%%%%%%%%%%%%%
# EMERGENCE MAPS
##%%%%%%%%%%%%%%
map2_SD<-list()
for(sd in SD){
  map2_SD[[which(sd==SD)]]<-tm_shape(hexSim) + tm_polygons(paste0("Emergence_SD",sd), legend.show = FALSE)
}
map2_SD[[1+which(sd==SD)]]<-tm_shape(hexSim) + tm_polygons(paste0("Emergence_SD",sd), title = "Emergence_sd") +
  tm_shape(hexSim) + tm_polygons(col="white", border.col="white") + tm_layout(legend.position = c ("LEFT", "TOP"))

tmap_arrange(map2_SD)

