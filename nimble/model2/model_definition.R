bugsBoarCode = nimbleCode({
  ## Known Parameters
    omega  <- 0.8                              # Habitat preference
    tauI   <- 1/7                               # Incubation rate
    tauC   <- 1/7                               # Disease induced mortality rate
    tauR   <- tauC * pR/(1 - pR)                # Recovery rate so that pR (recovfery probability ) is 0.05
    tauRot <- 1/90                            # Carcass decomposition rate
    tauH   <- -log((1 - pHuntY)^(12/(8 * 365))) # Hunting rate. Hunters think pHuntY=0.5 (prob. shot in one year). Hunting season is 8 month long.
    pTest  <- 20/100                           # Probability to test a hunted wild boar
    ## Unknown Parameters
    logit(scTIntro) ~ dLogitBeta(7, 7)       # Introduction date (on 0:1)
    tIntro <- tMin - scTIntro * tMin         # Introduction date (on tMin:0)
    logit(pIntroX) ~ dLogitBeta(1, 1)        # Introduction location relative to bounding box of whole island
    logit(pIntroY) ~ dLogitBeta(1, 1)        # Introduction location relative to bounding box of whole island
    logit(pHome) ~ dLogitBeta(1, 1)          # Connectivity: pHome=p(wild boar in home cell). (1-pHome)=p(wild boar in neighbouring cell)
    logit(attractI) ~ dLogitBeta(1, 1)       # Relative contact rate, i.e. how attractive is an infectious individual relative to a carcass.
    logit(pActive) ~ dLogitBeta(1, 1)        # Probability a carcass is found via active (and not passive) search
    log(beta) ~ dLogExp(rate = 1/1e+07)      # The scale of transmission - multiply with the realtive attractivity of Infectious and Carcasses.
    log(tauCarDet) ~ dLogExp(rate = 1/1e+07) # Total carcass detection rate
    tauA <- tauCarDet * pActive
    tauP <- tauCarDet * (1 - pActive)
    betaI <- beta * attractI
    betaC <- beta * (1 - attractI)
    xIntro <- xMin + pIntroX * (xMax - xMin)
    yIntro <- yMin + pIntroY * (yMax - yMin)
    ## Somewhat known, can be fixed for simplicity
    logit(pR)     ~ dLogitBeta(5/2.18, 95/2.18)
    logit(pHuntY) ~ dLogitBeta(330, 330)
    ## Observed &/or Simulated Data
    obsY[1:(nObsStates + returnI), 1:nTimeChunksPlus1, 1:nPops] <- simulateObservations(tIntro = tIntro,
        xIntro = xIntro, yIntro = yIntro, nObsStates = nObsStates,
        returnI = returnI, nPops = nPops, nTimeChunksPlus1 = nTimeChunksPlus1,
        stepsPerChunk = stepsPerChunk, xCentroid = xCentroid[1:nPops],
        yCentroid = yCentroid[1:nPops], weightF = weightF[1:nPops],
        weightA = weightA[1:nPops], areaHuntBag = areaHuntBag[1:nPops],
        omega = omega, betaI = betaI, betaC = betaC, pHome = pHome,
        pR = pR, tauA = tauA, tauP = tauP, tauI = tauI, tauC = tauC,
        tauR = tauR, tauRot = tauRot, tauH = tauH, pHuntY = pHuntY,
        pTest = pTest, nNeigh = nNeigh[1:nPops], neighVec = neighVec[1:lNeighVec],
        neighStart = neighStart[1:nPops], neighEnd = neighEnd[1:nPops])
    # For monitor2
    loglik <- 0
})


setWildBoarModelConstants <- function (wbArray4sim, Hex, HexCentroids, Outbreaks, stepsPerChunk, bboxAll, tStop, returnI=FALSE) {
  ## wbArray4sim - an element of {wildBoarArrayAll, wildBoarArraySub}
  xlims = bboxAll[c("xmin","xmax")]
  ylims = bboxAll[c("ymin","ymax")]
  nObsStates    = dim(wbArray4sim)[1]
  if (missing(tStop)) {
    nTimeChunksPlus1 = dim(wbArray4sim)[2]
  } else {
    nTimeChunks = ceiling(tStop / stepsPerChunk)
    nTimeChunksPlus1 = nTimeChunks + 1
  }
  nPops         = dim(wbArray4sim)[3]
  if (nPops != nrow(Hex)) stop("The following must be true: dim(wbArray4sim)[3] == nrow(Hex)")
  #
  const = list(
    nPops            = nPops,                    # Number of hexagonal patches
    nObsStates       = nObsStates,               # Number of observation types for wild boar, "NT" "PT" "AS" "PS" (we drop NAS)
    returnI          = returnI,                  # Indicator: FALSE - simulations don't return I, TRUE - simulations do return I
    nTimeChunksPlus1 = nTimeChunksPlus1,         # Number of bins for aggregating time-step data
    stepsPerChunk    = stepsPerChunk,            # Number of timesteps for each aggregated bin
    weightF          = Hex$weightF,              # Weight for estimating wild boar density (from admin area's hunting bag) based on forest cover in hexagon / total forest cover in admin area.
    weightA          = Hex$weightA,              # Weight for estimating wild boar density (from admin area's hunting bag) based on agro   cover in hexagon / total agro   cover in admin area.
    areaHuntBag      = Hex$adminAreaHuntBag,     # Hunting bag data - to be divided by all hexagons from a given admin area
    nNeigh           = Hex$nNeigh,               # Number of neighbours for each hexagon
    neighStart       = Hex$neighStart,           # Indicates start of sub-vector of neighVec for each hexagonal patch
    neighEnd         = Hex$neighEnd,             # Indicates end   of sub-vector of neighVec for each hexagonal patch
    neighVec         = attributes(Hex)$neighVec, # A vector containing indices for neighbours
    lNeighVec        = length(attributes(Hex)$neighVec),
    xCentroid        = (HexCentroids %>% st_coordinates())[,"X"], # X component of each hexagon's centroid
    yCentroid        = (HexCentroids %>% st_coordinates())[,"Y"], # Y component of each hexagon's centroid
    tMin             = -100,                                      # Lower bound on carcass-fall-from-space day
    xMin             = xlims[1],
    xMax             = xlims[2],
    yMin             = ylims[1],
    yMax             = ylims[2]
    ## x0            = as.double((Outbreaks %>% filter(DATE.SUSP==0) %>% st_coordinates())[1,"X"]), # X coordinate of first observed infected farm
    ## y0            = as.double((Outbreaks %>% filter(DATE.SUSP==0) %>% st_coordinates())[1,"Y"])  # Y coordinate of first observed infected farm
  )
  const
}

setWildBoarModelInitialValues <- function (wbArray4sim, wildBoarObs, stepsPerChunk, tStop, bboxAll, returnI=FALSE) {
  ## browser()
  ## wbArray4sim - an element of {wildBoarArrayAll, wildBoarArraySub}
  nObsStates       = dim(wbArray4sim)[1]
  nTimeChunksPlus1 = dim(wbArray4sim)[2]
  nPops            = dim(wbArray4sim)[3] # = nrow(Hex) # Same value
  nTimeChunks      = nTimeChunksPlus1 - 1
  nReturnStates    = nObsStates + returnI
  #
  if (missing(tStop)) {
    nimPrint("USING ORIGINAL OBSERVATIONS IN setWildBoarModelInitialValues")
    nimPrint("dim(obsY)[2] = ", nTimeChunksPlus1)
    tStop <- nTimeChunks * stepsPerChunk
    myObsY = nimArray(value=0, dim=c(nReturnStates, nTimeChunksPlus1, nPops))
    myObsY[1:nObsStates, 1:nTimeChunksPlus1, 1:nPops] = wbArray4sim[1:nObsStates, 1:nTimeChunksPlus1, 1:nPops] # These nodes are first initialised with observed data, but the data will be replaced each time this node is simulated.
  } else if (nTimeChunks <= ceiling(tStop / stepsPerChunk)) {
    nTimeChunks <- ceiling(tStop / stepsPerChunk)
    nTimeChunksPlus1 = nTimeChunks + 1
    nimPrint("NOT USING ORIGINAL OBSERVATIONS IN setWildBoarModelInitialValues. IF THIS IS NOT WHAT YOU WANT THEN DELETE tStop ARGUMENT.")
    nimPrint("setting dim(obsY)[2] to ", nTimeChunksPlus1)
    myObsY = nimArray(value=0, dim=c(nReturnStates, nTimeChunksPlus1, nPops))
  } else {
    stop("Please ensure ceiling(tStop / stepsPerChunk) >= dim(wbArray4sim)[2] in order to simulate at least as far as original data.")
  }
  # Ball-park Initial value for location of first infectious carcass
  xIntro = (wildBoarObs %>% filter(is.element(DET, c("AS","PS","PT"))) %>% st_coordinates() %>% colMeans())["X"]
  yIntro = (wildBoarObs %>% filter(is.element(DET, c("AS","PS","PT"))) %>% st_coordinates() %>% colMeans())["Y"]
  xMin = bboxAll["xmin"]
  xMax = bboxAll["xmax"]
  yMin = bboxAll["ymin"]
  yMax = bboxAll["ymax"]
  #
  inits = list(
    obsY           = myObsY[1:nReturnStates, 1:nTimeChunksPlus1, 1:nPops],
    logit_pHuntY   = logit(0.5),   # Probability a wild boar is shot within one year's 8 month hunting season
    logit_pR       = logit(0.05),  # Recovery probability
    logit_pHome    = logit(1/7),   # Connectivity
    logit_attractI = logit(0.5),   # Attraction of Infectious indivuidual (relative to a Carcass)
    logit_pActive  = logit(0.95),  # Attraction of Infectious indivuidual (relative to a Carcass)
    log_beta       = log(0.012),   # Transmission rate for I
    log_tauCarDet  = log(0.005),   # Active carcass detection rate
    logit_scTIntro = logit(0.75),  # Time first infectious carcass fell from space
    logit_pIntroX  = logit((xIntro-xMin)/(xMax-xMin)),
    logit_pIntroY  = logit((yIntro-yMin)/(yMax-yMin)),
  )
  inits
}
