<!DOCTYPE html>
<html lang="" xml:lang="">
  <head>
    <title>ASF Challenge</title>
    <meta charset="utf-8" />
    <meta name="author" content="Astre Team, Montpellier, France." />
    <meta name="date" content="2021-03-11" />
    <script src="libs/header-attrs/header-attrs.js"></script>
    <link href="libs/remark-css/default.css" rel="stylesheet" />
    <link href="libs/remark-css/default-fonts.css" rel="stylesheet" />
    <link href="libs/anchor-sections/anchor-sections.css" rel="stylesheet" />
    <script src="libs/anchor-sections/anchor-sections.js"></script>
    <link rel="stylesheet" href="libs/font-awesome/css/fontawesome-all.min.css" type="text/css" />
    <link rel="stylesheet" href="cirad.css" type="text/css" />
  </head>
  <body>
    <textarea id="source">












class: title-slide, inverse
background-image: url(img/presentation-title-background.png)
background-size: cover


![:scale 20%](img/asf-challenge_logo.png)

# ASF Challenge

## Modelling approach

### Astre Team, Montpellier, France.




---
# Team members

![](img/member_ferran.jpg)&lt;br/&gt;
Ferran Jori
.light[Veterinary epidemiologist&lt;br/&gt;
Team representative]

.pull-left[
![](img/member_david.jpg)&lt;br/&gt;
David Pleydell&lt;br/&gt;
.light[Mathematical ecologist]
]

.pull-right[
![:scale 20%](img/member_facu.jpg)&lt;br/&gt;
Facundo Muñoz&lt;br/&gt;
.light[Statistician]
]



---
# Introduction

![:scale 120%](img/relationship_data-models.svg)


---
class: inverse, middle, center

# Farm infection model

???

First, we are going to present the model of farm infection.

---
# Overview

Estimates the __probability of ASF infection__ over the next __prediction period__, for __each pig herd__.

.pull-left[

- Estimates specific infection probability estimates for __three transmission pathways__.

- Combines probabilities assuming __independence__.

- Prediction periods starting at D50, D80 and D110.
]

.pull-right[
![:scale 200%](img/model-farms-summary.svg)
]


???

We considered 3 possible transmission pathways of farm contamination: contact with infectious wild boar, trading of live pigs and fomites.

We aggregated the probabilities of transmission from each pathway into a total __probability__ of infection for each individual farm, over the next period, with data up to days 50, 80 and 110.













---
# Transmission via infectious wild boar - exposure level

.pull-left[
__Kernel density smoothing__ of observed __wild boar cases__ (.red[•]) up to D50, D80 and D110.

The density value at each __outdoor farm__ (.white[⏺]) was used as a measure of __exposure__ to infectious wild boar.

__Assumption__: only outdoor farms are susceptible to infectious WB.
]

.pull-right[
![](presentation_astre_files/figure-html/unnamed-chunk-1-1.png)&lt;!-- --&gt;
]

???

For quantifying the risk of transmission by contact with infectious wild boar, we considered the __density__ of the observed cases as a measure of the local exposure level.

The red points in the figure represent the locations of the recorded wild boar cases up to day 80.
The fence and the buffer zone are outlined for reference and the white circles are the location of the __outdoor farms__ in the area. Infected and detected farms by day 80 are identified with a interior black bullet and a label with their ID.

A strong assumption was that only outdoor farms were susceptible to this transmission pathway.


---
# Transmission via infectious wild boar - model


.pull-left[
Use the __exposure level__ as a covariate in a
__logistic regression__ for the
__detection status__ of __outdoor__ farms
in the region at risk.

Estimate __probability of infection and detection__ for each farm, in the observed period.
]

.pull-right[

![](presentation_astre_files/figure-html/prob-farm-detection-1.png)&lt;!-- --&gt;

]


???

We then fitted a logistic regression for the infection status of the farms given the level of exposure.
The figure in the right displays the estimated regression at D110.

Note that the estimated probabilities considers the risk cumulated over the entire observation period, since the first case up to the current date. They represent the probability of infection __during__ the observed period.


---
# Transmission via infectious wild boar - prediction


.pull-left[
Derive the __daily__ probability of infection

- __Assumption__: homogeneity and independence.
- __Neglect__ hypothetical culling periods.

__Predict__ probability of infection in the next period

- Effect of the __control strategies__

]

.pull-right[
&lt;img src="img/prob-inf-wb.svg" width="200%" /&gt;

]

???

We derive an estimate of the __daily__ probability of infection, based on the size in days of the observation period, assuming constant risk and independence across days.

This is effectively as if we threw the same dice every day to decide whether a farm gets contaminated.

Reversing the calculation, we computed the probability of infection for the farm in the __prediction period__ using the same assumption of homogeneity and independence.
We will discuss these assumptions in the conclusions.

Here we removed the culled periods from the number of days at risk in the prediction period.

The results change with some of the control strategies.
Specifically culling farms sites in the vicinity of wild boar cases, or within the protection zone, simply because they reduce the number of days at risk.

And this is how we obtained our estimate of the infection probability from exposure to infectious wild boar.


---
# Transmission via trade - overview


.pull-left[
For each __farm__, consider the risk of receiving an __infected shipment__.

Either __observed__ recent shipments or __future__ potential shipments.

]

.pull-right[
![](img/farm-shipments.svg)
]


???

Concerning the trade pathway, for each __farm__, we consider the risk of receiving an __infected shipment__ from any other farm.

Notice that such a transmission must either has occurred in one of the shipments from the last 15 days (but not earlier, since it would have been already detected at the destination by now), or it will happen in one future shipment during the prediction period.

For the observed shipments, we must assess the probability that either one of them included infected animals, whereas in the future there is the additional uncertainty about the actual shipments to take place.

Also note that shipments can potentially originate from infected farms, if they took place in the 7 days before symptom onset at the source.


---
# Transmission via trade - observed shipments


.pull-left[
Shipments in the __last 15 days__ (detection period)

- within the __7 days period__ (symptom onset) before suspicion of a __detected farm__ `\(\rightarrow p = .8\)`

- For farms at risk, __probability of infection__ from D-15 to the date of the shipment.
]

.pull-right[
![](img/prob-inf-trade-observed.svg)
]


???

For each target farm, we look at all received shipments in the last 15 days from farms that were later (before 7 days) detected as infected (if any).
This never actually occurred during the challenge.

Of course, once detected, farms were banned from trading and there was no further risk of dissemination.

For a recent shipment from a farm at risk, we considered that the probability of transmission equals the probability of infection of the shipping farm in the period from 15 days before today and the date of the shipment.

We can compute that probability from the daily probability of infection of the shipping farm, computed previously.

Finally, we combined additively the individual probabilities of infection for each observed shipment with destination to the target farm to compute the expected number of infected shipments.



---
# Transmission via trade - predicted shipments


.pull-left[
For each __origin__-__destination__,
`\(\mathbb{P}(\text{inf. ship.}) = \mathbb{P}(\text{ship.}) \cdot \mathbb{P}(\text{inf.} | \text{ship.})\)`

N shipments A to B:
`\(N_{AB} \sim \mathcal{Po}(\lambda_{AB})\)`

- `\(\hat\lambda_{AB}\)`: observed __rates__

`\(\mathbb{P}(\text{inf.} | \text{ship.}) = \frac12[1 - (1-p_d)^{n_d}]\)`

Exp. number of inf. shipments
`\(\hat\lambda_{AB}  \cdot \mathbb{P}(\text{inf.} | \text{ship.})\)`
]

.pull-right[
![](img/prob-inf-trade-predicted.svg)
]


???

During the predictive period we don't know the actual shipments that are going to take place.

The probability of an infected shipment from farm A to farm B can be expressed as the probability of a shipment taking place in that period times the probability that this particular shipment is infected.

We took a rough shortcut here and simply used the empirical rates of shipments for every source and destination. Thus, If no shipment from A to B has been observed in our historical records (since two months before the first case), we assumed that the probability was 0.
On the other hand, if we had observed 2 shipments from A to B in our historical record spanning 4 months, we have a rate of 0.5 shipments per month

The probability of the shipment being infectious was evaluated from the daily probability of infection of the farm at the origin, and the number of unbanned days in the prediction period. The cumulative probability was halved to account for the fact that the shipment can take place either before or after the infection with equal chance.

In this way we computed the expected number of infected shipments in the prediction period for all target farms, by adding together the individual expectations from all the possible origins with which there have been commercial relations in the past.


---
# Transmission via trade - predicted shipments


.pull-left[
For each destination `\(j\)`, add the __expected numbers of infected shipments__ from individual farms into `\(E_j\)`

Probability of infection via trade

`\(X \sim \mathcal{Bi}(n, p), \quad E_j = np\)`

`\(P(X&gt;0) = 1 - \big(1-\frac{E_j}{n}\big)^n \approx E_j\)` for `\(E_j &lt; .25\)`

]

.pull-right[
![](img/prob-inf-trade.svg)
]


???

In summary, for every target farm `\(j\)` we have expected numbers of infectious shipments from past or future mouvements that can be simply added together into a total expectation for the prediction period `\(E_j\)`.

Considering `\(E_j\)` as the expected value of a Binomial variable of the number of infected shipments, the probability of at least 1 infected shipment is approximately equal to `\(E_j\)` for moderate values.

This approximation avoids the dependency on the total number of shipments, which is uncertain for the future.


---
# Transmission via fomites - case 2634



.pull-left[
Probability of infection of farm `\(i\)` due to __proximity__ to farm `\(j\)`

`$$p_{ij} = p_{j} e^{-d_{ij}/\lambda}$$`

- `\(p_j\)` prob. of infection by exposure to contaminated wild boar
- `\(d_{ij}\)` distance in _metres_
- `\(\lambda = 700 / \log(2)\)`

]


.pull-right[
![](img/farms-fomites-1.svg)
]


???

For the second submission, we observed a newly infected farm (Id 2634) which, while in the area at risk, was not susceptible to infectious wild boar according to our framework, since it was an in-door farm. Neither it had received any shipment. Yet, it got infected.

However, it was located in close proximity (~710 m) to another infected, albeit very small, outdoor finishing farm. Perhaps, they were visited by the same veterinarian or other people that introduced the virus via their equipment or vehicles.
This suggested a fomite transmission pathway, related to __proximity__ to infected farms.

So we assumed a risk proportional to the probability of infection by wild boar of the neighbouring farm `\(j\)` weighted by an exponentially decaying function of the distance.

The calibration parameter `\(\lambda\)` was chosen so that the risk is halved every 700 `m`, after our friend 2634.

---
# Transmission via fomites - aggregation



.pull-left[

The total probability of infection by fomites
for every farm `\(i\)`, was computed as

`$$p_i = 1 - \prod_{j \in \partial i} (1 - p_{ij})$$`


- __Assumption__: conditional independence
]


.pull-right[
![](img/farms-fomites.svg)
]


???

In practice, every target farm `\(i\)` has multiple neighbours representing different magnitudes of risk depending on their distances and their particular probabilities of infection.

The final estimated risk of infection via fomites from __any__ of the neighbours at risk was computed by aggregating their individual risks assuming that a transmission event from a neighbouring farm is independent from transmission from other neighbours, given the infection probabilities.

This unavoidable approximation ignores some sources of dependency, such a veterinarian or vehicles visiting several neighbouring farms, causing transmission events more likely to occur in clusters. If anything, it leads to some underestimation of the aggregated probability of infection.


---
# Farm infection probability - rejoinder

.scroll-box[
![](img/model-farms.svg)
]

???

In summary, we estimated infection probabilities for each farm, from each transmission pathway, at each prediction period.

Some of the intermediate quantities depended on the alternative control strategies, which gave us a tool to quantify their impact and provide recommendations.

---
# Farm infection model - Predictions vs. observations

.pull-left[
__D51-D80__
![](presentation_astre_files/figure-html/performance-of-report1-model-1.png)&lt;!-- --&gt;


]


.pull-right[
__D81-110__
![](presentation_astre_files/figure-html/farms-pred-vs-realised-1.png)&lt;!-- --&gt;


]







---
class: inverse, middle, center
# Wild boar model (spatial, mechanistic, stochastic, Bayesian)



---
# Wild boar model

.pull-left[
__Compartmental model__

&lt;img src="img/modelWBflowDiagram.jpg" width="1516" /&gt;

__Force of infection__

&lt;img src="img/foi-equations.jpg" width="1812" /&gt;
]

.pull-right[
__Expected WB density__
&lt;img src="img/wb-density-equation.jpg" width="1732" /&gt;

&lt;img src="img/mapWBDensity.jpg" width="3400" /&gt;
]






---
# Wild boar model - priors

&lt;img src="img/priors-report3.jpg" width="1932" /&gt;





---
# Wild boar model - estimation


- Observed data __ `\(y_{i,j,k}\)` __,
of __type (_i_)__, __time interval (_j_)__ and __pixel (_k_)__,
assumed independant  __Poisson__ random variable.

- __Rate__ set to the __expected value__ of a Poisson-Gamma model given ___n_ simulations__ ( `\(\tau\)` leap method)

`$$\tilde\lambda_{i,j,k}=\frac{1+\sum_1^n \tilde y_{i,j,k}}{1+n}$$`

- `\(n=500\)` (reports 1 &amp; 2), or `\(n=1000\)` (report 3).

- Written in __Nimble__

- __MCMC__ with block MVGaussian Metropolis-Hastings sampler.


---
# Wild boar model - prediction

* Predictions were made for __infectious wild boar__, i.e. compartment __I__.

* Saved the __mean__ and __standard deviation__ of __I__ from __5000__ (report 1) and, __10000__ (reports 2 &amp; 3) simulations, for either

		i) each pixel, at the end of the prediction interval, or,

		ii) global, for each time-step of the prediction interval.


---
# Wild boar model - observed vs. predicted

.pull-left[
__Obs. cummul. incidence__
![](presentation_astre_files/figure-html/aggCumIWB_day110-1.png)&lt;!-- --&gt;
- Aggregated cases D0-D110
]


.pull-right[
__Pred. cummul. incidence__
![](presentation_astre_files/figure-html/aggCumIWB_predRep2-1.png)&lt;!-- --&gt;
- Fitted to data D0-80
- Mean of 10000 simulations

]

---
# Wild boar model - projections

.pull-left[
__Report 2__
![](presentation_astre_files/figure-html/IWB-scenarios-report2-1.png)&lt;!-- --&gt;
]

.pull-right[
__Report 3__
![](presentation_astre_files/figure-html/IWB-scenarios-report3-1.png)&lt;!-- --&gt;
]


- Increased hunting - strong effect
- Fencing           - weak effect
- 2km active search - zero effect




---
# Wild boar model - xyz



---
# Wild boar model - xyz



---
# Wild boar model - xyz




---
class: inverse, middle, center
# Conclusions


---
- It works

---
# Open lines for improvement

- Stronger link between the two modelled processes and better account for the __temporal dimension in the farm model__, relaxing the assumptions of homogeneity of risk of transmission via wild boar.

- Improve the estimation of the __probability of future shipments from farm to farm__ from empirical rates to something better. Network model?

- Remove the culled periods from the number of days at risk during the observation period, as well as for the prediction period.

- The __independence__ assumption for the contamination pathways in farms should be relaxed.

- Work with the full probabilistic model, rather than simply with expected values.

???

The assumptions of homogeneity and independence of risk from wild boar in the observation and prediction periods are probably the most questionable of our approximations, as the risk of transmission certainly evolves in time. We only updated our estimates 3 times at each submission stage.

We tried to use the estimated density of infectious wild boar from the wild boar model simulations as a measure of exposure. But it has shown a much inferior predictive power than the cumulative wild boar case density.

It is possible that the cumulative density accounts also for carcasses and virus left over the environment and potentially introduced into the farms via fomites.

Perhaps the fomites and wild boar pathways should be merged using the wild boar case and farm densities as exposure variables and the outdoor indicator as a fixed factor.

---
# Thanks!

All our code, reports, exploratory analyses, mistakes and abandoned tests are openly available at

https://forgemia.inra.fr/umr-astre/asf-challenge


&lt;p xmlns:cc="http://creativecommons.org/ns#" xmlns:dct="http://purl.org/dc/terms/"&gt;&lt;a property="dct:title" rel="cc:attributionURL" href="https://forgemia.inra.fr/umr-astre/asf-challenge"&gt;ASF Challenge modelling&lt;/a&gt; by &lt;a rel="cc:attributionURL dct:creator" property="cc:attributionName" href="https://umr-astre.cirad.fr/"&gt;Astre Team&lt;/a&gt; is licensed under &lt;a href="http://creativecommons.org/licenses/by-sa/4.0/?ref=chooser-v1" target="_blank" rel="license noopener noreferrer" style="display:inline-block;"&gt;CC BY-SA 4.0&lt;img style="height:22px!important;margin-left:3px;vertical-align:text-bottom;" src="https://mirrors.creativecommons.org/presskit/icons/cc.svg?ref=chooser-v1"&gt;&lt;img style="height:22px!important;margin-left:3px;vertical-align:text-bottom;" src="https://mirrors.creativecommons.org/presskit/icons/by.svg?ref=chooser-v1"&gt;&lt;img style="height:22px!important;margin-left:3px;vertical-align:text-bottom;" src="https://mirrors.creativecommons.org/presskit/icons/sa.svg?ref=chooser-v1"&gt;&lt;/a&gt;&lt;/p&gt;

Icons:
http://icons8.com
http://www.streamlineicons.com




&lt;!----------------------------------------------------------------------------------------------------------&gt;
&lt;!-- Limitations of math in xarigan																		  --&gt;
&lt;!-- 																									  --&gt;
&lt;!-- The source code of a LaTeX math expression must be in one line, unless it is inside a pair of double --&gt;
&lt;!-- dollar signs, in which case the starting $$ must appear in the very beginning of a line, followed	  --&gt;
&lt;!-- immediately by a non-space character, and the ending $$ must be at the end of a line, led by a		  --&gt;
&lt;!-- non-space character;																				  --&gt;
&lt;!-- 																									  --&gt;
&lt;!-- There should not be spaces after the opening $ or before the closing $.							  --&gt;
&lt;!-- 																									  --&gt;
&lt;!-- Math does not work on the title slide (see #61 for a workaround).									  --&gt;
&lt;!----------------------------------------------------------------------------------------------------------&gt;
    </textarea>
<style data-target="print-only">@media screen {.remark-slide-container{display:block;}.remark-slide-scaler{box-shadow:none;}}</style>
<script src="https://remarkjs.com/downloads/remark-latest.min.js"></script>
<script src="macros.js"></script>
<script>var slideshow = remark.create({
"highlightStyle": "github",
"highlightLines": true,
"countIncrementalSlides": false,
"ratio": "16:9"
});
if (window.HTMLWidgets) slideshow.on('afterShowSlide', function (slide) {
  window.dispatchEvent(new Event('resize'));
});
(function(d) {
  var s = d.createElement("style"), r = d.querySelector(".remark-slide-scaler");
  if (!r) return;
  s.type = "text/css"; s.innerHTML = "@page {size: " + r.style.width + " " + r.style.height +"; }";
  d.head.appendChild(s);
})(document);

(function(d) {
  var el = d.getElementsByClassName("remark-slides-area");
  if (!el) return;
  var slide, slides = slideshow.getSlides(), els = el[0].children;
  for (var i = 1; i < slides.length; i++) {
    slide = slides[i];
    if (slide.properties.continued === "true" || slide.properties.count === "false") {
      els[i - 1].className += ' has-continuation';
    }
  }
  var s = d.createElement("style");
  s.type = "text/css"; s.innerHTML = "@media print { .has-continuation { display: none; } }";
  d.head.appendChild(s);
})(document);
// delete the temporary CSS (for displaying all slides initially) when the user
// starts to view slides
(function() {
  var deleted = false;
  slideshow.on('beforeShowSlide', function(slide) {
    if (deleted) return;
    var sheets = document.styleSheets, node;
    for (var i = 0; i < sheets.length; i++) {
      node = sheets[i].ownerNode;
      if (node.dataset["target"] !== "print-only") continue;
      node.parentNode.removeChild(node);
    }
    deleted = true;
  });
})();
(function() {
  "use strict"
  // Replace <script> tags in slides area to make them executable
  var scripts = document.querySelectorAll(
    '.remark-slides-area .remark-slide-container script'
  );
  if (!scripts.length) return;
  for (var i = 0; i < scripts.length; i++) {
    var s = document.createElement('script');
    var code = document.createTextNode(scripts[i].textContent);
    s.appendChild(code);
    var scriptAttrs = scripts[i].attributes;
    for (var j = 0; j < scriptAttrs.length; j++) {
      s.setAttribute(scriptAttrs[j].name, scriptAttrs[j].value);
    }
    scripts[i].parentElement.replaceChild(s, scripts[i]);
  }
})();
(function() {
  var links = document.getElementsByTagName('a');
  for (var i = 0; i < links.length; i++) {
    if (/^(https?:)?\/\//.test(links[i].getAttribute('href'))) {
      links[i].target = '_blank';
    }
  }
})();
// adds .remark-code-has-line-highlighted class to <pre> parent elements
// of code chunks containing highlighted lines with class .remark-code-line-highlighted
(function(d) {
  const hlines = d.querySelectorAll('.remark-code-line-highlighted');
  const preParents = [];
  const findPreParent = function(line, p = 0) {
    if (p > 1) return null; // traverse up no further than grandparent
    const el = line.parentElement;
    return el.tagName === "PRE" ? el : findPreParent(el, ++p);
  };

  for (let line of hlines) {
    let pre = findPreParent(line);
    if (pre && !preParents.includes(pre)) preParents.push(pre);
  }
  preParents.forEach(p => p.classList.add("remark-code-has-line-highlighted"));
})(document);</script>

<script>
slideshow._releaseMath = function(el) {
  var i, text, code, codes = el.getElementsByTagName('code');
  for (i = 0; i < codes.length;) {
    code = codes[i];
    if (code.parentNode.tagName !== 'PRE' && code.childElementCount === 0) {
      text = code.textContent;
      if (/^\\\((.|\s)+\\\)$/.test(text) || /^\\\[(.|\s)+\\\]$/.test(text) ||
          /^\$\$(.|\s)+\$\$$/.test(text) ||
          /^\\begin\{([^}]+)\}(.|\s)+\\end\{[^}]+\}$/.test(text)) {
        code.outerHTML = code.innerHTML;  // remove <code></code>
        continue;
      }
    }
    i++;
  }
};
slideshow._releaseMath(document);
</script>
<!-- dynamically load mathjax for compatibility with self-contained -->
<script>
(function () {
  var script = document.createElement('script');
  script.type = 'text/javascript';
  script.src  = 'https://mathjax.rstudio.com/latest/MathJax.js?config=TeX-MML-AM_CHTML';
  if (location.protocol !== 'file:' && /^https?:/.test(script.src))
    script.src  = script.src.replace(/^https?:/, '');
  document.getElementsByTagName('head')[0].appendChild(script);
})();
</script>
  </body>
</html>
