---
title: "How to generate simulations from model 12."
author: "David Pleydell & Mahe Liabeuf"
date: "`r format(Sys.Date(), '%e %B, %Y')`"
output:
  rmdformats::readthedown:
    toc_depth: 2
    mathjax: "default"
    highlight: pygments
    lightbox: true
    gallery: true
    use_bookdown: true
# bibliography: asf_challenge.bib
---


```{r setup, include=FALSE, cache = FALSE}
knitr::opts_chunk$set(
  echo  = TRUE,
  cache = FALSE
)
theme_set(theme_bw())
```



# Introduction
This vignette aims to introduce how to run simualtions from model12.
Once Mahe has pushed her changes we will adapt this vignette to use code in model12Mahe.

The form of this vignette follows the structure of the file `simWildBoarFromMCMC12.R`



# Setting directories and loading libraries and functions
The first thing to do is set the directories corresponding to
1. the `model_definition.R` file,
2. the `samples.csv` (MCMC outpt) file, and
3. where we want our simulations to be stored.

```{r}
modelDir = here::here("nimble/model12-M1")    # Directory storing required model_definition.R file
mcmcDir  = paste0(modelDir,"/MCMCall1000") # Directory storing required MCMC output
simDir   = paste0(mcmcDir,"/simsMahe")     # Directory for storing simulations
setwd(modelDir)
```

Next, create objects that recreate the paths to the MCMC output.
```{r, collapse=TRUE}
mcmcFile   = paste0(mcmcDir,"/samples.csv")  ## MCMC samples of parameters
mcmcFile2  = paste0(mcmcDir,"/samples2.csv") ## Posterior log-likelihoods of each MCMC sample
mcmcFile
mcmcFile2
```

Next, we check if `simDir` exists. If it does not exist then we will create it.
```{r}
if (!file.exists(simDir))
  dir.create(simDir)
```

Next, load the libraries and functions we require.
```{r}
source(here::here("src/packages.R"))            # This script loads packages via pacman
source(here::here("src/functions.R"))           # Our own R functions
source(here::here("nimble/nimbleFunctions.R"))  # Our own R and nimble functions for working with the WB model
source(paste0(modelDir, "/model_definition.R")) # Where the wild boar model is defined
```


Finally, load the set of drake objects (targets) needed to use the wild boar model.
```{r  load-drake-targets}
  loadd(admin,				# Administrative boundaries
        bboxAll,			# A bounding box around the whole island
        fence,				# The fence
        huntZone,			# The hunting zone
        hexAll,				# All hexagonal pixels
        hexCentroidsAll,	# Centroids (i.e. points) for all pixels
        weightTauAHome1km,	# Weights for active search
        weightTauAAway1km,	# Weights for active search
        weightTauAHome2km,	# Weights for active search
        weightTauAAway2km,	# Weights for active search
        outbreaks_at_D110,	# Wild boar case data up to day 110
        pig_sites,			# locations of pig farms
        pHomeSim4km,        # pHome, simulated assuming a 4km^2 home range, integrating over evenly spaced centroids (100m hex grid) within the home pixel
        stepsPerChunkAll,	# Temporal resolution of output. Set as 1 day.
        wildBoarArrayAll,	# observations aggregated over one day intervals in hexagons covering whole island
        wildBoarObsAll)		# Wild boar data
```
To do: verify that we really need all of these for this project.

# Initialise the simulations
When creating a nimble model, we can provide up to3 arguments
1. `const` - the model's constants.
2. `inits` - initial parameter values.
3. `data`  - the model's data.

For simulating the wild boar model, we just need to set `const` and `inits`.
We do this via two functions.

```{r}
tStopAll          = 230 ## The final day of the simulation

initWildboarAll   = setWildBoarModelInitialValues(
  wbArray4sim   = wildBoarArrayAll,
  wildBoarObs   = wildBoarObsAll,
  stepsPerChunk = stepsPerChunkAll,
  bboxAll       = bboxAll,
  tStop         = tStopAll,
  returnI       = TRUE)

constWildboarAll  = setWildBoarModelConstants(
  wbArray4sim       = wildBoarArrayAll,
  Hex               = hexAll,
  HexCentroids      = hexCentroidsAll,
  Outbreaks         = outbreaks_at_D110,
  stepsPerChunk     = stepsPerChunkAll,
  bboxAll           = bboxAll,
  tStop             = tStopAll,
  weightTauAHome1km = weightTauAHome1km,
  weightTauAAway1km = weightTauAAway1km,
  weightTauAHome2km = weightTauAHome2km,
  weightTauAAway2km = weightTauAAway2km,
  returnI           = TRUE)
```

We now have everything we need to initialise the model
```{r}
rWildBoarModelAll = nimbleModel(bugsBoarCode,
                                constants=constWildboarAll,
                                inits=initWildboarAll,
                                calculate = FALSE, debug = FALSE)
## The following creates a "modelValues" object for storing output
rModel4MV = nimbleModel(bugsCode4MV)
```

# Navigating the model
A nimble model is constructed as a set of `nodes`.
It is useful to have lists of these nodes.
```{r}
 detNodes      = rWildBoarModelAll$getNodeNames(determOnly=TRUE)
 obsNodes      = sub("\\[.*","",detNodes[grep("obsY", detNodes)])
 detNodesNoObs =                detNodes[grep("obsY", detNodes, invert=TRUE)]
 stochNodes    = rWildBoarModelAll$getNodeNames(stochOnly=TRUE)
```
