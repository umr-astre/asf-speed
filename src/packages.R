# Basic packages commonly used for all targets
pacman::p_load(coda,        # MCMC diagnostics
               colorspace,  # Colour palettes
               datamodelr,  # https://github.com/bergant/datamodelr   devtools::install_github("bergant/datamodelr")
               drake,       # A pipeline toolkit for reproducible computation at scale
               DiagrammeR,
               easyalluvial,# Alluvial graphs for multivariate data
               fasterize,   # Rasterize an sf object of polygons
               formattable, # Nice html tables
               furrr,       # Apply Mapping Functions in Parallel using Futures
               future.callr,
               plyr,        # Provides revalue, for renaming factor levels. MUST BE LOADED BEFORE HERE TO AVOID CONFLICT.
               gifski,
               ggfortify,   # ggplot2::autoplot method for survival objects
               ggrepel,     # Repulsive labels for ggplot
               ggsn,
               gtsummary,
               here,        # Identifies where .git directory is. MUST BE LOADED AFTER PLYR TO AVOID CONFLICT.
               hrbrthemes,  # Additional Themes and Theme Components for 'ggplot2'
               igraph,      # Handle networks
               intergraph,  # Convert between network and igraph formats
               janitor,     # simple little tools for examining and cleaning dirty data
               kableExtra,  # Printing tables
               knitr,       # A general-purpose tool for dynamic report generation
               # landscapemetrics,  # enables extraction of land cover metrics from a raster grid
               magick,      # R bindings to imagemagick
               mapview,     # View spatial objects interactively
               MASS,        # Kernel density estimation kde2d()
               network,     # Handle networks,
               nimble,      # MCMC, Particle Filtering, and Programmable Hierarchical Modeling
               nngeo,       # For st_nn in our function generateHexagonalGrid
               patchwork,
               pryr,        # Memory management tools
               raster,      # Geographic Data Analysis and Modeling
               readxl,      # Read Excel Files
               rio,         # Import files from multiple formats
               rgdal,       # Bindings for the 'Geospatial' Data Abstraction Library
               rmapshaper,  # Client for 'mapshaper' for 'Geospatial' Operations. Used to simplify polygons.
               rmdformats,
               RColorBrewer,# For color palettes
               sf,          # extends data.frame-like objects with a simple feature list column
               skimr,       # provides an alternative to the default summary functions within R
               stplanr,     # Sustainable Transport Planning with R
               stringr,     # Provides str_pad
               survival,    # Time-to-event data description and models
               survminer,   # Survival plots
               tidyverse,   # A set of packages that work in harmony because they share common data representations and 'API' design.
               tmap,        # Thematic Map Visualization
               tmaptools,   # Provides get_brewer_pal
               units,       # Handle units of measurement
               viridisLite  # Provides color palettes
               )




# pacman::p_load(drake, igraph, magick, mapview, nngeo, rgdal, sf, stplanr, tmap, tmaptools)
# remove.packages("nngeo")
# pacman::p_load(sf)
