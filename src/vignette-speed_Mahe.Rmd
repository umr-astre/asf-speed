---
title: "How to generate simulations from model 12."
author: "David Pleydell & Mahe Liabeuf"
date: "`r format(Sys.Date(), '%e %B, %Y')`"
output:
  rmdformats::readthedown:
    toc_depth: 2
    mathjax: "default"
    highlight: pygments
    lightbox: true
    gallery: true
    use_bookdown: true
# bibliography: asf_challenge.bib
---


```{r setup, include=FALSE, cache = FALSE}
knitr::opts_chunk$set(
  echo  = TRUE,
  cache = FALSE
)
theme_set(theme_bw())
```



# Introduction
This vignette aims to introduce how to run simualtions from model12.
Once Mahe has pushed her changes we will adapt this vignette to use code in model12Mahe.

The form of this vignette follows the structure of the file `simWildBoarFromMCMC12.R`



# Setting directories and loading libraries and functions
The first thing to do is set the directories corresponding to
1. the `model_definition.R` file,
2. the `samples.csv` (MCMC outpt) file, and
3. where we want our simulations to be stored.

```{r}
modelDir = here::here("nimble/model12-M1")    # Directory storing required model_definition.R file
mcmcDir  = paste0(modelDir,"/MCMCall1000") # Directory storing required MCMC output
simDir   = paste0(mcmcDir,"/simsMahe")     # Directory for storing simulations
#setwd(modelDir)
```

Next, create objects that recreate the paths to the MCMC output.
```{r, collapse=TRUE}
mcmcFile   = paste0(mcmcDir,"/samples.csv")  ## MCMC samples of parameters
mcmcFile2  = paste0(mcmcDir,"/samples2.csv") ## Posterior log-likelihoods of each MCMC sample
mcmcFile
mcmcFile2
```

Next, we check if `simDir` exists. If it does not exist then we will create it.
```{r}
if (!file.exists(simDir))
  dir.create(simDir)
```

Next, load the libraries and functions we require.
```{r}
source(here::here("src/packages.R"))            # This script loads packages via pacman
source(here::here("src/functions.R"))           # Our own R functions
source(here::here("nimble/nimbleFunctions.R"))  # Our own R and nimble functions for working with the WB model
source(paste0(modelDir, "/definition_model12-M1.R")) # Where the wild boar model is defined
```


Finally, load the set of drake objects (targets) needed to use the wild boar model.
```{r  load-drake-targets}
  loadd(admin,
      bboxM1,
      hexM1,
      hexCentroidsM1,
      pHomeSim4km
      )
```
To do: verify that we really need all of these for this project.

# Initialise the simulations
When creating a nimble model, we can provide up to3 arguments
1. `const` - the model's constants.
2. `inits` - initial parameter values.
3. `data`  - the model's data.

For simulating the wild boar model, we just need to set `const` and `inits`.
We do this via two functions.

```{r}
tStop         = 365 ## The final day of the simulation
constWildboarM1 = setWildBoarModelConstants(Hex          = hexM1,
                                            HexCentroids = hexCentroidsM1,
                                            bboxHex      = bboxM1,
                                            tStop        = tStop)
initWildboarM1 = setWildBoarModelInitialValues(Hex     = hexM1,
                                               bboxHex = bboxM1,
                                               tStop   = tStop,
                                               pHome   = pHomeSim4km)
```

We now have everything we need to initialise the model
```{r}
rWildBoarModelM1 = nimbleModel(bugsBoarCode,
                               constants=constWildboarM1,
                               inits=initWildboarM1,
                               calculate = FALSE, debug = FALSE)
```

# Navigating the model
A nimble model is constructed as a set of `nodes`.
It is useful to have lists of these nodes.
```{r}
(detNodes      = rWildBoarModelM1$getNodeNames(determOnly=TRUE))
(epiNodes      = sub("\\[.*","",detNodes[grep("epidemic", detNodes)]))
(detNodesNoEpi =                detNodes[grep("epidemic", detNodes, invert=TRUE)])
(stochNodes    = rWildBoarModelM1$getNodeNames(stochOnly=TRUE))
```


```{r}
mcmcSamples  = read.table(file=mcmcFile,  header = TRUE)
mcmcSamples2 = read.table(file=mcmcFile2, header = TRUE) # Assumed to be posterior likelihood
```


```{r}
iMAP                      <- which(mcmcSamples2$var1==max(mcmcSamples2$var1)) ## returns index of most likely MCMC sample
parasMAP                  <- mcmcSamples[iMAP,]
rWildBoarModelM1$beta     <- exp(parasMAP$log_beta)
rWildBoarModelM1$attractI <- ilogit(parasMAP$logit_attractI)
rWildBoarModelM1$pHuntY   <- 0 #ilogit(parasMAP$logit_pHuntY)
rWildBoarModelM1$pIntroX  <- 1/2
rWildBoarModelM1$pIntroY  <- 1/2
```


```{r}
simulate(rWildBoarModelM1, detNodesNoEpi)
for (dn in 1:length(detNodesNoEpi))
  nimPrint(detNodesNoEpi[dn], " ", rWildBoarModelM1[[detNodesNoEpi[dn]]])
for (sn in 1:length(stochNodes))
  nimPrint(stochNodes[sn], " ", rWildBoarModelM1[[stochNodes[sn]]])

## Test R simulation
#simulate(rWildBoarModelAll, detNodes)
dim(rWildBoarModelM1$epidemic)
```

```{r echo=FALSE, message=FALSE}
cWildBoarModelM1 = compileNimble(rWildBoarModelM1, showCompilerOutput = TRUE)
```

```{r}
iS = 1
iE = 2
iI = 3
iR = 4
iC = 5
```

```{r}
nStates = dim(cWildBoarModelM1$epidemic)[1]
nSteps  = dim(cWildBoarModelM1$epidemic)[2] - 1
nPops   = dim(cWildBoarModelM1$epidemic)[3]
nSteps1 = nSteps + 1
```


# Addition of wild Boar

Initialization of wild boar density's to 0. The function used to add the wild boar is tested with different values of mean (grouped in the vector "Mean"). In order to store the values of wild boar and emergence date, two matrix are created.
```{r}
cWildBoarModelM1$sampleEboar<-0
Mean<-seq(2,16,by=2)
WB_data<-matrix(NA,nrow=nrow(hexM1),ncol=length(Mean))
colnames(WB_data)<-paste0("Density",Mean)
Emergence_date<-matrix(NA,nrow=nrow(hexM1),ncol=length(Mean))
```

The "rWBdensity" function use a log normal distribution whose parameters are mean and sd. The number of boar at each pixel is thus, a random variable result of this probability distribution.
The Loop for is used to simulate the different wild boar density with each mean values. Then the model "cWildBoarModelM1" simulate the spread of ASF. The day of emergence at each pixel are indicated.
```{r ,message = FALSE}
for (i in 1:length(Mean)){
  hexM1sim<-rWBdensity(Mean[i],SD=1,hexM1)
  WB_data[,i]=hexM1sim$E_wildboar
  cWildBoarModelM1$Eboar<-hexM1sim$E_wildboar
  simulate(cWildBoarModelM1, detNodes)
  simE <- t(cWildBoarModelM1$epidemic[iE, 1:nSteps1, 1:nPops])
  Emergence_date[,i]=apply(simE,1, function(x){
    xpg0 <- which (x>0)
    if (length(xpg0)>0){
      min(xpg0)-1
    }else{
      Inf
    }
  }
  )
}
Emergence_date[sapply(Emergence_date, is.infinite)]<- NA
colnames(Emergence_date)=paste0("Emergence_mean", Mean)
hexSim <- hexM1sim %>% select(!"E_wildboar") %>% cbind(Emergence_date) %>% cbind(WB_data)
rm(hexM1sim)
```

Wild boar density simulated
```{r echo=FALSE}
hexSim$E_wildboar = as.integer(hexSim$Density16) # as.integer(hexSim$E_wildboar)
tm_shape(hexSim)+tm_polygons(paste0("E_wildboar"), breaks=sort(unique(c(0,1,pretty(hexSim$E_wildboar)))))

hexSim$WB_present<-hexSim$E_wildboar>0
tm_shape(hexSim) + tm_polygons(paste0("Emergence_mean",max(Mean)))
```

Wild boar density simulated
```{r echo=FALSE}
map1_rWBdensity<-list()
for(m in Mean){
  map1_rWBdensity[[which(m==Mean)]] <- tm_shape(hexSim) + tm_polygons(paste0("Density",m), breaks=sort(unique(c(0,1,pretty(c(1,max(WB_data)))))), legend.show = FALSE)
}
map1_rWBdensity[[1+which(m==Mean)]] <- tm_shape(hexSim) + tm_polygons(paste0("Density",m),title="Density", breaks=sort(unique(c(0,1,pretty(c(1,max(WB_data))))))) +
  tm_shape(hexSim) + tm_polygons(col="white", border.col="white") + tm_layout(legend.position = c ("LEFT", "TOP"))

X11() # show the map in a new window
tmap_arrange(map1_rWBdensity)
```

Minimum days of emergence with different value of mean to calculate the wild boar density. The more boars there are, lower is the day of emergence in neighbours pixels and bigger is the spatial dispersion of ASF.
```{r echo=FALSE}
map2_rWBdensity<-list()
for(m in Mean){
  map2_rWBdensity[[which(m==Mean)]]<-tm_shape(hexSim) + tm_polygons(paste0("Emergence_mean",m), legend.show = FALSE)
}
map2_rWBdensity[[1+which(m==Mean)]]<-tm_shape(hexSim) + tm_polygons(paste0("Emergence_mean",m), title = "Emergence_mean") +
  tm_shape(hexSim) + tm_polygons(col="white", border.col="white") + tm_layout(legend.position = c ("LEFT", "TOP"))

X11() # show the map in a new windo
tmap_arrange(map2_rWBdensity)
```

Same thing but the variation come from SD.
```{r}
SD<-seq(0,4,by=2)
```

```{r echo=FALSE, message = FALSE}
cWildBoarModelM1$sampleEboar<-0
SD_data<-matrix(NA,nrow=nrow(hexM1),ncol=length(SD))
colnames(WB_data)<-paste0("Density",Mean)
Emergence_date<-matrix(NA,nrow=nrow(hexM1),ncol=length(SD))
for (i in 1:length(SD)){
  hexM1sim<-rWBdensity(SD[i],Mean=4,hexM1)
  SD_data[,i]=hexM1sim$E_wildboar
  cWildBoarModelM1$Eboar<-hexM1sim$E_wildboar
  simulate(cWildBoarModelM1, detNodes)
  simE <- t(cWildBoarModelM1$epidemic[iE, 1:nSteps1, 1:nPops])
  Emergence_date[,i]=apply(simE,1, function(x){
    xpg0 <- which (x>0)
    if (length(xpg0)>0){
      min(xpg0)-1
    }else{
      Inf
    }
  }
  )
}
Emergence_date[sapply(Emergence_date, is.infinite)]<- NA
colnames(Emergence_date)=paste0("Day_of_Emergence_SD", SD)
hexSim <- hexM1sim %>% select("E_wildboar") %>% cbind(Emergence_date)
```


```{r echo=FALSE}
mapSD<-list()
for(sd in SD){
  mapSD[[which(sd==SD)]]<-tm_shape(hexSim) + tm_polygons(paste0("Day_of_Emergence_SD",sd)) + tm_layout(legend.position = c ("LEFT", "TOP"))
}
tmap_arrange(mapSD)
```

rWB function
```{r echo=FALSE}
cWildBoarModelM1$sampleEboar<-0
Mean<-seq(2,16,by=2)
WB_data<-matrix(NA,nrow=nrow(hexM1),ncol=length(Mean))
colnames(WB_data)<-paste0("Density",Mean)
Emergence_date<-matrix(NA,nrow=nrow(hexM1),ncol=length(Mean))

for (i in 1:length(Mean)){
  hexM1sim<-rWB(Mean[i],SD=1,hexM1,niter=3, hexCentroidsM1)
  WB_data[,i]=hexM1sim$E_wildboar
  cWildBoarModelM1$Eboar<-hexM1sim$E_wildboar
  simulate(cWildBoarModelM1, detNodes)
  simE <- t(cWildBoarModelM1$epidemic[iE, 1:nSteps1, 1:nPops])
  Emergence_date[,i]=apply(simE,1, function(x){
    xpg0 <- which (x>0)
    if (length(xpg0)>0){
      min(xpg0)-1
    }else{
      Inf
    }
  }
  )
}
Emergence_date[sapply(Emergence_date, is.infinite)]<- NA
colnames(Emergence_date)=paste0("Emergence_mean", Mean)
```


```{r}
WB_Present <- WB_data > 0
colnames(WB_Present) <- paste0("Present",Mean)

hexSim <- hexM1sim %>% select("E_wildboar") %>% cbind(Emergence_date) %>% cbind(WB_data) %>%cbind(WB_Present)
attributes(hexSim)$neighVec=attributes(hexM1)$neighVec

```

Density of wild boar generated from the rWB function.
```{r echo=FALSE}
map1_rWB<-list()
for(m in Mean){
  map1_rWB[[which(m==Mean)]] <- tm_shape(hexSim) + tm_polygons(paste0("Density",m), breaks=sort(unique(c(0,1,pretty(c(1,max(WB_data)))))), legend.show = FALSE)
}
map1_rWB[[1+which(m==Mean)]] <- tm_shape(hexSim) + tm_polygons(paste0("Density",m),title="Density", breaks=sort(unique(c(0,1,pretty(c(1,max(WB_data))))))) +
  tm_shape(hexSim) + tm_polygons(col="white", border.col="white") + tm_layout(legend.position = c ("LEFT", "TOP"))

#X11()  show the map in a new window
tmap_arrange(map1_rWB)
```

Days of emergence of ASF.
```{r echo=FALSE}
map2_rWB<-list()
for(m in Mean){
  map2_rWB[[which(m==Mean)]]<-tm_shape(hexSim) + tm_polygons(paste0("Emergence_mean",m), legend.show = FALSE)
}
map2_rWB[[1+which(m==Mean)]]<-tm_shape(hexSim) + tm_polygons(paste0("Emergence_mean",m), title = "Emergence_mean") +
  tm_shape(hexSim) + tm_polygons(col="white", border.col="white") + tm_layout(legend.position = c ("LEFT", "TOP"))

#X11() show the map in a new windo
tmap_arrange(map2_rWB)
```
