---
title: "ASF Challenge - submission 1"
author: "Cirad-Inrae team"
date: "`r format(Sys.Date(), '%e %B, %Y')`"
# knit: (function(inputFile, encoding, knit_root_dir) {
#     rmarkdown::render(
#       inputFile,
#       encoding = encoding,
#       knit_root_dir = knit_root_dir,
#       output_dir = "../report",
#       output_format = "pdf_document"
#     )
#   })
output:  bookdown::pdf_document2
classoption: "a4paper"
bibliography: asf_challenge.bib
---


```{r eval = interactive(), cache = FALSE, include = FALSE}
source("src/packages.R")
source("src/functions.R")
message("Assuming interactive session")
```

```{r setup, include=FALSE, cache = FALSE}
knitr::opts_chunk$set(
  echo = FALSE,
  cache = FALSE
)

theme_set(theme_bw())
```


# Introduction


```{r load-drake-targets, include = FALSE}
## Explicitly name all targets we want access to
loadd(
  admin,
  animal_movements,
  data_structure,
  distMatPigs,
  farm_pred_inf_ship_CSIF_D50,
  fm_farm_inf_D50,  # model prob farm infection
  hexAll,
  landcover,
  lcMap,
  lc_lite,
  lc_lite_r,
  movementAnimation,
  movementAnimationHack,
  movementFileName,
  negative_hunt_samples,
  obs_days_D50,         ## Number of observation days for the exposure process
  outbreaks_at_D50,
  pig_sites,
  pig_sites_pred_CSIF_D50,
  # raw_datasets,
  region_at_risk_plot_D50,
  tab_farms_pred_CSIF_D50,
  tmAdminPigTypesAndOutbreaks,
  tmProportionAgricultureMap,
  tmProportionForestCoverMap,
  tblMoves_at_D50,
  tmAdmin,
  tmRowAdmin,
  wb_case_density_D50,
  wildBoarObsAll,
)


```

We considered the circulation of the African swine fever virus (ASFV) among wild boar (WB) and domestic pigs separately.

The first cases in WB were detected in hunted animals.
Given that only 20% of hunted animals were tested,
it appears likely that the prevalence of ASF in the WB population was already high
when the first cases were detected.


The first infected wild boar (IWB) was reported on day 6 (and probably hunted on day 3), i.e. after the first detect farm case. Thus, the first potential infection with the virus could have occurred 14 days earlier (i.e. day -11). Similarly, the first carcass was found in the forest on day 13 by active search and tested positive on day 16.

Because the virus can be detected in a carcass for up to 90 days, the first potential infection of this animal could have occurred as early as day -77, which is equivalent to 2.5 months before the first index case in domestic pigs.

Therefore, we assumed that the virus could have been circulating in the WB population for several weeks, that the virus in this population was well established for some time and that the forest represented an important source of virus for adjacent outdoor pig farms.

In those farms, we considered that ASFV can be introduced in the domestic pig circuit by direct contact between pigs and IWB (particularly in outdoor facilities) or by the introduction of fomites (hunting remains, infected forest materials,...), since both pathways are equally possible in the proximity of a forest with an important population of IWB.


\clearpage


# Pig farms

## Methods

### Probability of farm infection

- __Outdoor__ farm sites in the _proximity_ of observed WB cases are at
risk of contamination. For the moment, we neglected indoor sites as we
considered the probability of an epidemic among farms starting at these sites to be relatively negligible.

  We defined _proximity_ using a buffer zone of `r kde_buffer_km`
  km around the convex-hull of all the observed WB cases in the whole
  period (Figure \@ref(fig:region-at-risk)).


- We computed an _ad-hoc_ measure of __exposition__ to the virus using a __kernel
density estimation__ of WB cases aggregated over the whole period (Figure
\@ref(fig:region-at-risk)).


- We estimated the probability $p_i$ of __infection and detection__ of __outdoor
farms__ in the region at risk and __within the first observation period__ with a
simple logistic regression on the level of exposition.

  Let $y_i = 0$ or $1$ respectively for undetected and detected outdoor farm $i$
  in the region at risk, and $x_i$ the corresponding exposure level to ASFV as
  previously estimated.
  \begin{equation}
    \begin{aligned}
    y_i & \sim \text{Bernoulli}(p_i) \\
    \eta_i & = \log\Big(\frac{p_i}{1-p_i}\Big) = \beta_0 + \beta_e x_i
    \end{aligned}
  \end{equation}

  Figure \@ref(fig:prob-farm-detection) shows the predicted probabilities for
  a range of exposure values.

  Here we are taking a few shortcuts by considering the probability of infection
  as 0 for indoor farms or farms beyond the buffer depicting the region at risk. Also by considering
  the risk of exposition as constant in time during the observed period.
  Yet, given the limited number of observations and the short period observed,
  we considered these simplifications as sufficiently good approximations.


- For each farm at risk $i$ (i.e., outdoor and within the region at risk), we
compute a __daily probability of infection__ ${p_d}_i$ as follows.

  Assuming (again) that the exposure level has been constant, beginning from
  day `r day_expo_start` (which is a central value from our simulations
  of the epidemy among WB), we have that the probability of infection
  in period of $n_d$ days, with a daily probability of infection $p_d$ is
  \begin{equation}
    \label{eq:prob_period}
    1 - (1 - p_d)^{n_d}.
  \end{equation}


  Having estimated this probability in the last step, and observed
  $n_d = `r obs_days_D50`$ (taking into account the detection delay in farms of
  `r detection_delay` days), we solve for the daily probability of
  infection ${p_d}_i$ for each farm $i$.


### Prediction of farm infection

- Outdoor farms within the region at risk can get infected in the prediction
period, from day 51 to day 80.

  Pushing a little further the assumption of constant exposure, we use the
  estimated daily probability of infection and compute the probability of
  infection in the period using equation (\ref{eq:prob_period}).

  This allowed us to identify a few farms in the area with significant
  probability of infection (Figure \@ref(fig:prob-infection-farms)).


- Another pathway for introduction of ASFV in farms is via animal shipments from
an infected but not yet detected farm.

  Based on the available information, the three detected farms at day 50 had
  not shipped animals in the two weeks previous to detection. However, other
  farms in the area at risk have.

  We considered the recorded shipments from any of the farms at risk. Furthermore,
  we computed the expected number of shipments and destinations for the prediction
  period, simply based on the empirical rates of shipments.

  For the observed shipments, we computed the probability that the source farm
  was infected at the time of the shipment.
  For the prediction period, we approximated the expected number of shipments
  and the probability that any of them carried the virus.

  The probability of introduction of ASF into each of
  the destination farms is shown in Figure \@ref(fig:farm-transmission).


As a final result, we produce the list of farms with non-null probability of
infection by day 80, excluding the three already detected.
Note that farms infected in the last two weeks of the prediction period will not have been detected yet.




<!---------------->
<!-- \clearpage -->
<!---------------->

## Results


### Farm circulation



```{r region-at-risk, fig.cap = cap}
cap <- "Region at risk for outdoor farm contamination due to proximity to infected wild boar.
Background colour represents a of wild boar case density.
Smaller red dots are the locations of WB cases. Bigger white
points are outdoor farms. The three of them already infected and detected by day 50
are marked with a black point. kde indicates kernel density estimate,
and is used to quantify the exposition level of outdoor farms."

region_at_risk_plot_D50

```



```{r prob-farm-detection, fig.cap = cap, fig.height = 3}
cap <- "Probability of farm infection and detection (curve), estimated from pressence/abscence data (points),
for outdoor farms in the region at risk, as a function of the exposition level."
pig_sites_pred_CSIF_D50 %>%
  filter(susceptible_wb) %>% 
  mutate(
    detected = as.numeric(detected)
  ) %>%
  ggplot(aes(expo_inf_wb, detected)) +
  geom_point() +
  geom_line(
    data = data.frame(
      expo_inf_wb = seq(
        min(pig_sites_pred_CSIF_D50$expo_inf_wb),
        max(pig_sites_pred_CSIF_D50$expo_inf_wb),
        length = 101
      )
    ) %>%
      mutate(
        pred = predict(fm_farm_inf_D50, type = "response", newdata = .)
      ),
    aes(expo_inf_wb, pred)
  ) +
  labs(x = "Exposition level", y = "Detection probability") +
  theme(
    panel.grid.minor.x = element_blank(),
    panel.grid.minor.y = element_blank(),
    panel.grid.major.x = element_blank()
  )
```

```{r prob-infection-period, fig.cap = cap, include = FALSE}
cap <- "Probability of infection due to exposition of infected wild boar in a number of days, given the daily infection probability."
expand.grid(
  p_d = seq(0.001, .050, length = 50),
  n_d = seq(45, 75)
) %>%
  mutate(
    p_p = p_period(p_d, n_d)
  ) %>%
  ggplot(aes(p_d, n_d)) +
  geom_tile(aes(fill = p_p)) +
  geom_contour(aes(z = p_p), breaks = .5, col = "white") +
  scale_fill_viridis_c() +
  labs(x = "Prob. infection in a day", y = "Number of days")

```

```{r prob-infection-farms, fig.cap = cap}
cap <- "Probability of infection due to exposition of infected wild boar during the prediction period (D35 - D65), for farms in the
area at risk."
tm_shape(
  admin,
  bbox = st_bbox(wb_case_density_D50)
) +
  tm_layout(bg.color = "lightblue") +
  # tm_grid(col = "grey", labels.size = 1) +
  tm_scale_bar(breaks = c(0, 10, 30, 60), text.size = 0.9, position = c("left", "top")) +
  tm_polygons() +
  tm_shape(pig_sites_pred_CSIF_D50 %>% filter(susceptible_wb)) +
  tm_dots(
    title.size = "Prob. of detection",
    size = "p_pred",
    col = "detected"
  )
```



```{r farm-transmission, fig.cap = cap}
cap <- "Cartographic representation of observed animal shipments with risk of ASFV transmission."
tmAdmin +
  tm_shape(pig_sites_pred_CSIF_D50 %>% filter(susceptible_wb)) +
    tm_bubbles(col = "FarmType", size=0.3, palette = "viridis") +
  tm_shape(
    pig_sites %>%
      right_join(
        farm_pred_inf_ship_CSIF_D50,
        by = c(population_id = "dest")
      )
  ) +
  tm_bubbles(
    title.size = "Exp.# of infectious shipments",
    size = "e_inf_shipments"
  ) +
  tm_shape(
    animal_movements %>%
      filter(date >= 50 - detection_delay, date <= 50) %>% 
      filter(
        source %in% 
          filter(pig_sites_pred_CSIF_D50, susceptible_wb, !detected)$population_id,
        dest %in% farm_pred_inf_ship_CSIF_D50$dest
      )
  ) +
  tm_lines()
```


```{r table-farms-pred}
kable(
  tab_farms_pred_CSIF_D50 %>%
  filter(p_pred > 0.05, !detected) %>% 
  select(-detected),
  caption = "Probability of infection in farm sites.",
  format = "latex",
  digits = 3,
  booktabs = TRUE
)

```

\clearpage

# Wild boar

```{r wb-model, fig.cap = cap, out.width='16cm'}
cap <- "Compartmental model for ASF spread in wild boar. The force of infection (FOI) in a pixel is a funciton of the number of infetious wild boar and carcasses in neighbouring pixels."
knitr::include_graphics(here("src/img/modelWBflowDiagram.jpg"))
```


Transmission of ASFV among WB was modelled using a stochastic space-time compartmental model (Figure \@ref(fig:wb-model)).

Modelling was performed in two steps

1. a parameter estimation step
2. a predictive simulation step

A hexagonal grid was generated across the island with pixel size set to 5km --
a pixel size that we believe provides a reasonable compromise between
(i) being small enough to characterise wildboar territoriality,
and
(ii) not exploding computation time into impractical time scales.
For estimation we used a sub-grid corresponding to a 20km rectangular buffer around all known IWB (402 pixels).
For prediction we used the full grid (6652 pixels).

Within each pixel
individual WB progress randomly through 11 different possible compartments.
Although WB are not moved "physically" from pixel to pixel,
we model connectivity between pixels via the proportion of time
WB spend in the home pixel ($p_\text{Home}$) or visiting adjoining  neighbouring pixels (1 - $p_\text{Home}$) when
calculating the local force of infection.
Model compartments are repressented visually in figure \@ref(fig:wb-model) and are summarised in the following table.


| Notation | Compartment                       |
|----------|-----------------------------------|
| S        | Susceptible                       |
| E        | Exposed                           |
| I        | Infectious                        |
| R        | Recovered                         |
| C_I      | Carcass (infectious)              |
| C_R      | Carcass (non-infectious)          |
| H_-      | Hunted & tested -ve               |
| H_U      | Hunted & untested                 |
| H_+      | Hunted & tested +ve               |
| AS       | Detected carcass (active search)  |
| PS       | Detected carcass (passive search) |



Fixed or determined parameters were as follows


| Notation          | Parameter                               | Value                                      |
|-------------------|-----------------------------------------|--------------------------------------------|
| $\omega$          | Preference for forest                   | 0.8                                        |
| $\tau_I$          | ASF incubation rate                     | 1/7                                        |
| $\tau_C$          | ASF induced mortality rate              | 1/7                                        |
| $p_\text{Rec}$    | Probability to recover                  | 1/20                                       |
| $\tau_\text{Rec}$ | Recovery rate                           | $\tau_C \frac{ p_\text{Rec}}{ (1-p_\text{Rec})}$     |
| $\tau_\text{Rot}$ | Carcass decontamination rate            | 1/90                                                |
| $\tau_H$          | Daily hunting rate                      | $-log((1-p_\text{HuntY})^{\frac{12}{8\times 365}})$ |
| $p_\text{Test}$   | Prob. test hunted animal                | 1/5                                                 |
| $\tau_A$          | Carcass detection rate (active search)  | $\tau_\text{Det}  p_\text{Active}$        |
| $\tau_P$          | Carcass detection rate (passive search) | $\tau_\text{Det}  (1-p_\text{Active})$    |
| $\beta_I$         | Transmission  rate (infectious)         | $\beta  p_\text{AttractI}$                |
| $\beta_C$         | Transmission  rate (carcass)            | $\beta  (1-p_\text{AttractI})$            |


Estimated parameters, and their associated priors, are shown in the following table.


| Notation              | Parameter                                      | Prior                  |
|-----------------------|------------------------------------------------|------------------------|
| $t_\text{Intro}$      | ASF introduction time                          | Unif(-90,0)            |
| $x_\text{Intro}$      | ASF introduction (easting)                     | Unif(extent of island) |
| $y_\text{Intro}$      | ASF introduction (northing)                    | Unif(extent of island) |
| $p_\text{Home}$       | Connectivity (prob. in home pixel)             | Beta(1,1)              |
| $p_\text{AttractI}$   | Attractivity of infectious relative to carcass | Beta(1,1)              |
| $p_\text{Active}$     | Relative sensitivity of active search          | Beta(1,1)              |
| $\beta$               | Transmission rate                              | Exp($\lambda=10^{-7}$) |
| $\tau_\text{Det}$     | "Global" carcass detection rate                | Exp($\lambda=10^{-7}$) |
| $p_\text{HuntY}$      | Prob. hunted in 1 year                         | Beta(330,330)          |




Finally, for predcting the effectiveness of fencing, two parameters were considered with the following values.

| Notation              | Parameter                                      | Prior                  |
| --------------------- | ---------------------------------------------- | -----------------------|
| $\omega_\text{Fence}$ | Fence efficacity                               | 100%, 99%, 98%         |
| $t_\text{Fence}$      | Fence completion date                          | 60, 58, 55, 52         |

Note, that the second parameter ($t_\text{Fence}$)
considers the scenario that the fence is constructed more rapidly than planned (for example, with the aid of the military),
because (as we show below) it is unlikely that completion at day 60 is sufficiently rapid to prevent ASF spreading to the area south of the current epicentre.

Note also the above model neglects the natural population dynamics of the WB. i.e. we
assume the population is stable and that hunting pressure equals the net effects of natural birth
and death processes. We make this simplification to reduce the computation time of the stochastic simulations.


## Expected density

At the start of each simulation, the density of WB in each pixel is drawn from a Poisson distribution with an expected value that

1. reflects the hunting bag from 2019 in each administrative area
2. reflects the 80% habitat preference of WB for forest
3. assumes linear scaling effects of these two variables

Thus, the expected density of wild boar ($D$), in a given pixel $k$, is determined via the following linear weighting scheme

$$ E[D_k] = \Big ( \frac{\omega \times pForest_k}{\sum_{k'\in A_k} pForest_{k'}} + \frac{(1-\omega)\times pAgro_k }{\sum_{k'\in A_k} pAgro_{k'}} \Big ) \times H_A  $$

where $H_A$ indicates the number of WB hunted in 2019 in a given administrative area, and
the $k'\in A_k$ indicates all hexagons within the same administrative area as hexagon $k$.

We originally scaled this value by dividing by $p_\text{HuntY}$, but this resulted in simulations generating approximately twice as many
hunted & tested -ve as were observed. Thus, we subsequently removed this scaling, which greatly improved the fit of our model.


## Force of infection (FOI)

The force of infection (the expected number of infectious events within a pixel $k$ in a given unit of time)
was modeled by considering both living and dead (carcass) infectious WB,
both within a pixel and within all adjoining neighbouring pixels.
Thus, the FOI is calculated as


$$ FOI_k = S_k \times \Big ( \beta_I \times \Big  (c_\text{Ik} I_k + c_\text{Ik'} \sum_{k' \in N_k} w_{kk'} I_{k'} \Big ) +
                             \beta_C \times \Big  (c_\text{Ck} C_k + c_\text{Ck'} \sum_{k' \in N_k} w_{kk'} c_\text{k'} \Big ) \Big ) $$

<!-- $$ FOI_k = S_k \times ( \beta_I \times (c_\text{Ik} * I_k + c_\text{Ik'} \times \sum_\text{k' \in Neighbours} w_{kk'} I_{k'}) + -->
<!--                 \beta_C \times (c_\text{Ck} * C_k + c_\text{Ck'} \times \sum_\text{k' \in Neighbours} w_{kk'} c_\text{k'}) $$ -->

where $N_k$ is the set of adjoining neighbour pixels. Note,  c_\text{Ik}, c_\text{Ik'}, c_\text{Ck} and c_\text{Ck'}
are weights (that sum to one) for the relative importance of infectious-local, infectious-neighbouring, carcass-local and carcass-neighbouring
sources of infection.
These are defined based on probabilities that an individual is in it's home or neighbouring pixels at any moment in time
(which is assumed 1 and 0 for carcasses respectively). Thus

$\begin{aligned}
c_\text{Ik}    &=  p_\text{Home}^2 + \frac{p_\text{Away}^2}{n_k}                                   \\
c_\text{Ik'}   &=  p_\text{Home} p_\text{Away} (\frac{1}{n_k}+\frac{1}{n_{k'}}) + 2\times \frac{p_\text{Away}^2}{n_k n_k'} \\
c_\text{Ck}    &=  p_\text{Home}                                                                       \\
c_\text{Ck'}   &=  \frac{p_\text{Away}}{n_{k}}
\end{aligned}$

where $p_\text{Away} = 1-p_\text{Home}$ is the proportion of time a WB spends visiting pixels adjacent to their home pixel (i.e the pixel at the centre of their home range),
$n_k$ is the number of neighbouring pixels for pixel $k$, thus
$\frac{p_\text{Away}}{n_k}$ is the probability a WB visits a given neighbouring pixel.

Note, the weights $w_{kk'}$ are used to impliment the effects of fencing in reducing contact between neighbouring pixels.
Thus for estimation they are set to one consistently, and for simualtion, following the building of the fence on day $t_\text{fence}$,
they are set to $(1 - \omega_\text{Fence})$ for all pixel-pairs with centroids falling on either side of the fence.
Thus, an $\omega_\text{Fence}$ of 1 corresponds to 100% fence efficacy.



## Simulation strategy

The compartmental model was simulated using the $\tau$-leap method --
a discrete time approximation of the Gillespie algorithm.
Thus, we used stochastic simulation where, for each day,

1. the number of WB leaving a given compartment is drawn from a Poisson distribution, with rate set to the expected value (given the parameters and current state of the system)
2. those WB are distributed among destination compartments by drawing from a multinomial distribution with probabilities proportional to the rate of each transition.

At the start of each simulation, WB  were


## Parameter estimation 1: Monte Carlo likelihood
In order to estimate the unknown parameters we aggregated WB data in a 3D array ($y$) with dimensions:

1. Observation type: hunted & tested +ve; hunted & tested -ve; detected by active surveillance; detected by passive surveillance.
2. Time "chunk". For estimation we aggregated into chunks (bins) of 10 days. For subsequent simulation we increased resolution to 1 day.
3. Pixel ID.

To quantify how well a given set of parameters explains the aggregated observed data, we quantified
the probability density for each element in $y$ (the aggregated data array) by assuming

$$
\begin{aligned}
y[\text{type}=i,\text{chunk}=j,\text{pixel}=k] \sim \text{Poisson} (\tilde \lambda_{i,j,k} = \tilde E[y[i,j,k]])
\end{aligned}
$$

where the rate parameter ($\lambda_{i,j,k}$) is estimated,
via 500 Monte Carlo simulations,
as the expected value of a Bayesian Poisson-Gamma model (with hyper parameters shape=1 & scale=1), as follows

$$ \tilde \lambda_{i,j,k} = \frac{1 + \sum \tilde y_{i,j,k}} {1 + 500} $$


## Parameter estimation 2: Markov chain Monte Carlo

Markov chain Monte Carlo (MCMC) methods are widely used in Bayesian statistics to explore the relative probability
of various parameter sets given the observed data and the assumed model.
We wrote the model in Nimble (https://r-nimble.org/) so that:
(i) R-like code could be compiled to C++ (providing speed)
(ii) we could exploit nimble's powerful and flexible suite of MCMC samplers.

We performed a series of short "burn-in" runs followed by a longer run of 20000 iterations.
The trajectories through the parameter space were checked visually using
coda (https://cran.r-project.org/web/packages/coda/index.html).

Thus, this MCMC analysis provided 20000 likely parameter combinations that were used for predicting the future course of the epidemic and the effects of fencing.

## Prediction

In this section we describe predictions of the density of infectious wild boar.
Throughout, we use the abbreviation IWB for infectious wild boar and DIWB for density of infectious wild boar.

For each of the twelve combinations of values for the two fence parameters (table 4), 5000 simulations were generated.
For each simulation, the model was parameterised according to a single randomly selected line of MCMC output.

Each simulation was assigned a weight reflecting how closely the simulation fitted the observed data.
This weight was proportional to (the product over all pixels of) the posterior likelihood of the simulated data
given the observed data and a Bayesian Poisson-Gamma model.
These weights were used to calculate a weighted average for the DIWB in each pixel and in each day.

Note, we do not include in these predictions the densities of exposed (i.e. infected but pre-infectious) WB.
In fact the wave front for these animals can be expected to be (on average) 7 days in advance of the wave fronts shown here.
We can readily add these latent infections to our predictions if requested.



### Scenario 1: fence 100% effective and finished on day 60

In the following sub-section we pressent results from 5000 simulations where, 1) the fence is 100% effective, and 2) it is finished on day 60.

Two DIWB predictions for day 50 are shown (figure \@ref(fig:f100tf60t50)).
The most likely simulation (left) suggests that on day 50 IWB can be found over a large portion of the proposed fenced area.
Their density is greatest in a curved cluster within the fenced area and weakest to the west of the proposed fenced --
this reflects differences in land cover and WB density in two administrative areas.
An area between the three infected farms (where the administrative boundary (black line) intersects the proposed fence (purple line))
is predicted to have zero infected WB --
i.e. in the simulated epidemic, the disease has already taken a large toll on the WB population near the epicentre.
The curved shape of the cluster of pixels with greatest DIWB (dark orange and red pixels) arises from a travelling wave
that is radiating from the epicentre in forested areas (figure \@ref(fig:f100tf60t6080)).
By contrast, spread into the agricultural area to the west is much more scattered and less extensive.
The weighted average (right) is very similar to the most likely simulation, but with one exception -- we see a large number of pixels
where the density of IWB is estimated as some value between 0 and 1 (light yellow).
This reflects areas where at least one simulation, with non zero probability, disagrees with the zero-IWB predictions of the most likely simulation.
Interestingly, this area extends south of the proposed fenced zone (purple line), into an area of high WB density (dark green back ground)
and suggests that the disease may move past the proposed fence before it has been built.


```{r f100tf60t50, out.width='16cm', fig.cap = cap}
cap <- "Predictions of the density of infectious wild boar (DIWB) per 5km pixel on day 50.
The predictions repressent: the most likely (of 5000) simulations (left); and, the weighted average of those simulation (right).
The different sahdes of green repressent expected differences in wild boar density (log scale not shown)
Known infected pig-farms are marked by light blue circles.
Farm type is repressented by coloured dots, farm size by dot-size, and practice
 (indoor (under-score), outdoor(O), non-commercial (under-score), commercial (C)) by dot-shape.
Administrative boundaries are in black.
Finally, it is worth noting that the map accurately depicts how the sea is always blue at Merry Island."
knitr::include_graphics(here("figures/report1/infectiousBestAndWM_fence100_tFence60_t50_half.jpg"))
```


<!-- Yellow pixels indicate areas where some simulations indicated the pressence of infectious wild boar, -->
<!-- but the most likely simulations did not, thus the expected density of IWB is less than one. -->
<!-- The light blue circles indicate the three farms with known ASF cases. -->
<!-- The purple rectangle repressents the proposed fence. -->

The progression of the epidemic on days 60 and 80, as predicted by the most likely simulation, is shown in figure \@ref(fig:f100tf60t6080).
All simulations clearly indicate that the fence would not contain the virus in the west -- but this may not be catastrophic since
the DIWB remain relatively low in the west. Indeed, all of our simulations indicate
a slower, more scattered, and perhaps unstable (see figure \@ref(fig:f100tf60t100)), wave front west of the epicentre -- due to relatively low WB densities (light green back ground).
The simulations do suggest that the fence, could be a timely intervention in the north and east.
However, of greater concern is the spread to the south.
In this simulation,  IWB are already adjacent to the southern fence (in one pixel) on day 50 (figure \@ref(fig:f100tf60t50)),
and several IWB are predicted south of the fence on day 60 (figure \@ref(fig:f100tf60t6080)).
Moreover, the relatively high WB densities (dark green background) south of the wave front
will most likely ensure the continued spread of ASF, and a high risk to pig farms, in that area.


```{r f100tf60t6080, out.width='16cm', fig.cap = cap}
cap <- "Predicted density of infectious wild boar (DIWB) for days 60 (left) and 80 (right).
The wave front is predicted be within the fenced area in the north and east on these days.
However, in this simulation (the most likely of 5000) the wave front manages to pass the fence before it's completion on day 60 --
suggesting that either the fence needs to be built more rapidly in that area, or it's location should be shifted further south. "
knitr::include_graphics(here("figures/report1/infectiousBest_fence100_tFence60_t60-80_half.jpg"))
```


Finally, in \@ref(fig:f100tf60t100) we present the weighted average prediction for DIWB at day 100.
This prediction suggests that the most likely scenario is that, assuming the fence is 100% effective,
it's implementation by day 60 will most likely contain the spread of ASF
in the north and east, but could very likely fail to contain the spread to the south.
This southern area is of great concern, because there are many breeder-finishing farms, and even commercial breeder farms, who could potentially
export the virus to other areas of the island through their commercial practices.
It could be pertinent therefore to exercise an elevated hunting pressure to the south of the fence prior to the arrival of ASFV in that area.

```{r f100tf60t100, out.width='8cm', fig.cap = cap}
cap <- "Weighted average DIWB predictions for day 100.
The predictions represent: the most likely (of 5000) simulations (left); and, the weighted average of those simulation (right).
Some yellow pixels to the north and east of the fence indicate that at least one simulation was assigned non-zero probability and
predicted the wave front to pass these sections of fence before the day 60 completion date.
However, the expected values in this area are bellow one, and the most likely scenario is that the fence contains ASF in the north and east, but not in the south. Known infections in pig-farms are marked by light blue circles. "
knitr::include_graphics(here("figures/report1/infectiousWeightedMean_fence100_tFence60_t100_half.jpg"))

```


### Scenario 2: fence 100% effective and finished earlier in the south

Due to the prediction of a likely failure to contain the southern spread of ASF,
we investigated the possible benefits of accelerating fence completion (perhaps through military intervention or international aid) in that area.
Figure \@ref(fig:f100tf5558t100) shows the weighted average predictions for the DIWB on day 100 assuming fence completion on day 55 (left) and day 58 (right).
Recall, that for these predictions, the light yellow pixels represent areas with an expected DIWB of less than one -- so although we cannot exclude spread in these areas,
it appears not to be the most likely scenario.
These simulations suggest that, if the southern portion of the fence is completed by day 55 (left sub-figure), then the southern spread of ASF could be successfully halted -- greatly reducing the risk to pig farms in that area.
However, if work on the fence is insufficiently rapid, then even fence completion on day 58 (right sub-figure) will most likely fail to contain the southern spread of ASF.
These simulations suggest that the immediate priority is the rapid completion of the southern portion of the fence -- starting in the fence's planned south-western corner and working (rapidly) towards the east.


```{r f100tf5558t100, fig.cap = cap, out.width='16cm'}
cap <- "Predicted density of infectious wild boar (DIWB), on day 100, assuming fence completion on day 55 (left) and 58 (right)."
knitr::include_graphics(here("figures/report1/infectiousWeightedMean_fence100_tFence55-58_t100_half.jpg"))
```




### Scenarios 3 & 4: fence 98% effective and finished early vs. no fence

Since some WB can expected to pass through the fence, we investigated the potential implications, focusing attention on the high-risk south of the fenced zone.
We ran simulations with the assumption that the fence (or at least the southern portion) is completed extremely rapidly, on day 52.
We do this to characterise an ideal (even if unrealistic) rapid construction scenario,
which increases the likelihood that, if ASF does spread beyond the fence,
then it is due to fence porosity and not due to slow construction.
We could hypothetically extend this line of thought and implement the fence from day 0
-- we have not done this, but we could readily do it if requested.


The weighted average predictions for day 50 and day 100 are presented in \@ref(fig:f98tf52t50100).
The results indicate that, in the most likely simulations, IWB are already quite close to the southern fence at day 50 (left) and that the epidemic wave front
continues beyond the southern fence after it's completion date (right).
However, these simulations do not eliminate potential confounding with
exposed WB passing the fence location prior to it's completion date.
It would therefore be interesting to run further simulations with a fence in place much earlier.




```{r f98tf52t50100, out.width='16cm', fig.cap = cap}
cap <- "Predicted density of infectious wild boar (DIWB), on days 50 and 100, assuming an extremely rapid (day 52) fence completion, but an imperfect fence efficacity of 98\\%."
knitr::include_graphics(here("figures/report1/infectiousWeightedMean_fence98_tFence52_t50-100_half.jpg"))

```


To put the above results into context, we ran 5000 simulations with the effect of the fence removed.
The weighted average of those simulations (figure \@ref(fig:f0tf0t100))
does not indicate that the southern spread of ASFV will be significantly reduced by the fence.

It is worth noting that, once ASF has established this southern foyer, then it will have successfully spread to an area
with a high density of large pig farms. If one of the breeder or breeder-fattening farms in this area becomes
infected then the disease could spread to trading partners in other areas of the island --
an example of such long distance dispersal events is depicted in figure \@ref(fig:farm-transmission).




```{r f0tf0t100, out.width='8cm', fig.cap = cap}
cap <- "Predicted density of infectious wild boar (DIWB) for day 100 with the fence effect removed.
"
knitr::include_graphics(here("figures/report1/infectiousWeightedMean_fence0_tFence1000_t100_half.jpg"))
```




<!-- montage infectiousWeightedMean_fence98_tFence52_t50_half.jpg infectiousWeightedMean_fence98_tFence52_t100_half.jpg -tile 2x1 -geometry +0+0 infectiousWeightedMean_fence98_tFence52_t50-100_half.jpg -->







<!-- We re-ran the above simulations assuming that fence efficacy was 98%. -->






<!-- ![Weighted mean number of infectious wild boar on days 50 (left) and 60 (right).](../figures/report1/infectiousWeightedMean_fence100_tFence60_t50-60_half.jpg){width=600px} -->


\clearpage

# Conclusions and prediction requests

## Number of infected animals on day 80

### Number and location of outbreaks in __farms__ on day 80

- We identified farms sites with ids
`r tab_farms_pred_CSIF_D50 %>% filter(p_pred >= .5) %>% pull(id) %>% paste(collapse = ", ")` as very likely ($p >= .5$) to have been
infected in this period. To a lesser extent ($.05 <= p <= .5$), farm sites `r tab_farms_pred_CSIF_D50 %>% filter(p_pred > .05, p_pred < .5) %>% pull(id) %>% paste(collapse = ", ")` also have some risk of being infected.

  The total number of animals affected in these farms are
  `r tab_farms_pred_CSIF_D50 %>% filter(p_pred >= .5) %>% pull(size) %>% sum` in the first case and `r tab_farms_pred_CSIF_D50 %>% filter(p_pred > .05, p_pred < .5) %>% pull(size) %>% sum` in the second.


### Number and location of __wild boar cases__ on day 80

The number of infected WB in the forest area to be fenced is very high and the disease spreads quickly in areas of
high population density (see above).


## Effectiveness of fencing the infected zone

Given the estimated rate of spread, there is a high likelihood that by day 60 some animals will have already become infected
to the south of the proposed fenced area. Based on our simulations, this could have already happened as early as day 50, although
estimating a probability for this scenario would require further simulations.

Therefore, the 300 km fence would be ineffective to contain the outbreak in the southern area.
It is also not expected to be very effective to the west of the epicentre, because WB densities are relatively low there.
The fence could however be effective on the northern and eastern sides.

We recommend to start building the fence on the southern side, and suggest that at lest this section of fence
be completed as rapidly as possible.


Our model suggests that the disease spreads rapidly, with speeds of up to 27Km/month.
For the fenced barrier to be effective,
we would recommend to finalise the southern portion as soon as possible, and the rest by day 60.
Alternatively, the perimeter could be extended a further 20 km to the south -- although that will increase the number
of farms exposed to a high risk force of infection within the fenced area.
Since the northern section of the western fence is not expected to be very useful,
it could be displaced to the south to provide a second barrier there.


## Advise on increased hunting pressure in the fenced area.

An highly increased hunting pressure within the fenced area would not be a good approach to prevent the spread of ASF within
the WB population. This action risks disrupting the territorial movement patterns of WB, and could therefore increase the
speed of the travelling wave.  Moreover, our simulations suggest that in areas where WB densities are high,
and that the disease is spreading fast,
the virus can do a very good job of reducing the wild boar population to low density levels.
In addition, the stress induced on the  population by a heightened hunting pressure
would likely increase the spread of the disease among  WB, and
put more pressure (from stressed animals) on the fence itself,
resulting in a higher number of infected animals escaping the fenced area.

Thus, we recommend to allow the virus to reduce WB numbers and only increase hunting pressure once
the population has been reduced as much as possible by the virus (roughly 50 days).

If they do decide to increase the hunting pressure,
we would recommend to target this hunting outside the fence,
particularly in the area outside the south-west corner of the fenced area, since it is here that the virus is most likely
to escape into an area with high densities of both WB and pig farms.

Finally, it is highly important that the fence operates at close as possible to 100% efficacy --
since one infected animal escaping the fenced area could remain infectious for many months after death.
Therefore, it should be pertinent to have very frequent patrols along the fence perimeter, in order to maintain it's quality
and to limit the inevitable reduction in fence efficacy induced by pressure from the wild boar.
